package com.bbva.pzic.risks.business.dto;

import javax.validation.constraints.NotNull;

/**
 * Created on 17/06/2020.
 *
 * @author Entelgy
 */
public class InputListRiskExclusions {

    @NotNull(groups = ValidationGroup.ListRiskExclusions.class)
    private String customerId;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}
