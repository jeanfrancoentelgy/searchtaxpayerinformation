package com.bbva.pzic.risks.business.dto;

/**
 * Created on 19/05/2020.
 *
 * @author Entelgy.
 */
public class InputListRiskFinancialDebts {

    private String customerId;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}
