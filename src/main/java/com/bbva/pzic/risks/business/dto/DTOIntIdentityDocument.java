package com.bbva.pzic.risks.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class DTOIntIdentityDocument {

    @NotNull(groups = ValidationGroup.SearchRisksFinancialInformationV0.class)
    private String documentNumber;

    @NotNull(groups = ValidationGroup.SearchRisksFinancialInformationV0.class)
    @Valid
    private DTOIntDocumentType documentType;

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public DTOIntDocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DTOIntDocumentType documentType) {
        this.documentType = documentType;
    }
}
