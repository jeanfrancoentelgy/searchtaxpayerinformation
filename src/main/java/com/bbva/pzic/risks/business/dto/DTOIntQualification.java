package com.bbva.pzic.risks.business.dto;

/**
 * Created on 15/06/2020.
 *
 * @author Entelgy.
 */
public class DTOIntQualification {

    private String id;
    private String result;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
