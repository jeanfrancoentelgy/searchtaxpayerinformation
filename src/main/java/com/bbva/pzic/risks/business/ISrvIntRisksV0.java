package com.bbva.pzic.risks.business;

import com.bbva.pzic.risks.business.dto.InputListLettersDelegationsCollaborators;
import com.bbva.pzic.risks.business.dto.InputSearchRiskInternalFilters;
import com.bbva.pzic.risks.business.dto.InputSearchRisksFinancialInformation;
import com.bbva.pzic.risks.business.dto.InputSearchTaxpayerInformation;
import com.bbva.pzic.risks.facade.v0.dto.ListLettersDelegationsCollaborators;
import com.bbva.pzic.risks.facade.v0.dto.SearchFinancialInformation;
import com.bbva.pzic.risks.facade.v0.dto.SearchRiskInternalFilters;
import com.bbva.pzic.risks.facade.v0.dto.SearchTaxpayer;

import java.util.List;

/**
 * Created on 15/06/2020.
 *
 * @author Entelgy.
 */
public interface ISrvIntRisksV0 {

    SearchRiskInternalFilters searchRiskInternalFilters(InputSearchRiskInternalFilters mapIn);

    SearchFinancialInformation searchRisksFinancialInformation(InputSearchRisksFinancialInformation input);

    List<ListLettersDelegationsCollaborators> listLettersDelegationsCollaborators(InputListLettersDelegationsCollaborators input);

    SearchTaxpayer searchTaxpayerInformation(InputSearchTaxpayerInformation input);

}
