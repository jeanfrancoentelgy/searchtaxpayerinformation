package com.bbva.pzic.risks.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 21/09/2020.
 *
 * @author Entelgy.
 */
public class InputSearchTaxpayerInformation {

    @Size(max = 11, groups = ValidationGroup.SearchTaxpayerInformation.class)
    @NotNull(groups = ValidationGroup.SearchTaxpayerInformation.class)
    private String documentNumber;
    @NotNull(groups = ValidationGroup.SearchTaxpayerInformation.class)
    private String documentType;

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }
}
