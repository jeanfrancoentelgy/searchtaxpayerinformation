package com.bbva.pzic.risks.business.dto;

import javax.validation.constraints.NotNull;

public class DTOIntDocumentType {

    @NotNull(groups = ValidationGroup.SearchRisksFinancialInformationV0.class)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
