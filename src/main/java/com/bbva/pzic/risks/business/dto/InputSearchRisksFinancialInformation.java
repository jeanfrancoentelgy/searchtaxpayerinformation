package com.bbva.pzic.risks.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class InputSearchRisksFinancialInformation {

    @NotNull(groups = ValidationGroup.SearchRisksFinancialInformationV0.class)
    private String consumerId;

    @NotNull(groups = ValidationGroup.SearchRisksFinancialInformationV0.class)
    private String callingChannel;

    @Valid
    private DTOIntCustomer customer;

    @Valid
    private DTOIntIdentityDocument identityDocument;

    public String getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(String consumerId) {
        this.consumerId = consumerId;
    }

    public String getCallingChannel() {
        return callingChannel;
    }

    public void setCallingChannel(String callingChannel) {
        this.callingChannel = callingChannel;
    }

    public DTOIntCustomer getCustomer() {
        return customer;
    }

    public void setCustomer(DTOIntCustomer customer) {
        this.customer = customer;
    }

    public DTOIntIdentityDocument getIdentityDocument() {
        return identityDocument;
    }

    public void setIdentityDocument(DTOIntIdentityDocument identityDocument) {
        this.identityDocument = identityDocument;
    }
}
