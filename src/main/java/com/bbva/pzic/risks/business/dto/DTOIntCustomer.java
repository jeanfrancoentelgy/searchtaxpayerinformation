package com.bbva.pzic.risks.business.dto;

import javax.validation.constraints.NotNull;

public class DTOIntCustomer {

    @NotNull(groups = ValidationGroup.SearchRisksFinancialInformationV0.class)
    private String customerId;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}
