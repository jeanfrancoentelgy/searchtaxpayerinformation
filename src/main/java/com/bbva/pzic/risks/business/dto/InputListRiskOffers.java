package com.bbva.pzic.risks.business.dto;

import javax.validation.constraints.NotNull;

/**
 * Created on 12/06/2020.
 * 
 * @author Entelgy
 */
public class InputListRiskOffers {

	@NotNull(groups = ValidationGroup.ListRiskOffers.class)
	private String customerId;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
}