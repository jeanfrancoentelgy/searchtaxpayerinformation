package com.bbva.pzic.risks.business.dto;


public class InputListLettersDelegationsCollaborators {

    private String codigoRegistro;
    private String codigoOficina;

    public String getCodigoRegistro() {
        return codigoRegistro;
    }

    public void setCodigoRegistro(String codigoRegistro) {
        this.codigoRegistro = codigoRegistro;
    }

    public String getCodigoOficina() {
        return codigoOficina;
    }

    public void setCodigoOficina(String codigoOficina) {
        this.codigoOficina = codigoOficina;
    }
}
