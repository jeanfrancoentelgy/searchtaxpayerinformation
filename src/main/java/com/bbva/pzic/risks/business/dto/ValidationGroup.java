package com.bbva.pzic.risks.business.dto;

/**
 * Created on 12/06/2020.
 *
 * @author Entelgy
 */
public interface ValidationGroup {

    interface ListRiskBlackListedEntities {
    }

    interface ModifyCustomerRiskProfile {
    }

    interface ListRiskOffers {
    }

    interface SearchRiskInternalFilters {
    }

    interface ListRiskExclusions {
    }

    interface SearchRisksFinancialInformationV0 {
    }

    interface SearchTaxpayerInformation{

    }

}
