package com.bbva.pzic.risks.business.dto;

import javax.validation.constraints.Size;

/**
 * Created on 15/06/2020.
 *
 * @author Entelgy.
 */
public class InputSearchRiskInternalFilters {

    private String documentTypeId;
    @Size(max = 11, groups = ValidationGroup.SearchRiskInternalFilters.class)
    private String documentNumber;
    @Size(max = 8, groups = ValidationGroup.SearchRiskInternalFilters.class)
    private String customerId;

    public String getDocumentTypeId() {
        return documentTypeId;
    }

    public void setDocumentTypeId(String documentTypeId) {
        this.documentTypeId = documentTypeId;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}
