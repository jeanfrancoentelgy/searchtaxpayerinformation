package com.bbva.pzic.risks.business.impl;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.risks.business.ISrvIntRisksV0;
import com.bbva.pzic.risks.business.dto.*;
import com.bbva.pzic.risks.dao.IRisksDAOV0;
import com.bbva.pzic.risks.facade.v0.dto.ListLettersDelegationsCollaborators;
import com.bbva.pzic.risks.facade.v0.dto.SearchFinancialInformation;
import com.bbva.pzic.risks.facade.v0.dto.SearchRiskInternalFilters;
import com.bbva.pzic.risks.facade.v0.dto.SearchTaxpayer;
import com.bbva.pzic.risks.util.Errors;
import com.bbva.pzic.routine.validator.Validator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created on 15/06/2020.
 *
 * @author Entelgy.
 */
@Component
public class SrvIntRisksV0 implements ISrvIntRisksV0 {

    private static final Log LOG = LogFactory.getLog(SrvIntRisksV0.class);

    @Autowired
    private IRisksDAOV0 risksDAOV0;

    @Autowired
    private Validator validator;

    @Override
    public SearchRiskInternalFilters searchRiskInternalFilters(final InputSearchRiskInternalFilters input) {
        LOG.info("... Invoking method SrvIntRisksV0.searchRiskInternalFilters ...");
        validator.validate(input, ValidationGroup.SearchRiskInternalFilters.class);
        if (StringUtils.isEmpty(input.getDocumentNumber()) && StringUtils.isNotEmpty(input.getDocumentTypeId())) {
            throw new BusinessServiceException(Errors.MANDATORY_PARAMETERS_MISSING,
                    "identityDocument.documentNumber field is mandatory.");
        } else if (StringUtils.isNotEmpty(input.getDocumentNumber()) && StringUtils.isEmpty(input.getDocumentTypeId())) {
            throw new BusinessServiceException(Errors.MANDATORY_PARAMETERS_MISSING,
                    "identityDocument.documentType.id field is mandatory.");
        } else if (StringUtils.isEmpty(input.getCustomerId()) && StringUtils.isEmpty(input.getDocumentNumber()) && StringUtils.isEmpty(input.getDocumentTypeId())) {
            throw new BusinessServiceException(Errors.MANDATORY_PARAMETERS_MISSING,
                    "customerId field is mandatory.");
        }
        LOG.info("... Validating searchRiskInternalFilters input parameter ...");
        return risksDAOV0.searchRiskInternalFilters(input);
    }

    @Override
    public SearchFinancialInformation searchRisksFinancialInformation(final InputSearchRisksFinancialInformation input) {
        LOG.info("... Invoking method SrvIntRisksV0.searchRisksFinancialInformation ...");
        LOG.info("... Validating searchRisksFinancialInformation input parameter ...");
        validator.validate(input, ValidationGroup.SearchRisksFinancialInformationV0.class);
        return risksDAOV0.searchRisksFinancialInformation(input);
    }

    @Override
    public List<ListLettersDelegationsCollaborators> listLettersDelegationsCollaborators(final InputListLettersDelegationsCollaborators input) {
        LOG.info("... Invoking method SrvIntRisksV0.listLettersDelegationsCollaborators ...");
        return risksDAOV0.listLettersDelegationsCollaborators(input);
    }

    @Override
    public SearchTaxpayer searchTaxpayerInformation(final InputSearchTaxpayerInformation input) {
        LOG.info("... Invoking method SrvIntRisksV0.searchTaxpayerInformation ...");
        validator.validate(input, ValidationGroup.SearchTaxpayerInformation.class);
        return risksDAOV0.searchTaxpayerInformation(input);
    }

}
