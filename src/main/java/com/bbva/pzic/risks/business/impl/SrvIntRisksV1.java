package com.bbva.pzic.risks.business.impl;

import com.bbva.pzic.risks.business.ISrvIntRisksV1;
import com.bbva.pzic.risks.business.dto.InputListRiskExclusions;
import com.bbva.pzic.risks.business.dto.InputListRiskFinancialDebts;
import com.bbva.pzic.risks.business.dto.InputListRiskOffers;
import com.bbva.pzic.risks.business.dto.ValidationGroup;
import com.bbva.pzic.risks.dao.IRisksDAOV1;
import com.bbva.pzic.risks.dao.model.blacklisted.ModelClientRequest;
import com.bbva.pzic.risks.dao.model.blacklisted.ModelProfileRequest;
import com.bbva.pzic.risks.dao.model.profille.ModelCustomerRiskProfile;
import com.bbva.pzic.risks.facade.v1.dto.Exclusion;
import com.bbva.pzic.risks.facade.v1.dto.FinancialDebt;
import com.bbva.pzic.risks.facade.v1.dto.RiskBlackListedEntity;
import com.bbva.pzic.risks.facade.v1.dto.RiskOffer;
import com.bbva.pzic.routine.validator.Validator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SrvIntRisksV1 implements ISrvIntRisksV1 {

    private static final Log LOG = LogFactory.getLog(SrvIntRisksV1.class);

    @Autowired
    private IRisksDAOV1 risksDAOV1;

    @Autowired
    private Validator validator;

    @Override
    public List<RiskBlackListedEntity> listRiskBlackListedEntities(ModelClientRequest request) {
        LOG.info("... Invoking method SrvIntRisksV1.listRiskBlackListedEntities ...");
        validator.validate(request, ValidationGroup.ListRiskBlackListedEntities.class);

        return risksDAOV1.listRiskBlackListedEntities(request);
    }

    @Override
    public ModelCustomerRiskProfile getCustomerRiskProfile(String customerId) {
        LOG.info("... Invoking method SrvIntRisksV1.getCustomerRiskProfile ...");
        return risksDAOV1.getCustomerRiskProfile(customerId);
    }

    @Override
    public void modifyCustomerRiskProfile(ModelProfileRequest request) {
        LOG.info("... Invoking method SrvIntRisksV1.modifyCustomerRiskProfile ...");
        validator.validate(request, ValidationGroup.ModifyCustomerRiskProfile.class);
        risksDAOV1.modifyCustomerRiskProfile(request);
    }

    @Override
    public List<FinancialDebt> listRiskFinancialDebts(final InputListRiskFinancialDebts input) {
        LOG.info("... Invoking method SrvIntRisksV1.listRiskFinancialDebts ...");
        return risksDAOV1.listRiskFinancialDebts(input);
    }

    @Override
    public List<RiskOffer> listRiskOffers(final InputListRiskOffers input) {
        LOG.info("... Invoking method SrvIntRisksV1.listRiskOffers ...");
        LOG.info("... Validating listRiskOffers input parameter ...");
        validator.validate(input, ValidationGroup.ListRiskOffers.class);
        return risksDAOV1.listRiskOffers(input);
    }

    @Override
    public List<Exclusion> listRiskExclusions(final InputListRiskExclusions input) {
        LOG.info("... Invoking method SrvIntRisksV1.listRiskExclusions ...");
        LOG.info("... Validating listRiskExclusions input parameter ...");
        validator.validate(input, ValidationGroup.ListRiskExclusions.class);
        return risksDAOV1.listRiskExclusions(input);
    }
}
