package com.bbva.pzic.risks.facade.v1.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.business.dto.InputListRiskExclusions;
import com.bbva.pzic.risks.facade.v1.dto.Exclusion;

import java.util.List;

/**
 * Created on 17/06/2020.
 *
 * @author Entelgy
 */
public interface IListRiskExclusionsMapper {

    InputListRiskExclusions mapIn(String customerId);

    ServiceResponse<List<Exclusion>> mapOut(List<Exclusion> exclusion);
}
