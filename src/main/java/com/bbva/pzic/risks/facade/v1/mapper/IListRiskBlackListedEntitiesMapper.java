package com.bbva.pzic.risks.facade.v1.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.dao.model.blacklisted.ModelClientRequest;
import com.bbva.pzic.risks.facade.v1.dto.RiskBlackListedEntity;

import java.util.List;

public interface IListRiskBlackListedEntitiesMapper {

    ModelClientRequest mapIn(String documentType, String documentNumber);

    ServiceResponse<List<RiskBlackListedEntity>> mapOut(List<RiskBlackListedEntity> blackListedEntity);
}
