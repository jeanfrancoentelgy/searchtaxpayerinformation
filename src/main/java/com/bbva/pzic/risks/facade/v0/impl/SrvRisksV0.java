package com.bbva.pzic.risks.facade.v0.impl;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;
import com.bbva.jee.arq.spring.core.servicing.utils.ContextAware;
import com.bbva.pzic.risks.business.ISrvIntRisksV0;
import com.bbva.pzic.risks.facade.RegistryIds;
import com.bbva.pzic.risks.facade.v0.ISrvRisksV0;
import com.bbva.pzic.risks.facade.v0.dto.ListLettersDelegationsCollaborators;
import com.bbva.pzic.risks.facade.v0.dto.SearchFinancialInformation;
import com.bbva.pzic.risks.facade.v0.dto.SearchRiskInternalFilters;
import com.bbva.pzic.risks.facade.v0.dto.SearchTaxpayer;
import com.bbva.pzic.risks.facade.v0.mapper.IListLettersDelegationsCollaborators;
import com.bbva.pzic.risks.facade.v0.mapper.ISearchRiskInternalFiltersMapper;
import com.bbva.pzic.risks.facade.v0.mapper.ISearchRisksFinancialInformationV0Mapper;
import com.bbva.pzic.risks.facade.v0.mapper.ISearchTaxpayerInformationMapper;
import com.bbva.pzic.routine.processing.data.DataProcessingExecutor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.bbva.pzic.risks.facade.RegistryIds.SMC_REGISTRY_ID_OF_LIST_LETTERS_DELEGATIONS_COLLABORATORS;
import static com.bbva.pzic.risks.facade.RegistryIds.SMC_REGISTRY_ID_OF_SEARCH_RISKS_FINANCIAL_INFORMATION;
import static com.bbva.pzic.risks.util.Constants.BRANCH_ID;
import static com.bbva.pzic.risks.util.Constants.EMPLOYEE_ID;

/**
 * Created on 15/06/2020.
 *
 * @author Entelgy.
 */
@Path("/v0")
@SN(registryID = "SNPE2000031", logicalID = "risks")
@VN(vnn = "v0")
@Produces(MediaType.APPLICATION_JSON)
@Service
public class SrvRisksV0 implements ISrvRisksV0, ContextAware {

    private static final Log LOG = LogFactory.getLog(SrvRisksV0.class);

    public UriInfo uriInfo;
    public HttpHeaders httpHeaders;

    @Autowired
    private ISearchRiskInternalFiltersMapper searchRiskInternalFiltersMapper;

    @Autowired
    private ISearchRisksFinancialInformationV0Mapper searchRisksFinancialInformationV0Mapper;

    @Autowired
    private IListLettersDelegationsCollaborators listLettersDelegationsCollaborators;

    @Autowired
    private ISearchTaxpayerInformationMapper searchTaxpayerInformationMapper;

    @Autowired
    private ISrvIntRisksV0 srvIntRisksV0;

    @Autowired
    private DataProcessingExecutor inputDataProcessingExecutor;
    @Autowired
    private DataProcessingExecutor outputDataProcessingExecutor;

    @Override
    public void setUriInfo(UriInfo uriInfo) {
        this.uriInfo = uriInfo;
    }

    @Override
    public void setHttpHeaders(HttpHeaders httpHeaders) {
        this.httpHeaders = httpHeaders;
    }

    @Override
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/risk-internal-filters/search")
    @SMC(registryID = RegistryIds.SMC_REGISTRY_ID_OF_SEARCH_RISK_INTERNAL_FILTERS, logicalID = "searchRiskInternalFilters", forcedCatalog = "gabiCatalog")
    public ServiceResponse<SearchRiskInternalFilters> searchRiskInternalFilters(final SearchRiskInternalFilters searchRiskInternalFilters) {
        LOG.info("----- Invoking service searchRiskInternalFilters -----");

        inputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_SEARCH_RISK_INTERNAL_FILTERS, searchRiskInternalFilters, null, null);
        ServiceResponse<SearchRiskInternalFilters> serviceResponse = searchRiskInternalFiltersMapper.mapOut(
                srvIntRisksV0.searchRiskInternalFilters(
                        searchRiskInternalFiltersMapper.mapIn(searchRiskInternalFilters)));
        outputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_SEARCH_RISK_INTERNAL_FILTERS, serviceResponse, null, null);
        return serviceResponse;
    }

    @Override
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/financial-information/search")
    @SMC(registryID = SMC_REGISTRY_ID_OF_SEARCH_RISKS_FINANCIAL_INFORMATION, logicalID = "searchRisksFinancialInformation")
    public ServiceResponse<SearchFinancialInformation> searchRisksFinancialInformation(final SearchFinancialInformation searchFinancialInformation) {
        LOG.info("----- Invoking service searchRisksFinancialInformation -----");

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_SEARCH_RISKS_FINANCIAL_INFORMATION, searchFinancialInformation, null, null);

        ServiceResponse<SearchFinancialInformation> serviceResponse = searchRisksFinancialInformationV0Mapper.mapOut(
                srvIntRisksV0.searchRisksFinancialInformation(
                        searchRisksFinancialInformationV0Mapper.mapIn(searchFinancialInformation)
                )
        );

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_SEARCH_RISKS_FINANCIAL_INFORMATION, serviceResponse, null, null);

        return serviceResponse;
    }

    @Override
    @GET
    @Path("/letters-delegations/collaborators")
    @SMC(registryID = RegistryIds.SMC_REGISTRY_ID_OF_LIST_LETTERS_DELEGATIONS_COLLABORATORS, logicalID = "listLettersDelegationsCollaborators", forcedCatalog = "gabiCatalog")
    public ServiceResponse<List<ListLettersDelegationsCollaborators>> listLettersDelegationsCollaborators(
            @DatoAuditable(omitir = true) @QueryParam(EMPLOYEE_ID) final String employeeId,
            @QueryParam(BRANCH_ID) final String branchId) {
        LOG.info("----- Invoking service listLettersDelegationsCollaborators -----");

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put(EMPLOYEE_ID, employeeId);
        queryParams.put(BRANCH_ID, branchId);

        inputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_LIST_LETTERS_DELEGATIONS_COLLABORATORS, null, null, queryParams);

        ServiceResponse<List<ListLettersDelegationsCollaborators>> serviceResponse = listLettersDelegationsCollaborators.mapOut(
                srvIntRisksV0.listLettersDelegationsCollaborators(
                        listLettersDelegationsCollaborators.mapIn((String) queryParams.get(EMPLOYEE_ID), (String) queryParams.get(BRANCH_ID))
                )
        );

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_LETTERS_DELEGATIONS_COLLABORATORS, serviceResponse, null, queryParams);
        return serviceResponse;
    }

    @Override
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/taxpayer-information/search")
    @SMC(registryID = RegistryIds.SMC_REGISTRY_ID_OF_SEARCH_TAXPAYER_INFORMATION, logicalID = "searchTaxpayerInformation", forcedCatalog = "gabiCatalog")
    public ServiceResponse<SearchTaxpayer> searchTaxpayerInformation(final SearchTaxpayer searchTaxpayer) {
        LOG.info("----- Invoking service searchTaxpayerInformation -----");

        inputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_SEARCH_TAXPAYER_INFORMATION, searchTaxpayer, null, null);

        ServiceResponse<SearchTaxpayer> serviceResponse = searchTaxpayerInformationMapper.mapOut(
                srvIntRisksV0.searchTaxpayerInformation(
                        searchTaxpayerInformationMapper.mapIn(searchTaxpayer)
                )
        );

        outputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_SEARCH_TAXPAYER_INFORMATION, serviceResponse, null, null);

        return serviceResponse;
    }
}
