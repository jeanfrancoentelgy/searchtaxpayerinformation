package com.bbva.pzic.risks.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "financialProductType", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlType(name = "financialProductType", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class FinancialProductType implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Financial product type identifier.
     */
    private String id;
    /**
     * Name of the financial product type
     */
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
