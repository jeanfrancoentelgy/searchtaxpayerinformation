package com.bbva.pzic.risks.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.risks.business.dto.DTOIntCustomer;
import com.bbva.pzic.risks.business.dto.DTOIntDocumentType;
import com.bbva.pzic.risks.business.dto.DTOIntIdentityDocument;
import com.bbva.pzic.risks.business.dto.InputSearchRisksFinancialInformation;
import com.bbva.pzic.risks.facade.v0.dto.DocumentType;
import com.bbva.pzic.risks.facade.v0.dto.IdentityDocument;
import com.bbva.pzic.risks.facade.v0.dto.SearchFinancialInformation;
import com.bbva.pzic.risks.facade.v0.mapper.ISearchRisksFinancialInformationV0Mapper;
import com.bbva.pzic.risks.util.mappers.Mapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.springframework.beans.factory.annotation.Autowired;

import static com.bbva.pzic.risks.util.Constants.AAP_PROPERTY;
import static com.bbva.pzic.risks.util.Constants.CALLING_CHANNEL_PROPERTY;
import static org.springframework.util.StringUtils.isEmpty;

@Mapper
public class SearchRisksFinancialInformationV0Mapper implements ISearchRisksFinancialInformationV0Mapper {

    private Translator translator;

    private ServiceInvocationContext serviceInvocationContext;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Autowired
    public void setServiceInvocationContext(ServiceInvocationContext serviceInvocationContext) {
        this.serviceInvocationContext = serviceInvocationContext;
    }

    @Override
    public InputSearchRisksFinancialInformation mapIn(final SearchFinancialInformation searchFinancialInformation) {
        InputSearchRisksFinancialInformation dtoInt = new InputSearchRisksFinancialInformation();
        dtoInt.setConsumerId(serviceInvocationContext.getProperty(AAP_PROPERTY));
        dtoInt.setCallingChannel(serviceInvocationContext.getProperty(CALLING_CHANNEL_PROPERTY));
        dtoInt.setIdentityDocument(mapInIdentityDocument(searchFinancialInformation.getIdentityDocument()));
        if (dtoInt.getIdentityDocument() == null || searchFinancialInformation.getCustomerId() != null) {
            DTOIntCustomer dtoIntCustomer = new DTOIntCustomer();
            dtoIntCustomer.setCustomerId(searchFinancialInformation.getCustomerId());
            dtoInt.setCustomer(dtoIntCustomer);
        }
        return dtoInt;
    }

    private DTOIntIdentityDocument mapInIdentityDocument(final IdentityDocument identityDocument) {
        if (identityDocument == null
                || (isEmpty(identityDocument.getDocumentNumber())
                && (identityDocument.getDocumentType() == null || isEmpty(identityDocument.getDocumentType().getId())))) {
            return null;
        }
        DTOIntIdentityDocument dtoIntIdentityDocument = new DTOIntIdentityDocument();
        dtoIntIdentityDocument.setDocumentNumber(identityDocument.getDocumentNumber());
        dtoIntIdentityDocument.setDocumentType(mapInDocumentType(identityDocument.getDocumentType()));
        return dtoIntIdentityDocument;
    }

    private DTOIntDocumentType mapInDocumentType(final DocumentType documentType) {
        if (documentType == null) {
            return null;
        }
        DTOIntDocumentType dtoIntDocumentType = new DTOIntDocumentType();
        dtoIntDocumentType.setId(translator.translateFrontendEnumValueStrictly("risks.riskFinancialInformation.documentType.id", documentType.getId()));
        return dtoIntDocumentType;
    }


    @Override
    public ServiceResponse<SearchFinancialInformation> mapOut(final SearchFinancialInformation searchFinancialInformation) {
        if (searchFinancialInformation == null) {
            return null;
        }
        return ServiceResponse.data(searchFinancialInformation).build();
    }
}
