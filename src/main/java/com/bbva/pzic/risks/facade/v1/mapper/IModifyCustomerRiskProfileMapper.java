package com.bbva.pzic.risks.facade.v1.mapper;

import com.bbva.pzic.risks.dao.model.blacklisted.ModelProfileRequest;
import com.bbva.pzic.risks.facade.v1.dto.Profile;

public interface IModifyCustomerRiskProfileMapper {

    ModelProfileRequest mapIn(String profileId, Profile profile);
}
