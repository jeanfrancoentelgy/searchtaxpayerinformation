package com.bbva.pzic.risks.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "Limits", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlType(name = "Limits", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Limits implements Serializable {

    private static final long serialVersionUID = 1L;

    private String group;

    private String range;

    private Limit limit;

    public String getGroup() {return group;}

    public void setGroup(String group) {this.group = group;}

    public String getRange() {return range;}

    public void setRange(String range) {this.range = range;}

    public Limit getLimit() {return limit;}

    public void setLimit(Limit limit) {this.limit = limit;}
}
