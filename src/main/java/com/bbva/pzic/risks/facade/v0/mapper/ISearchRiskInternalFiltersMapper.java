package com.bbva.pzic.risks.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.business.dto.InputSearchRiskInternalFilters;
import com.bbva.pzic.risks.facade.v0.dto.SearchRiskInternalFilters;

/**
 * Created on 15/06/2020.
 *
 * @author Entelgy.
 */
public interface ISearchRiskInternalFiltersMapper {

    InputSearchRiskInternalFilters mapIn(SearchRiskInternalFilters searchRiskInternalFilters);

    ServiceResponse<SearchRiskInternalFilters> mapOut(SearchRiskInternalFilters searchRiskInternalFilters);
}
