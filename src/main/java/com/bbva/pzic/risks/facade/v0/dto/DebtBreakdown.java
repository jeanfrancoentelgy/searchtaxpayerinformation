package com.bbva.pzic.risks.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "debtBreakdown", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlType(name = "debtBreakdown", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DebtBreakdown implements Serializable {

    private static final long serialVersionUID = 1L;

    private String month;

    private ProductType productType;

    private List<BankDebt> bankDebt;

    private Amount financialSystemAmount;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public List<BankDebt> getBankDebt() {
        return bankDebt;
    }

    public void setBankDebt(List<BankDebt> bankDebt) {
        this.bankDebt = bankDebt;
    }

    public Amount getFinancialSystemAmount() {
        return financialSystemAmount;
    }

    public void setFinancialSystemAmount(Amount financialSystemAmount) {
        this.financialSystemAmount = financialSystemAmount;
    }
}
