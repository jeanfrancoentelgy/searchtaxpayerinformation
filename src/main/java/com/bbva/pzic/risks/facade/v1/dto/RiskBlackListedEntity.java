package com.bbva.pzic.risks.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.Date;

@XmlRootElement(name = "riskBlackListedEntity", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlType(name = "riskBlackListedEntity", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class RiskBlackListedEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Indicates whether a person is blocked or not in the financial institution.
     */
    private Boolean isBlocked;

    /**
     * Information about person or company identification.
     */
    private IdentityDocument identityDocument;

    /**
     * String based on ISO-8601 date format to specify the date of registry of the person
     */
    private Date entryDate;

    public Boolean getBlocked() {
        return isBlocked;
    }

    public void setBlocked(Boolean blocked) {
        isBlocked = blocked;
    }

    public IdentityDocument getIdentityDocument() {
        return identityDocument;
    }

    public void setIdentityDocument(IdentityDocument identityDocument) {
        this.identityDocument = identityDocument;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }
}
