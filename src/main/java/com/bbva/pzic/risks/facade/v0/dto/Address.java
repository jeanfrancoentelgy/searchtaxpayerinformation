package com.bbva.pzic.risks.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 21/09/2020.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "address", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlType(name = "address", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Address implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Information about address of taxpayer.
     */
    private Location location;

    public Location getLocation() { return location; }

    public void setLocation(Location location) { this.location = location; }
}
