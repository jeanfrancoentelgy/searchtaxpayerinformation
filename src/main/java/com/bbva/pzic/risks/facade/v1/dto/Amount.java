package com.bbva.pzic.risks.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created on 19/05/2020.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "amount", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlType(name = "amount", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Amount implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Monetary amount.
     */
    private BigDecimal amount;
    /**
     * String based on ISO-4217 for specifying the currency.
     */
    private String currency;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
