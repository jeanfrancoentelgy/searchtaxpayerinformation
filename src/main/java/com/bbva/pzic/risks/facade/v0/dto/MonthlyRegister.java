package com.bbva.pzic.risks.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

@XmlRootElement(name = "monthlyRegister", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlType(name = "monthlyRegister", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class MonthlyRegister implements Serializable {

    private static final long serialVersionUID = 1L;

    private String month;

    private Product product;

    private Amount billingAmount;

    private BigDecimal annualEffectiveRate;

    private BigDecimal spread;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Amount getBillingAmount() {
        return billingAmount;
    }

    public void setBillingAmount(Amount billingAmount) {
        this.billingAmount = billingAmount;
    }

    public BigDecimal getAnnualEffectiveRate() {
        return annualEffectiveRate;
    }

    public void setAnnualEffectiveRate(BigDecimal annualEffectiveRate) {
        this.annualEffectiveRate = annualEffectiveRate;
    }

    public BigDecimal getSpread() {
        return spread;
    }

    public void setSpread(BigDecimal spread) {
        this.spread = spread;
    }
}
