package com.bbva.pzic.risks.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "internalCode", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlType(name = "internalCode", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class InternalCode implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * The identifier associated with the exclusion type internal code
     * ineligible.
     */
    private Integer id;
    /**
     * Subcode associated with the exclusion internal code.
     */
    private String subCode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubCode() {
        return subCode;
    }

    public void setSubCode(String subCode) {
        this.subCode = subCode;
    }
}
