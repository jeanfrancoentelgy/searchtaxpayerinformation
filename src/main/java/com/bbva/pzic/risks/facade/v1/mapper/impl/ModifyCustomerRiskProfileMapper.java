package com.bbva.pzic.risks.facade.v1.mapper.impl;

import com.bbva.pzic.risks.dao.model.blacklisted.ModelProfileRequest;
import com.bbva.pzic.risks.facade.v1.dto.Profile;
import com.bbva.pzic.risks.facade.v1.mapper.IModifyCustomerRiskProfileMapper;
import com.bbva.pzic.risks.util.mappers.EnumMapper;
import com.bbva.pzic.risks.util.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper
public class ModifyCustomerRiskProfileMapper implements IModifyCustomerRiskProfileMapper {

    @Autowired
    private EnumMapper enumMapper;

    @Override
    public ModelProfileRequest mapIn(String profileId, Profile profile) {
        ModelProfileRequest modelProfileRequest = new ModelProfileRequest();
        modelProfileRequest.setProfileId(profileId);
        modelProfileRequest.setRiskProfileType(enumMapper.getBackendValue("riskProfileType", profile.getRiskProfileType()));
        return modelProfileRequest;
    }
}
