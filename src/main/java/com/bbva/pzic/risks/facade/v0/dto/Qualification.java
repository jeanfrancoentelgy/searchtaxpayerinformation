package com.bbva.pzic.risks.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 15/06/2020.
 */
@XmlRootElement(name = "qualification", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlType(name = "qualification", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Qualification implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Risk qualification identifier.
     */
    private String id;
    /**
     * Result of the qualification.
     */
    private String result;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
