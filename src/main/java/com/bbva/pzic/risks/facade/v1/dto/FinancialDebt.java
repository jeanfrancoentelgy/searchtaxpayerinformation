package com.bbva.pzic.risks.facade.v1.dto;

import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Entelgy
 */
@XmlRootElement(name = "financialDebt", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlType(name = "financialDebt", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class FinancialDebt implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Financial debt identifier.
     */
    private String id;

    /**
     * Type of financial debt declared in the risks information agency by BBVA and the rest of financial entities.
     */
    private String financialDebtType;
    /**
     * Authorized monetary amount of the client in a financial system related to the type of financial debt declared by financial entities.
     */
    private Amount nonBBVAAuthorizedAmount;
    /**
     * Disposed monetary amount of the client in  financial system related to the type of financial debt declared by financial entities.
     */
    private Amount nonBBVADisposedAmount;
    /**
     * Authorized monetary amount of the client in BBVA related to the type of financial debt declared by BBVA.
     */
    private Amount authorizedAmount;
    /**
     * Disposed monetary amount of the client in BBVA related to the type of financial debt declared  by BBVA.
     */
    private Amount disposedAmount;
    /**
     * Percentage of amount authorized by the client in BBVA, (related to the type of financial debt declared), with respect to the amount he can have authorized in the financial system, including BBVA.
     */
    private Integer authorizedPercentage;
    /**
     * Percentage of amount disposed by the client in BBVA, (related to the type of financial debt declared), with respect to the amount he has disposed in the financial system, including BBVA.
     */
    private BigDecimal disposedPercentage;
    /**
     * String based on ISO-8601 timestamp format for specifying the deadline information in the risks information agency about of the customers or business’s financial debt.
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date reportingDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFinancialDebtType() {
        return financialDebtType;
    }

    public void setFinancialDebtType(String financialDebtType) {
        this.financialDebtType = financialDebtType;
    }

    public Amount getNonBBVAAuthorizedAmount() {
        return nonBBVAAuthorizedAmount;
    }

    public void setNonBBVAAuthorizedAmount(Amount nonBBVAAuthorizedAmount) {
        this.nonBBVAAuthorizedAmount = nonBBVAAuthorizedAmount;
    }

    public Amount getNonBBVADisposedAmount() {
        return nonBBVADisposedAmount;
    }

    public void setNonBBVADisposedAmount(Amount nonBBVADisposedAmount) {
        this.nonBBVADisposedAmount = nonBBVADisposedAmount;
    }

    public Amount getAuthorizedAmount() {
        return authorizedAmount;
    }

    public void setAuthorizedAmount(Amount authorizedAmount) {
        this.authorizedAmount = authorizedAmount;
    }

    public Amount getDisposedAmount() {
        return disposedAmount;
    }

    public void setDisposedAmount(Amount disposedAmount) {
        this.disposedAmount = disposedAmount;
    }

    public Integer getAuthorizedPercentage() {
        return authorizedPercentage;
    }

    public void setAuthorizedPercentage(Integer authorizedPercentage) {
        this.authorizedPercentage = authorizedPercentage;
    }

    public BigDecimal getDisposedPercentage() {
        return disposedPercentage;
    }

    public void setDisposedPercentage(BigDecimal disposedPercentage) {
        this.disposedPercentage = disposedPercentage;
    }

    public Date getReportingDate() {
        return reportingDate;
    }

    public void setReportingDate(Date reportingDate) {
        this.reportingDate = reportingDate;
    }
}
