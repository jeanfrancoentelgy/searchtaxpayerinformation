package com.bbva.pzic.risks.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

@XmlRootElement(name = "debtInformation", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlType(name = "debtInformation", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DebtInformation implements Serializable {

    private static final long serialVersionUID = 1L;

    private String month;

    private Amount bbvaAmount;

    private BigDecimal directFee;

    private Amount financialSystemAmount;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Amount getBbvaAmount() {
        return bbvaAmount;
    }

    public void setBbvaAmount(Amount bbvaAmount) {
        this.bbvaAmount = bbvaAmount;
    }

    public BigDecimal getDirectFee() {
        return directFee;
    }

    public void setDirectFee(BigDecimal directFee) {
        this.directFee = directFee;
    }

    public Amount getFinancialSystemAmount() {
        return financialSystemAmount;
    }

    public void setFinancialSystemAmount(Amount financialSystemAmount) {
        this.financialSystemAmount = financialSystemAmount;
    }
}
