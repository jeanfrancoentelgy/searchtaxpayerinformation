package com.bbva.pzic.risks.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "rates", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlType(name = "rates", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Rates implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Detail of each rate.
     */
    private List<ItemizeRate> itemizeRates;

    public List<ItemizeRate> getItemizeRates() {
        return itemizeRates;
    }

    public void setItemizeRates(List<ItemizeRate> itemizeRates) {
        this.itemizeRates = itemizeRates;
    }
}
