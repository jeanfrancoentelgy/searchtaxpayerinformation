package com.bbva.pzic.risks.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.business.dto.InputSearchRisksFinancialInformation;
import com.bbva.pzic.risks.facade.v0.dto.SearchFinancialInformation;

public interface ISearchRisksFinancialInformationV0Mapper {

    InputSearchRisksFinancialInformation mapIn(SearchFinancialInformation searchFinancialInformation);

    ServiceResponse<SearchFinancialInformation> mapOut(SearchFinancialInformation searchFinancialInformation);

}
