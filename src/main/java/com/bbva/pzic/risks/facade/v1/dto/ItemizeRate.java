package com.bbva.pzic.risks.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "itemizeRate", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlType(name = "itemizeRate", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ItemizeRate implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Rate type identifier.
     */
    private String rateType;
    /**
     * Detail mode in which the collection rate is applied.
     */
    private PercentageMode itemizeRateUnit;

    public String getRateType() {
        return rateType;
    }

    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    public PercentageMode getItemizeRateUnit() {
        return itemizeRateUnit;
    }

    public void setItemizeRateUnit(PercentageMode itemizeRateUnit) {
        this.itemizeRateUnit = itemizeRateUnit;
    }
}
