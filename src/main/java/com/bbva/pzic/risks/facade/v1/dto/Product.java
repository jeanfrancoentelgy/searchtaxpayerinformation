package com.bbva.pzic.risks.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "product", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlType(name = "product", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Financial product identifier. DISCLAIMER, It is only reported when the
     * type is financial commercial product.
     */
    private String id;
    /**
     * Financial product name.
     */
    private String name;
    /**
     * Flag that indicates if the product must be contracted of obligatory form
     * together with the main product of the offer. In case multiproduct offer
     * does not apply, is not necessary to inform
     */
    private Boolean isMandatory;
    /**
     * Financial product type.
     */
    private FinancialProductType financialProductType;
    /**
     * Amount of the offer expressed in different currencies.
     */
    private List<ProductAmount> productAmount;
    /**
     * Term of the offer related to the product.
     */
    private ProductTerm productTerm;
    /**
     * Identifier associated to the status of product.
     */
    private String status;
    /**
     * Information about the contracted products that belongs to a customer.
     * DISCLAIMER, It is only reported when the offer is directed to a
     * contracted product that the customer currently has, for example if the
     * type of financial product is associated with the offer of line increase
     * for the credit card, here the identifier of the contract of the customer
     * credit card.
     */
    private Contract contract;
    /**
     * Rate charge applied to the type of offer associated.
     */
    private Rates rates;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(Boolean isMandatory) {
        this.isMandatory = isMandatory;
    }

    public FinancialProductType getFinancialProductType() {
        return financialProductType;
    }

    public void setFinancialProductType(
            FinancialProductType financialProductType) {
        this.financialProductType = financialProductType;
    }

    public List<ProductAmount> getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(List<ProductAmount> productAmount) {
        this.productAmount = productAmount;
    }

    public ProductTerm getProductTerm() {
        return productTerm;
    }

    public void setProductTerm(ProductTerm productTerm) {
        this.productTerm = productTerm;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public Rates getRates() {
        return rates;
    }

    public void setRates(Rates rates) {
        this.rates = rates;
    }
}
