package com.bbva.pzic.risks.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created by Entelgy
 */
@XmlRootElement(name = "documentType", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlType(name = "documentType", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DocumentType implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Identity document type identifier.
     */
    private String id;

    /**
     * Description of the identity document type.
     */
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
