package com.bbva.pzic.risks.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "searchFinancialInformation", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlType(name = "searchFinancialInformation", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class SearchFinancialInformation implements Serializable {

    private static final long serialVersionUID = 1L;

    private IdentityDocument identityDocument;

    private String customerId;

    private List<ContractedProduct> contractedProducts;

    private DirectDebt directDebt;

    private List<MonthlyRegister> monthlyRegistation;

    private List<Spread> historicalSpread;

    private List<DebtInformation> totalDebtInformation;

    private List<DebtBreakdown> productDebtBreakdown;

    private AdditionalInfo additionalInfo;

    public IdentityDocument getIdentityDocument() {
        return identityDocument;
    }

    public void setIdentityDocument(IdentityDocument identityDocument) {
        this.identityDocument = identityDocument;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public List<ContractedProduct> getContractedProducts() {
        return contractedProducts;
    }

    public void setContractedProducts(List<ContractedProduct> contractedProducts) {
        this.contractedProducts = contractedProducts;
    }

    public DirectDebt getDirectDebt() {
        return directDebt;
    }

    public void setDirectDebt(DirectDebt directDebt) {
        this.directDebt = directDebt;
    }

    public List<MonthlyRegister> getMonthlyRegistation() {
        return monthlyRegistation;
    }

    public void setMonthlyRegistation(List<MonthlyRegister> monthlyRegistation) {
        this.monthlyRegistation = monthlyRegistation;
    }

    public List<Spread> getHistoricalSpread() {
        return historicalSpread;
    }

    public void setHistoricalSpread(List<Spread> historicalSpread) {
        this.historicalSpread = historicalSpread;
    }

    public List<DebtInformation> getTotalDebtInformation() {
        return totalDebtInformation;
    }

    public void setTotalDebtInformation(List<DebtInformation> totalDebtInformation) {
        this.totalDebtInformation = totalDebtInformation;
    }

    public List<DebtBreakdown> getProductDebtBreakdown() {
        return productDebtBreakdown;
    }

    public void setProductDebtBreakdown(List<DebtBreakdown> productDebtBreakdown) {
        this.productDebtBreakdown = productDebtBreakdown;
    }

    public AdditionalInfo getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(AdditionalInfo additionalInfo) {
        this.additionalInfo = additionalInfo;
    }
}
