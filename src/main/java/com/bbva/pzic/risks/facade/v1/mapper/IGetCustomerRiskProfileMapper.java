package com.bbva.pzic.risks.facade.v1.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.dao.model.profille.ModelCustomerRiskProfile;
import com.bbva.pzic.risks.facade.v1.dto.Profile;

public interface IGetCustomerRiskProfileMapper {

    ServiceResponse<Profile> mapOut(ModelCustomerRiskProfile customerRiskProfile);

}
