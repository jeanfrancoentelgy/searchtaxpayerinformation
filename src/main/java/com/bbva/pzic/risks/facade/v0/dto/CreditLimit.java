package com.bbva.pzic.risks.facade.v0.dto;

import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@XmlRootElement(name = "creditLimit", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlType(name = "creditLimit", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreditLimit implements Serializable {

    private static final long serialVersionUID = 1L;

    private Amount amount;

    private BigDecimal usedPercentage;

    private String contractNumber;

    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date dueDate;

    public Amount getAmount() {
        return amount;
    }

    public void setAmount(Amount amount) {
        this.amount = amount;
    }

    public BigDecimal getUsedPercentage() {
        return usedPercentage;
    }

    public void setUsedPercentage(BigDecimal usedPercentage) {
        this.usedPercentage = usedPercentage;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }
}
