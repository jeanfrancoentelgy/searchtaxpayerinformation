package com.bbva.pzic.risks.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 15/06/2020.
 */
@XmlRootElement(name = "searchRiskInternalFilters", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlType(name = "searchRiskInternalFilters", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class SearchRiskInternalFilters implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Identity document information related to the customer.
     */
    private IdentityDocument identityDocument;
    /**
     * It allows classifying a customer taking into account their level of indebtedness in the financial system.
     * This value is calculated according to the customer's credit history,
     * internal variables (provided by the bank BBVA) and external variables (provided by the Superintendency of Banking, Insurance and AFP) are used.
     */
    private String bureau;
    /**
     * Risk label assigned to a customer based on the potential risk they have in the financial system.
     * For example Exposed, Over Exposed, etc.
     */
    private String label;
    /**
     * Total number of fiqualificationsnancial entities in which the customer has a direct risk.
     * For example, in how many banks (including BBVA) have loans, credit cards, etc.
     */
    private Integer numberOfFinancialEntities;
    /**
     * Risk qualification for the customer. Risk qualification is an assessment of a customer ability to meet their contractual obligations.
     */
    private List<Qualification> qualifications;
    /**
     * Unique identifier of the customer.
     */
    private String customerId;

    public IdentityDocument getIdentityDocument() {
        return identityDocument;
    }

    public void setIdentityDocument(IdentityDocument identityDocument) {
        this.identityDocument = identityDocument;
    }

    public String getBureau() {
        return bureau;
    }

    public void setBureau(String bureau) {
        this.bureau = bureau;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getNumberOfFinancialEntities() {
        return numberOfFinancialEntities;
    }

    public void setNumberOfFinancialEntities(Integer numberOfFinancialEntities) {
        this.numberOfFinancialEntities = numberOfFinancialEntities;
    }

    public List<Qualification> getQualifications() {
        return qualifications;
    }

    public void setQualifications(List<Qualification> qualifications) {
        this.qualifications = qualifications;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}
