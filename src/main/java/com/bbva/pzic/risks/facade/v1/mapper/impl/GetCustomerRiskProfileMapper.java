package com.bbva.pzic.risks.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.dao.model.profille.ModelCustomerRiskProfile;
import com.bbva.pzic.risks.facade.v1.dto.Profile;
import com.bbva.pzic.risks.facade.v1.mapper.IGetCustomerRiskProfileMapper;
import com.bbva.pzic.risks.util.mappers.EnumMapper;
import com.bbva.pzic.risks.util.mappers.Mapper;
import com.bbva.pzic.risks.util.orika.MapperFactory;
import com.bbva.pzic.risks.util.orika.impl.ConfigurableMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper
public class GetCustomerRiskProfileMapper extends ConfigurableMapper implements IGetCustomerRiskProfileMapper {

    private static final Log LOG = LogFactory.getLog(GetCustomerRiskProfileMapper.class);

    private static final String STATUS = "status";
    private static final String RISK_PROFILE_TYPE = "riskProfileType";

    @Autowired
    private EnumMapper enumMapper;

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        factory.classMap(Profile.class, ModelCustomerRiskProfile.class)
                .field("id", "id")
                .field("name", "name")
                .field(RISK_PROFILE_TYPE, RISK_PROFILE_TYPE)
                .field("recalculationAttemptsLeft", "recalculationAttemptsLeft")
                .field(STATUS, STATUS)
                .register();
    }

    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponse<Profile> mapOut(ModelCustomerRiskProfile customerRiskProfile) {
        LOG.info("... called method GetCustomerRiskProfileMapper.mapOut ...");

        if (customerRiskProfile == null) {
            return null;
        }

        Profile profile = map(customerRiskProfile, Profile.class);

        if (customerRiskProfile.getRiskProfileType() != null) {
            profile.setRiskProfileType(enumMapper.getEnumValue(RISK_PROFILE_TYPE, customerRiskProfile.getRiskProfileType()));
        }

        if (customerRiskProfile.getStatus() != null) {
            profile.setStatus(enumMapper.getEnumValue(STATUS, customerRiskProfile.getStatus()));
        }

        return ServiceResponse.data(profile).build();
    }
}
