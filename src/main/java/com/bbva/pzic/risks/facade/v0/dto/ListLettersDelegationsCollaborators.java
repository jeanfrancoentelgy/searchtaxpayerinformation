package com.bbva.pzic.risks.facade.v0.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "listLettersDelegationsCollaborators", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlType(name = "listLettersDelegationsCollaborators", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ListLettersDelegationsCollaborators implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    @DatoAuditable(omitir = true)
    private String fullName;

    private Role role;

    private Bank bank;

    private String delegationId;

    private List<EconomicGroupLimits> economicGroupLimits;

    private List<LimitsGaranteed> limitsGaranteed;

    private List<ProductsLimit> productsLimits;

    public String getId() {return id;}

    public void setId(String id) {this.id = id;}

    public String getFullName() {return fullName;}

    public void setFullName(String fullName) {this.fullName = fullName;}

    public Role getRole() {return role;}

    public void setRole(Role role) {this.role = role;}

    public Bank getBank() {return bank;}

    public void setBank(Bank bank) {this.bank = bank;}

    public String getDelegationId() {return delegationId;}

    public void setDelegationId(String delegationId) {this.delegationId = delegationId;}

    public List<EconomicGroupLimits> getEconomicGroupLimits() {return economicGroupLimits;}

    public void setEconomicGroupLimits(List<EconomicGroupLimits> economicGroupLimits) {this.economicGroupLimits = economicGroupLimits;}

    public List<ProductsLimit> getProductsLimits() {return productsLimits;}

    public void setProductsLimits(List<ProductsLimit> productsLimits) {this.productsLimits = productsLimits;}

    public List<LimitsGaranteed> getLimitsGaranteed() {
        return limitsGaranteed;
    }

    public void setLimitsGaranteed(List<LimitsGaranteed> limitsGaranteed) {
        this.limitsGaranteed = limitsGaranteed;
    }
}
