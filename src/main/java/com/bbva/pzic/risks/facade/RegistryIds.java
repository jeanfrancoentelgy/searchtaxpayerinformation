package com.bbva.pzic.risks.facade;

/**
 * @author Entelgy
 */
public final class RegistryIds {

    public static final String SMC_REGISTRY_ID_OF_LIST_RISK_BLACK_LISTED_ENTITIES = "SMCPE1810361";
    public static final String SMC_REGISTRY_ID_OF_GET_CUSTOMER_RISK_PROFILE = "SMCPE1810384";
    public static final String SMC_REGISTRY_ID_OF_MODIFY_CUSTOMER_RISK_PROFILE = "SMCPE1810385";
    public static final String SMC_REGISTRY_ID_OF_LIST_RISK_FINANCIAL_DEBTS = "SMCPE2010187";
    public static final String SMC_REGISTRY_ID_OF_SEARCH_RISK_INTERNAL_FILTERS = "SMGG20200195";
    public static final String SMC_REGISTRY_ID_OF_LIST_RISK_OFFERS = "SMCPE2010196";
    public static final String SMC_REGISTRY_ID_OF_LIST_RISK_EXCLUSIONS = "SMCPE2010197";
    public static final String SMC_REGISTRY_ID_OF_SEARCH_RISKS_FINANCIAL_INFORMATION = "SMGG20200450";
    public static final String SMC_REGISTRY_ID_OF_LIST_LETTERS_DELEGATIONS_COLLABORATORS = "SMGG20200564";
    public static final String SMC_REGISTRY_ID_OF_SEARCH_TAXPAYER_INFORMATION = "SMGG20200622";

    private RegistryIds() {
    }
}
