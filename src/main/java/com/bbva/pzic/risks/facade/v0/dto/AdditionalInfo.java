package com.bbva.pzic.risks.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "additionalInfo", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlType(name = "additionalInfo", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class AdditionalInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String bureauCode;

    private Branch branch;

    private BusinessAgent businessAgent;

    private String rating;

    private String economicGroup;

    private String entailmentCode;

    private Boolean isPaymentHolder;

    private List<CreditLimit> creditLimit;

    private Segment segment;

    public String getBureauCode() {
        return bureauCode;
    }

    public void setBureauCode(String bureauCode) {
        this.bureauCode = bureauCode;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public BusinessAgent getBusinessAgent() {
        return businessAgent;
    }

    public void setBusinessAgent(BusinessAgent businessAgent) {
        this.businessAgent = businessAgent;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getEconomicGroup() {
        return economicGroup;
    }

    public void setEconomicGroup(String economicGroup) {
        this.economicGroup = economicGroup;
    }

    public String getEntailmentCode() {
        return entailmentCode;
    }

    public void setEntailmentCode(String entailmentCode) {
        this.entailmentCode = entailmentCode;
    }

    public Boolean getIsPaymentHolder() {
        return isPaymentHolder;
    }

    public void setIsPaymentHolder(Boolean paymentHolder) {
        isPaymentHolder = paymentHolder;
    }

    public List<CreditLimit> getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(List<CreditLimit> creditLimit) {
        this.creditLimit = creditLimit;
    }

    public Segment getSegment() {
        return segment;
    }

    public void setSegment(Segment segment) {
        this.segment = segment;
    }
}
