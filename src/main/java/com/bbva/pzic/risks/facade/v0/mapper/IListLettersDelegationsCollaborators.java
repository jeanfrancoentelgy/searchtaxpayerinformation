package com.bbva.pzic.risks.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.business.dto.InputListLettersDelegationsCollaborators;
import com.bbva.pzic.risks.facade.v0.dto.ListLettersDelegationsCollaborators;

import java.util.List;

public interface IListLettersDelegationsCollaborators {

    InputListLettersDelegationsCollaborators mapIn(String employeeId, String branchId);

    ServiceResponse<List<ListLettersDelegationsCollaborators>> mapOut(List<ListLettersDelegationsCollaborators> listLettersDelegationsCollaborators);
}
