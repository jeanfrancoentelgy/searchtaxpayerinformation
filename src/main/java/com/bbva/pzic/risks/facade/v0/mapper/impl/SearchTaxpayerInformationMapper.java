package com.bbva.pzic.risks.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.risks.business.dto.InputSearchTaxpayerInformation;
import com.bbva.pzic.risks.facade.v0.dto.SearchTaxpayer;
import com.bbva.pzic.risks.facade.v0.mapper.ISearchTaxpayerInformationMapper;
import com.bbva.pzic.risks.util.mappers.Mapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created on 21/09/2020.
 *
 * @author Entelgy.
 */
@Mapper
public class SearchTaxpayerInformationMapper implements ISearchTaxpayerInformationMapper {

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public InputSearchTaxpayerInformation mapIn(final SearchTaxpayer searchTaxpayer) {
        if (searchTaxpayer == null){
            return null;
        }
        InputSearchTaxpayerInformation input = new InputSearchTaxpayerInformation();
        input.setDocumentType(translator.translateFrontendEnumValueStrictly("risks.taxpayerInformation.search.documentType.id", searchTaxpayer.getDocumentType()));
        input.setDocumentNumber(searchTaxpayer.getDocumentNumber());
        return input;
    }

    @Override
    public ServiceResponse<SearchTaxpayer> mapOut(final SearchTaxpayer searchTaxpayer) {
        if (searchTaxpayer == null) {
            return null;
        }
        return ServiceResponse.data(searchTaxpayer).build();
    }
}
