package com.bbva.pzic.risks.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.business.dto.InputListRiskExclusions;
import com.bbva.pzic.risks.facade.v1.dto.Exclusion;
import com.bbva.pzic.risks.facade.v1.mapper.IListRiskExclusionsMapper;
import com.bbva.pzic.risks.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/**
 * Created on 17/06/2020.
 *
 * @author Entelgy
 */
@Mapper
public class ListRiskExclusionsMapper implements IListRiskExclusionsMapper {

    private static final Log LOG = LogFactory.getLog(ListRiskExclusionsMapper.class);

    @Override
    public InputListRiskExclusions mapIn(final String customerId) {
        LOG.info("... called method ListRiskExclusionsMapper.mapIn ...");
        InputListRiskExclusions input = new InputListRiskExclusions();
        input.setCustomerId(customerId);
        return input;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ServiceResponse<List<Exclusion>> mapOut(final List<Exclusion> exclusion) {
        LOG.info("... called method ListRiskExclusionsMapper.mapOut ...");
        if (exclusion.isEmpty()) {
            return null;
        }
        return ServiceResponse.data(exclusion).build();
    }
}
