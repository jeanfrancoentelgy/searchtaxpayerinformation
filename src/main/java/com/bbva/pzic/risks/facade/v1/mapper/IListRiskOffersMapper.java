package com.bbva.pzic.risks.facade.v1.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.business.dto.InputListRiskOffers;
import com.bbva.pzic.risks.facade.v1.dto.RiskOffer;

import java.util.List;

/**
 * Created on 12/06/2020.
 *
 * @author Entelgy
 */
public interface IListRiskOffersMapper {

    InputListRiskOffers mapIn(String customerId);

    ServiceResponse<List<RiskOffer>> mapOut(List<RiskOffer> riskOffer);
}
