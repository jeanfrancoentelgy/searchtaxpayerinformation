package com.bbva.pzic.risks.facade.v1;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.facade.v1.dto.*;

import java.util.List;

/**
 * @author Entelgy
 */
public interface ISrvRisksV1 {

    ServiceResponse<List<RiskBlackListedEntity>> listRiskBlackListedEntities(String documentType, String documentNumber);

    ServiceResponse<Profile> getCustomerRiskProfile(String customerId);

    void modifyCustomerRiskProfile(String profileId, Profile profile);

    ServiceResponse<List<FinancialDebt>> listRiskFinancialDebts(String customerId);

    ServiceResponse<List<RiskOffer>> listRiskOffers(String customerId);

    ServiceResponse<List<Exclusion>> listRiskExclusions(String customerId);
}
