package com.bbva.pzic.risks.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "riskOffer", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlType(name = "riskOffer", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class RiskOffer implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Unique offer identifier.
     */
    private String id;
    /**
     * Identifier of characteristics of the associated offer.
     */
    private String offerType;
    /**
     * It indicates at which customer segment it refers the offer
     */
    private String riskSegment;
    /**
     * Determines the interval time in which the offer will be available.
     */
    private Period period;
    /**
     * Current status of the actual offer.
     */
    private String status;
    /**
     * It indicates at which risk segment it refers the offer, this value is
     * calculated based on different variables managed by the risk area. For
     * example, rate segment, payment to the owner, type of Offer, risk segment,
     * etc.
     */
    private String riskOfferSegment;
    /**
     * Product information being offered.
     */
    private List<Product> products;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public String getRiskSegment() {
        return riskSegment;
    }

    public void setRiskSegment(String riskSegment) {
        this.riskSegment = riskSegment;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRiskOfferSegment() {
        return riskOfferSegment;
    }

    public void setRiskOfferSegment(String riskOfferSegment) {
        this.riskOfferSegment = riskOfferSegment;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
