package com.bbva.pzic.risks.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "MaximumTerm", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlType(name = "MaximumTerm", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class MaximumTerm implements Serializable {

    private static final long serialVersionUID = 1L;

    private String termType;

    private Integer term;

    public String getTermType() {return termType;}

    public void setTermType(String termType) {this.termType = termType;}

    public Integer getTerm() {return term;}

    public void setTerm(Integer term) {this.term = term;}
}
