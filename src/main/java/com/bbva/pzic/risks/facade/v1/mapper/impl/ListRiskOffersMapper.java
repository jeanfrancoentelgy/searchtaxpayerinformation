package com.bbva.pzic.risks.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.business.dto.InputListRiskOffers;
import com.bbva.pzic.risks.facade.v1.dto.RiskOffer;
import com.bbva.pzic.risks.facade.v1.mapper.IListRiskOffersMapper;
import com.bbva.pzic.risks.util.mappers.Mapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/**
 * Created on 12/06/2020.
 *
 * @author Entelgy
 */
@Mapper
public class ListRiskOffersMapper implements IListRiskOffersMapper {

    private static final Log LOG = LogFactory.getLog(ListRiskOffersMapper.class);

    @Override
    public InputListRiskOffers mapIn(final String customerId) {
        LOG.info("... called method ListRiskOffersMapper.mapIn ...");
        InputListRiskOffers input = new InputListRiskOffers();
        input.setCustomerId(customerId);
        return input;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ServiceResponse<List<RiskOffer>> mapOut(final List<RiskOffer> riskOffer) {
        LOG.info("... called method ListRiskOffersMapper.mapOut ...");
        if (CollectionUtils.isEmpty(riskOffer)) {
            return null;
        }
        return ServiceResponse.data(riskOffer).pagination(null).build();
    }
}
