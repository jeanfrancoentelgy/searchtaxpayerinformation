package com.bbva.pzic.risks.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "productsLimit", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlType(name = "productsLimit", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductsLimit implements Serializable {

    private static final long serialVersionUID = 1L;

    private Product product;

    private List<Limits> limits;

    public Product getProduct() {return product;}

    public void setProduct(Product product) {this.product = product;}

    public List<Limits> getLimits() {return limits;}

    public void setLimits(List<Limits> limits) {this.limits = limits;}
}
