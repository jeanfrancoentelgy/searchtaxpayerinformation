package com.bbva.pzic.risks.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

@XmlRootElement(name = "bankDebt", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlType(name = "bankDebt", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class BankDebt implements Serializable {

    private static final long serialVersionUID = 1L;

    private String bankName;

    private Amount amount;

    private BigDecimal directFee;

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Amount getAmount() {
        return amount;
    }

    public void setAmount(Amount amount) {
        this.amount = amount;
    }

    public BigDecimal getDirectFee() {
        return directFee;
    }

    public void setDirectFee(BigDecimal directFee) {
        this.directFee = directFee;
    }
}
