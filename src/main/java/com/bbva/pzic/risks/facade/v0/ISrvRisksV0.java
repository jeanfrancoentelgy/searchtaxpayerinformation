package com.bbva.pzic.risks.facade.v0;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.facade.v0.dto.ListLettersDelegationsCollaborators;
import com.bbva.pzic.risks.facade.v0.dto.SearchFinancialInformation;
import com.bbva.pzic.risks.facade.v0.dto.SearchRiskInternalFilters;
import com.bbva.pzic.risks.facade.v0.dto.SearchTaxpayer;

import java.util.List;

/**
 * Created on 15/06/2020.
 *
 * @author Entelgy.
 */
public interface ISrvRisksV0 {

    ServiceResponse<SearchRiskInternalFilters> searchRiskInternalFilters(SearchRiskInternalFilters searchRiskInternalFilters);

    ServiceResponse<SearchFinancialInformation> searchRisksFinancialInformation(SearchFinancialInformation searchFinancialInformation);

    ServiceResponse<List<ListLettersDelegationsCollaborators>> listLettersDelegationsCollaborators(String employeeId, String branchId);

    ServiceResponse<SearchTaxpayer> searchTaxpayerInformation(SearchTaxpayer searchTaxpayerInformation);
}
