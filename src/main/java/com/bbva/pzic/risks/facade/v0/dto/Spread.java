package com.bbva.pzic.risks.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

@XmlRootElement(name = "spread", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlType(name = "spread", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Spread implements Serializable {

    private static final long serialVersionUID = 1L;

    private String initialMonth;

    private String finalMonth;

    private Amount billingAmount;

    private BigDecimal annualEffectiveRate;

    private BigDecimal spread;

    public String getInitialMonth() {
        return initialMonth;
    }

    public void setInitialMonth(String initialMonth) {
        this.initialMonth = initialMonth;
    }

    public String getFinalMonth() {
        return finalMonth;
    }

    public void setFinalMonth(String finalMonth) {
        this.finalMonth = finalMonth;
    }

    public Amount getBillingAmount() {
        return billingAmount;
    }

    public void setBillingAmount(Amount billingAmount) {
        this.billingAmount = billingAmount;
    }

    public BigDecimal getAnnualEffectiveRate() {
        return annualEffectiveRate;
    }

    public void setAnnualEffectiveRate(BigDecimal annualEffectiveRate) {
        this.annualEffectiveRate = annualEffectiveRate;
    }

    public BigDecimal getSpread() {
        return spread;
    }

    public void setSpread(BigDecimal spread) {
        this.spread = spread;
    }
}
