package com.bbva.pzic.risks.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.BackendContext;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.risks.business.dto.InputListRiskFinancialDebts;
import com.bbva.pzic.risks.facade.v1.dto.FinancialDebt;
import com.bbva.pzic.risks.facade.v1.mapper.IListRiskFinancialDebtsMapper;
import com.bbva.pzic.risks.util.mappers.Mapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import java.util.List;

/**
 * Created on 19/05/2020.
 *
 * @author Entelgy.
 */
@Mapper
public class ListRiskFinancialDebtsMapper implements IListRiskFinancialDebtsMapper {

    private final ServiceInvocationContext serviceInvocationContext;

    public ListRiskFinancialDebtsMapper(ServiceInvocationContext serviceInvocationContext) {
        this.serviceInvocationContext = serviceInvocationContext;
    }


    @Override
    public InputListRiskFinancialDebts mapIn(final String customerId) {
        InputListRiskFinancialDebts input = new InputListRiskFinancialDebts();
        String clientId = serviceInvocationContext.getProperty(BackendContext.ASTA_MX_CLIENT_ID);
        if (StringUtils.isEmpty(clientId)) {
            input.setCustomerId(customerId);
        } else {
            input.setCustomerId(clientId);
        }
        return input;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ServiceResponse<List<FinancialDebt>> mapOut(final List<FinancialDebt> financialDebts) {
        if (CollectionUtils.isEmpty(financialDebts)) {
            return null;
        }

        return ServiceResponse.data(financialDebts).build();
    }
}
