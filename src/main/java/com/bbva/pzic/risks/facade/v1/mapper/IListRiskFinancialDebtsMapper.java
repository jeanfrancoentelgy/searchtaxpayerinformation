package com.bbva.pzic.risks.facade.v1.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.business.dto.InputListRiskFinancialDebts;
import com.bbva.pzic.risks.facade.v1.dto.FinancialDebt;

import java.util.List;

/**
 * Created on 19/05/2020.
 *
 * @author Entelgy.
 */
public interface IListRiskFinancialDebtsMapper {

    InputListRiskFinancialDebts mapIn(String customerId);

    ServiceResponse<List<FinancialDebt>> mapOut(List<FinancialDebt> listRiskFinancialDebts);
}
