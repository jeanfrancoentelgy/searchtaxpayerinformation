package com.bbva.pzic.risks.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 21/09/2020.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "economicActivity", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlType(name = "economicActivity", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class EconomicActivity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Economic activity identifier.
     */
    private String id;

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }
}
