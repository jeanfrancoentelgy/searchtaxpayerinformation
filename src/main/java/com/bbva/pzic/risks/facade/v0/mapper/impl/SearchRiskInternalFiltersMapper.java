package com.bbva.pzic.risks.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.business.dto.InputSearchRiskInternalFilters;
import com.bbva.pzic.risks.facade.v0.dto.IdentityDocument;
import com.bbva.pzic.risks.facade.v0.dto.SearchRiskInternalFilters;
import com.bbva.pzic.risks.facade.v0.mapper.ISearchRiskInternalFiltersMapper;
import com.bbva.pzic.risks.util.mappers.Mapper;
import org.apache.commons.lang.StringUtils;

/**
 * Created on 15/06/2020.
 *
 * @author Entelgy.
 */
@Mapper
public class SearchRiskInternalFiltersMapper implements ISearchRiskInternalFiltersMapper {

    @Override
    public InputSearchRiskInternalFilters mapIn(final SearchRiskInternalFilters searchRiskInternalFilters) {
        InputSearchRiskInternalFilters input = new InputSearchRiskInternalFilters();
        mapInIdentityDocument(input, searchRiskInternalFilters.getIdentityDocument());
        input.setCustomerId(searchRiskInternalFilters.getCustomerId());
        return input;
    }

    private void mapInIdentityDocument(final InputSearchRiskInternalFilters input, final IdentityDocument identityDocument) {
        if (identityDocument == null) {
            return;
        }

        input.setDocumentNumber(identityDocument.getDocumentNumber());
        if (identityDocument.getDocumentType() != null && StringUtils.isNotEmpty(identityDocument.getDocumentType().getId())) {
            input.setDocumentTypeId(identityDocument.getDocumentType().getId());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponse<SearchRiskInternalFilters> mapOut(final SearchRiskInternalFilters searchRiskInternalFilters) {
        if (searchRiskInternalFilters == null) {
            return null;
        }

        return ServiceResponse.data(searchRiskInternalFilters).build();
    }
}
