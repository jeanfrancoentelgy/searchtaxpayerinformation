package com.bbva.pzic.risks.facade.v1.impl;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.annotations.PATCH;
import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;
import com.bbva.jee.arq.spring.core.servicing.utils.ContextAware;
import com.bbva.pzic.risks.business.ISrvIntRisksV1;
import com.bbva.pzic.risks.facade.RegistryIds;
import com.bbva.pzic.risks.facade.v1.ISrvRisksV1;
import com.bbva.pzic.risks.facade.v1.dto.*;
import com.bbva.pzic.risks.facade.v1.mapper.*;
import com.bbva.pzic.routine.processing.data.DataProcessingExecutor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.bbva.pzic.risks.facade.RegistryIds.*;

@Path("/v1")
@SN(registryID = "SNPE1800104", logicalID = "risks")
@VN(vnn = "v1")
@Produces(MediaType.APPLICATION_JSON)
@Service
public class SrvRisksV1 implements ISrvRisksV1, ContextAware {

    private static final Log LOG = LogFactory.getLog(SrvRisksV1.class);

    private static final String CUSTOMER_ID = "customerId";

    public UriInfo uriInfo;
    public HttpHeaders httpHeaders;

    @Autowired
    private IListRiskBlackListedEntitiesMapper listRiskBlackListedEntitiesMapper;

    @Autowired
    private IGetCustomerRiskProfileMapper getCustomerRiskProfileMapper;

    @Autowired
    private IModifyCustomerRiskProfileMapper modifyCustomerRiskProfileMapper;

    @Autowired
    private IListRiskFinancialDebtsMapper listRiskFinancialDebtsMapper;

    @Autowired
    private IListRiskOffersMapper listRiskOffersMapper;

    @Autowired
    private IListRiskExclusionsMapper listRiskExclusionsMapper;

    @Autowired
    private ISrvIntRisksV1 srvIntRisksV1;

    @Autowired
    private DataProcessingExecutor inputDataProcessingExecutor;
    @Autowired
    private DataProcessingExecutor outputDataProcessingExecutor;

    @Override
    public void setUriInfo(UriInfo uriInfo) {
        this.uriInfo = uriInfo;
    }

    @Override
    public void setHttpHeaders(HttpHeaders httpHeaders) {
        this.httpHeaders = httpHeaders;
    }

    @Override
    @GET
    @Path("/black-listed-entities")
    @SMC(registryID = SMC_REGISTRY_ID_OF_LIST_RISK_BLACK_LISTED_ENTITIES, logicalID = "listRiskBlackListedEntities")
    public ServiceResponse<List<RiskBlackListedEntity>> listRiskBlackListedEntities(
            @QueryParam("identityDocument.documentType.id") final String documentType,
            @QueryParam("identityDocument.documentNumber") final String documentNumber) {

        LOG.info("----- Invoking service listRiskBlackListedEntities -----");
        Map<String, Object> queryParam = new HashMap<>();
        queryParam.put("identityDocument.documentType.id", documentType);
        queryParam.put("identityDocument.documentNumber", documentNumber);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_RISK_BLACK_LISTED_ENTITIES, null, null, queryParam);
        ServiceResponse<List<RiskBlackListedEntity>> serviceResponse = listRiskBlackListedEntitiesMapper.mapOut(
                srvIntRisksV1.listRiskBlackListedEntities(
                        listRiskBlackListedEntitiesMapper.mapIn(
                                (String) queryParam.get("identityDocument.documentType.id"),
                                (String) queryParam.get("identityDocument.documentNumber"))));
        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_RISK_BLACK_LISTED_ENTITIES, serviceResponse, null, null);
        return serviceResponse;
    }

    @Override
    @GET
    @Path("/profile")
    @SMC(registryID = SMC_REGISTRY_ID_OF_GET_CUSTOMER_RISK_PROFILE, logicalID = "getCustomerRiskProfile")
    public ServiceResponse<Profile> getCustomerRiskProfile(@QueryParam(CUSTOMER_ID) final String customerId) {

        LOG.info("----- Invoking service getCustomerRiskProfile -----");
        Map<String, Object> queryParam = new HashMap<>();
        queryParam.put(CUSTOMER_ID, customerId);
        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_CUSTOMER_RISK_PROFILE, null, null, queryParam);
        ServiceResponse<Profile> serviceResponse = getCustomerRiskProfileMapper.mapOut(srvIntRisksV1.getCustomerRiskProfile(
                (String) queryParam.get(CUSTOMER_ID)
        ));
        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_CUSTOMER_RISK_PROFILE, serviceResponse, null, null);
        return serviceResponse;
    }

    @Override
    @PATCH
    @Path("/profile/{profile-id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_MODIFY_CUSTOMER_RISK_PROFILE, logicalID = "modifyCustomerRiskProfile")
    public void modifyCustomerRiskProfile(@PathParam("profile-id") String profileId,
                                          @RequestBody Profile profile) {

        LOG.info("----- Invoking service modifyCustomerRiskProfile -----");
        Map<String, Object> pathParam = new HashMap<>();
        pathParam.put("profile-id", profileId);
        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_MODIFY_CUSTOMER_RISK_PROFILE, profile, null, pathParam);
        srvIntRisksV1.modifyCustomerRiskProfile(modifyCustomerRiskProfileMapper.mapIn(
                (String) pathParam.get("profile-id"),
                profile));
    }

    @Override
    @GET
    @Path("/financial-debts")
    @SMC(registryID = RegistryIds.SMC_REGISTRY_ID_OF_LIST_RISK_FINANCIAL_DEBTS, logicalID = "listRiskFinancialDebts", forcedCatalog = "gabiCatalog")
    public ServiceResponse<List<FinancialDebt>> listRiskFinancialDebts(@QueryParam(CUSTOMER_ID) final String customerId) {
        LOG.info("----- Invoking service listRiskFinancialDebts -----");

        Map<String, Object> queryParam = new HashMap<>();
        queryParam.put(CUSTOMER_ID, customerId);

        inputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_LIST_RISK_FINANCIAL_DEBTS, null, null, queryParam);

        ServiceResponse<List<FinancialDebt>> serviceResponse = listRiskFinancialDebtsMapper.mapOut(
                srvIntRisksV1.listRiskFinancialDebts(
                        listRiskFinancialDebtsMapper.mapIn((String) queryParam.get(CUSTOMER_ID))));

        outputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_LIST_RISK_FINANCIAL_DEBTS, serviceResponse, null, null);

        return serviceResponse;
    }

    @Override
    @GET
    @Path("/risk-offers")
    @SMC(registryID = SMC_REGISTRY_ID_OF_LIST_RISK_OFFERS, logicalID = "listRiskOffers", forcedCatalog = "gabiCatalog")
    public ServiceResponse<List<RiskOffer>> listRiskOffers(
            @DatoAuditable(omitir = true) @QueryParam(CUSTOMER_ID) final String customerId) {
        LOG.info("----- Invoking service listRiskOffers -----");

        Map<String, Object> queryParam = new HashMap<>();
        queryParam.put(CUSTOMER_ID, customerId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_RISK_OFFERS, null, queryParam, null);

        ServiceResponse<List<RiskOffer>> serviceResponse = listRiskOffersMapper.mapOut(
                srvIntRisksV1.listRiskOffers(
                        listRiskOffersMapper.mapIn(
                                (String) queryParam.get(CUSTOMER_ID))));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_RISK_OFFERS, serviceResponse, null, null);

        return serviceResponse;
    }

    @Override
    @GET
    @Path("/exclusions")
    @SMC(registryID = SMC_REGISTRY_ID_OF_LIST_RISK_EXCLUSIONS, logicalID = "listRiskExclusions", forcedCatalog = "gabiCatalog")
    public ServiceResponse<List<Exclusion>> listRiskExclusions(
            @DatoAuditable(omitir = true) @QueryParam("customerId") final String customerId) {
        LOG.info("----- Invoking service listRiskExclusions -----");

        Map<String, Object> queryParam = new HashMap<>();
        queryParam.put(CUSTOMER_ID, customerId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_RISK_EXCLUSIONS, null, queryParam, null);

        ServiceResponse<List<Exclusion>> serviceResponse = listRiskExclusionsMapper.mapOut(
                srvIntRisksV1.listRiskExclusions(
                        listRiskExclusionsMapper.mapIn((String) queryParam.get(CUSTOMER_ID))));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_RISK_EXCLUSIONS, serviceResponse, null, null);

        return serviceResponse;
    }
}
