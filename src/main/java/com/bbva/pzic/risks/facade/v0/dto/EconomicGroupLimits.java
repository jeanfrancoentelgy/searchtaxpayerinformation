package com.bbva.pzic.risks.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "economicGroupLimits", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlType(name = "economicGroupLimits", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class EconomicGroupLimits implements Serializable {

    private static final long serialVersionUID = 1L;

    private String group;

    private Limit limit;

    private String delegationLevel;

    public String getGroup(){return group;}

    public void setGroup(String group) {this.group = group;}

    public Limit getLimit() {return limit;}

    public void setLimit(Limit limit) {this.limit = limit;}

    public String getDelegationLevel() {return delegationLevel;}

    public void setDelegationLevel(String delegationLevel) {this.delegationLevel = delegationLevel;}
}
