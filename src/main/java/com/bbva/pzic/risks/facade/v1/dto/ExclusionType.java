package com.bbva.pzic.risks.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "exclusionType", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlType(name = "exclusionType", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExclusionType implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Identifier of the exclusion.
     */
    private String id;
    /**
     * Description of the exclusion.
     */
    private String name;
    /**
     * Associated with the type of exclusion that needs to be classified in more
     * detail. DISCLAIMER, In case of using the INELIGIBLE type, this attribute
     * must be forcibly informed with the information that talks about which
     * kind of exclusion is being shown.
     */
    private InternalCode internalCode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public InternalCode getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(InternalCode internalCode) {
        this.internalCode = internalCode;
    }
}
