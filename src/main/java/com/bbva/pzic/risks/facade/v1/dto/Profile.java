package com.bbva.pzic.risks.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "profile", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlType(name = "profile", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Profile implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Identity document type identifier.
     */
    private String id;

    /**
     * Description of the identity document type.
     */
    private String name;
    /**
     * A risk profile is an evaluation of an individual business willingness to take risks. A risk profile is important for determining a proper investment asset allocation for a portfolio, among other things.
     */
    private Long recalculationAttemptsLeft;
    /**
     * Risk profile type.
     */
    private String riskProfileType;

    /**
     * Profile status. The profile status indicates if the customer agrees with the questionnaire evaluation. If no, the status remains inactive. On the contrary, it change to active.
     */
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getRecalculationAttemptsLeft() {
        return recalculationAttemptsLeft;
    }

    public void setRecalculationAttemptsLeft(Long recalculationAttemptsLeft) {
        this.recalculationAttemptsLeft = recalculationAttemptsLeft;
    }

    public String getRiskProfileType() {
        return riskProfileType;
    }

    public void setRiskProfileType(String riskProfileType) {
        this.riskProfileType = riskProfileType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
