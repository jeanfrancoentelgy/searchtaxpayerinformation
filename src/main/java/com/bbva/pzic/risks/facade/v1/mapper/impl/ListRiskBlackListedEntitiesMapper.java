package com.bbva.pzic.risks.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.dao.model.blacklisted.ModelClientRequest;
import com.bbva.pzic.risks.facade.v1.dto.RiskBlackListedEntity;
import com.bbva.pzic.risks.facade.v1.mapper.IListRiskBlackListedEntitiesMapper;
import com.bbva.pzic.risks.util.mappers.EnumMapper;
import com.bbva.pzic.risks.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper
public class ListRiskBlackListedEntitiesMapper implements IListRiskBlackListedEntitiesMapper {

    private static final Log LOG = LogFactory.getLog(ListRiskBlackListedEntitiesMapper.class);

    @Autowired
    private EnumMapper enumMapper;

    @Override
    public ModelClientRequest mapIn(final String documentType, final String documentNumber) {
        LOG.info("... called method ListRiskBlackListedEntitiesMapper.mapIn ...");

        ModelClientRequest request = new ModelClientRequest();
        request.setTipdocumento(enumMapper.getBackendValue("documentType.id", documentType));
        request.setNumdocumento(documentNumber);

        return request;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ServiceResponse<List<RiskBlackListedEntity>> mapOut(final List<RiskBlackListedEntity> data) {
        LOG.info("... called method ListRiskBlackListedEntitiesMapper.mapOut ...");

        if (data == null || data.isEmpty()) {
            return null;
        }
        return ServiceResponse.data(data).pagination(null).build();
    }

}
