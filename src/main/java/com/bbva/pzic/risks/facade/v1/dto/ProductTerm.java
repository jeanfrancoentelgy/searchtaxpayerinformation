package com.bbva.pzic.risks.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "productTerm", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlType(name = "productTerm", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductTerm implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Frequency of the term
     */
    private String frequency;
    /**
     * Value of the term.
     */
    private Integer value;

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
