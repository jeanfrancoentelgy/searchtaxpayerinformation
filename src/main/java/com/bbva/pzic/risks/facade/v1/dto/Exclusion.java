package com.bbva.pzic.risks.facade.v1.dto;

import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "exclusion", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlType(name = "exclusion", namespace = "urn:com:bbva:pzic:risks:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Exclusion implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Exclusion unique identifier.
     */
    private String id;
    /**
     * Full bank detail of the bank where the exclusion was issued.
     */
    private Bank bank;
    /**
     * Date when the exclusion report was issued .
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date issuedDate;
    /**
     * Type of exclusion related to the current operation.
     */
    private ExclusionType exclusionType;
    /**
     * Identifier of the exclusion level, it is related to the type of
     * exclusion.
     */
    private String exclusionLevel;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public Date getIssuedDate() {
        return issuedDate;
    }

    public void setIssuedDate(Date issuedDate) {
        this.issuedDate = issuedDate;
    }

    public ExclusionType getExclusionType() {
        return exclusionType;
    }

    public void setExclusionType(ExclusionType exclusionType) {
        this.exclusionType = exclusionType;
    }

    public String getExclusionLevel() {
        return exclusionLevel;
    }

    public void setExclusionLevel(String exclusionLevel) {
        this.exclusionLevel = exclusionLevel;
    }
}
