package com.bbva.pzic.risks.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

@XmlRootElement(name = "directDebt", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlType(name = "directDebt", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DirectDebt implements Serializable {

    private static final long serialVersionUID = 1L;

    private String month;

    private Amount financialSystemDebt;

    private Amount bbvaDebt;

    private BigDecimal directFee;

    private Amount grossMargin;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Amount getFinancialSystemDebt() {
        return financialSystemDebt;
    }

    public void setFinancialSystemDebt(Amount financialSystemDebt) {
        this.financialSystemDebt = financialSystemDebt;
    }

    public Amount getBbvaDebt() {
        return bbvaDebt;
    }

    public void setBbvaDebt(Amount bbvaDebt) {
        this.bbvaDebt = bbvaDebt;
    }

    public BigDecimal getDirectFee() {
        return directFee;
    }

    public void setDirectFee(BigDecimal directFee) {
        this.directFee = directFee;
    }

    public Amount getGrossMargin() {
        return grossMargin;
    }

    public void setGrossMargin(Amount grossMargin) {
        this.grossMargin = grossMargin;
    }
}
