package com.bbva.pzic.risks.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "product", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlType(name = "product", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String name;

    private MaximumTerm maximumTerm;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {this.name = name;}

    public MaximumTerm getMaximumTerm() {return maximumTerm;}

    public void setMaximumTerm(MaximumTerm maximumTerm) {this.maximumTerm = maximumTerm;}
}
