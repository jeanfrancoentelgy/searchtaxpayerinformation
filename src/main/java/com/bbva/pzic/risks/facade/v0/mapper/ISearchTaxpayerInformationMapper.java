package com.bbva.pzic.risks.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.business.dto.InputSearchTaxpayerInformation;
import com.bbva.pzic.risks.facade.v0.dto.SearchTaxpayer;

/**
 * Created on 21/09/2020.
 *
 * @author Entelgy.
 */
public interface ISearchTaxpayerInformationMapper {

    InputSearchTaxpayerInformation mapIn(SearchTaxpayer searchTaxpayerInformation);

    ServiceResponse<SearchTaxpayer> mapOut(SearchTaxpayer searchTaxpayerInformation);
}
