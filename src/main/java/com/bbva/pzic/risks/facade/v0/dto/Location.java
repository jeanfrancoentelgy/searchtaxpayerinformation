package com.bbva.pzic.risks.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 21/09/2020.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "location", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlType(name = "location", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Location implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<AddressComponent> addressComponents;

    public List<AddressComponent> getAddressComponents() { return addressComponents; }

    public void setAddressComponents(List<AddressComponent> addressComponents) {
        this.addressComponents = addressComponents;
    }

}
