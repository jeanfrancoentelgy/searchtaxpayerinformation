package com.bbva.pzic.risks.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.business.dto.InputListLettersDelegationsCollaborators;
import com.bbva.pzic.risks.facade.v0.dto.ListLettersDelegationsCollaborators;
import com.bbva.pzic.risks.facade.v0.mapper.IListLettersDelegationsCollaborators;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ListLettersDelegationsCollaboratorsMapper implements IListLettersDelegationsCollaborators {

    @Override
    public InputListLettersDelegationsCollaborators mapIn(final String employeeId, final String branchId) {
        InputListLettersDelegationsCollaborators inputListLettersDelegationsCollaborators = new InputListLettersDelegationsCollaborators();
        inputListLettersDelegationsCollaborators.setCodigoRegistro(employeeId);
        inputListLettersDelegationsCollaborators.setCodigoOficina(branchId);
        return inputListLettersDelegationsCollaborators;
    }

    @Override
    public ServiceResponse<List<ListLettersDelegationsCollaborators>> mapOut(final List<ListLettersDelegationsCollaborators> listLettersDelegationsCollaborators) {
        if (CollectionUtils.isEmpty(listLettersDelegationsCollaborators)){
            return null;
        }
        return ServiceResponse.data(listLettersDelegationsCollaborators).build();
    }

}
