package com.bbva.pzic.risks.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 21/09/2020.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "addressComponent", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlType(name = "addressComponent", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class AddressComponent implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * This array indicates the type of the returned result. It contains a
     * set of zero or more tags identifying the type of feature returned in
     * the result. For example, the city of "Madrid" may return `COMMUNITY`
     * which indicates that "Madrid" is an autonomous community, and also
     * `ADMINISTRATIVE_AREA_LEVEL_2` which indicates a second-level political
     * subdivision, and `POLITICAL` since it is a political entity.
     */
    private List<String> componentTypes;
    /**
     * An abbreviated textual name for the address component. For example,
     * an address component for Spain, may have a `longName` of "Spain", and
     * a `shortName` of `ES`, using the ISO-3166-1 2-digit code.
     */
    private String code;
    /**
     * The full text description or name of the address component.
     */
    private String name;

    public List<String> getComponentTypes() {
        return componentTypes;
    }

    public void setComponentTypes(List<String> componentTypes) {
        this.componentTypes = componentTypes;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
