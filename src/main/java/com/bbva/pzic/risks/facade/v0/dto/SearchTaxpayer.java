package com.bbva.pzic.risks.facade.v0.dto;

import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;

/**
 * Created on 21/09/2020.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "searchTaxpayer", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlType(name = "searchTaxpayer", namespace = "urn:com:bbva:pzic:risks:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class SearchTaxpayer implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Type of identity number.
     */
    private String documentType;
    /**
     * Number of identity document.
     */
    private String documentNumber;
    /**
     * Full name of taxpayer.
     */
    private String fullName;
    /**
     * Closing date that information of taxpayer were updated.
     * String based on ISO-8601 timestamp format.
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date clossingDate;
    /**
     * Debt amount that is expired in SUNAT. It's about total
     * amount of debts that could have a taxpayer.
     */
    private Amount debtAmount;
    /**
     * Status of a taxpayer in SUNAT regarding their work activity
     * that generates tax obligations.
     */
    private String taxPayerStatus;
    /**
     * Condition of taxpayer in SUNAT. It's about the address is declared by
     * taxpayer and the SUNAT may or may not confirm the address.
     */
    private String taxPayerCondition;
    /**
     * Type of taxpayer. it is related with obligations that a taxpayer can
     * have and with type of business.
     */
    private String taxPayerType;
    /**
     * Establishment date of taxpayer. String based on ISO-8601 timestamp format.
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date formationDate;
    /**
     * Activity start date of taxpayer. String based on ISO-8601 timestamp format.
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date activityStartDate;
    /**
     * Economic activity In Peru, this field corresponds to CIIU.
     */
    private EconomicActivity economicActivity;
    /**
     * Information about address of taxpayer.
     */
    private Address address;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Date getClossingDate() {
        return clossingDate;
    }

    public void setClossingDate(Date clossingDate) {
        this.clossingDate = clossingDate;
    }

    public Amount getDebtAmount() {
        return debtAmount;
    }

    public void setDebtAmount(Amount debtAmount) {
        this.debtAmount = debtAmount;
    }

    public String getTaxPayerStatus() {
        return taxPayerStatus;
    }

    public void setTaxPayerStatus(String taxPayerStatus) {
        this.taxPayerStatus = taxPayerStatus;
    }

    public String getTaxPayerCondition() {
        return taxPayerCondition;
    }

    public void setTaxPayerCondition(String taxPayerCondition) {
        this.taxPayerCondition = taxPayerCondition;
    }

    public String getTaxPayerType() {
        return taxPayerType;
    }

    public void setTaxPayerType(String taxPayerType) {
        this.taxPayerType = taxPayerType;
    }

    public Date getFormationDate() {
        return formationDate;
    }

    public void setFormationDate(Date formationDate) {
        this.formationDate = formationDate;
    }

    public Date getActivityStartDate() {
        return activityStartDate;
    }

    public void setActivityStartDate(Date activityStartDate) {
        this.activityStartDate = activityStartDate;
    }

    public EconomicActivity getEconomicActivity() {
        return economicActivity;
    }

    public void setEconomicActivity(EconomicActivity economicActivity) {
        this.economicActivity = economicActivity;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }
}
