package com.bbva.pzic.risks.dao.rest.mapper.impl;

import com.bbva.pzic.risks.business.dto.InputListLettersDelegationsCollaborators;
import com.bbva.pzic.risks.dao.model.collaborators.*;
import com.bbva.pzic.risks.dao.rest.mapper.IRestListLettersDelegationsCollaboratorsMapper;
import com.bbva.pzic.risks.facade.v0.dto.*;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class RestListLettersDelegationsCollaboratorsMapper implements IRestListLettersDelegationsCollaboratorsMapper {

    @Override
    public HashMap<String, String> mapInQueryParams(final InputListLettersDelegationsCollaborators inputListLettersDelegationsCollaborators) {
        HashMap<String, String> params = new HashMap<>();
        params.put("codigoRegistro",inputListLettersDelegationsCollaborators.getCodigoRegistro());
        params.put("codigoOficina",inputListLettersDelegationsCollaborators.getCodigoOficina());
        return params;
    }

    @Override
    public List<ListLettersDelegationsCollaborators> mapOut(final ModelListLettersDelegationsCollaboratorsResponseBody model) {
        if(model==null || CollectionUtils.isEmpty(model.getData())){
            return null;
        }

        return model.getData().stream().filter(Objects::nonNull).map(this::mapOutCollaborators).collect(Collectors.toList());
    }

    private ListLettersDelegationsCollaborators mapOutCollaborators(final ModelCollaborators model){
        ListLettersDelegationsCollaborators result = new ListLettersDelegationsCollaborators();
        result.setDelegationId(model.getId());
        if (model.getColaborador()==null){
            return result;
        } else {
            result.setId(model.getColaborador().getRegistro());
            result.setFullName(model.getColaborador().getNombres());
            result.setRole(mapOutRole(model.getColaborador().getPuesto()));
            result.setBank(mapOutBank(model.getColaborador().getBanco()));
            result.setEconomicGroupLimits(mapOutEconomicGroupLimits(model.getColaborador().getLimitesPorGrupoEconomico()));
            result.setLimitsGaranteed(mapOutLimitsGaranteed(model.getColaborador().getLimitesGarantizados()));
            result.setProductsLimits(mapOutProductsLimits(model.getColaborador().getLimitePorProducto()));
        }
        return result;
    }

    private List<ProductsLimit> mapOutProductsLimits(final List<ModelProductLimits> limitePorProducto) {
        if (CollectionUtils.isEmpty(limitePorProducto)) {
            return null;
        }
        return limitePorProducto.stream().filter(Objects::nonNull).map(this::mapOutProductLimit).collect(Collectors.toList());
    }

    private ProductsLimit mapOutProductLimit(final ModelProductLimits modelProductLimits) {
        ProductsLimit productsLimit = new ProductsLimit();
        productsLimit.setProduct(mapOutProduct(modelProductLimits.getProducto()));
        productsLimit.setLimits(mapOutTotalLimits(modelProductLimits.getLimites()));
        return productsLimit;
    }

    private List<Limits> mapOutTotalLimits(final List<ModelLimits> limites) {
        if (CollectionUtils.isEmpty(limites)) {
            return null;
        }
        return limites.stream().filter(Objects::nonNull).map(this::mapOutTotalLimit).collect(Collectors.toList());
    }

    private Limits mapOutTotalLimit(final ModelLimits modelLimits) {
        Limits limits = new Limits();
        limits.setGroup(modelLimits.getGrupo());
        limits.setRange(modelLimits.getRango());
        limits.setLimit(mapOutLimit(modelLimits.getLimite()));
        return limits;
    }

    private Product mapOutProduct(final ModelProduct producto) {
        if (producto == null) {
            return null;
        }
        Product product = new Product();
        product.setId(producto.getCodigo());
        product.setName(producto.getDescripcion());
        product.setMaximumTerm(mapOutMaximumTerm(producto.getPlazoMaximo()));
        return product;
    }

    private List<LimitsGaranteed> mapOutLimitsGaranteed(final List<ModelLimitsGaranteed> limitesGarantizados) {
        if (CollectionUtils.isEmpty(limitesGarantizados)) {
            return null;
        }
        return limitesGarantizados.stream().filter(Objects::nonNull).map(this::mapOutLimitGaranteed).collect(Collectors.toList());
    }

    private LimitsGaranteed mapOutLimitGaranteed(final ModelLimitsGaranteed modelLimitsGaranteed) {
        LimitsGaranteed limitsGarantee = new LimitsGaranteed();
        limitsGarantee.setGroup(modelLimitsGaranteed.getGrupo());
        limitsGarantee.setLimit(mapOutLimit(modelLimitsGaranteed.getLimite()));
        limitsGarantee.setMaximumTerm(mapOutMaximumTerm(modelLimitsGaranteed.getPlazoMaximo()));
        return limitsGarantee;
    }

    private MaximumTerm mapOutMaximumTerm(final ModelMaximumTerm plazoMaximo) {
        if (plazoMaximo == null) {
            return null;
        }
        MaximumTerm maximumTerm = new MaximumTerm();
        maximumTerm.setTermType(plazoMaximo.getTipoPlazo());
        maximumTerm.setTerm(plazoMaximo.getPlazo());
        return maximumTerm;
    }


    private List<EconomicGroupLimits> mapOutEconomicGroupLimits(final List<ModelEconomicGroupLimits> limitesPorGrupoEconomico) {
        if (CollectionUtils.isEmpty(limitesPorGrupoEconomico)) {
            return null;
        }
        return limitesPorGrupoEconomico.stream().filter(Objects::nonNull).map(this::mapOutEconomicGroupLimit).collect(Collectors.toList());
    }

    private EconomicGroupLimits mapOutEconomicGroupLimit(final ModelEconomicGroupLimits modelEconomicGroupLimits) {
        EconomicGroupLimits economicGroupLimits = new EconomicGroupLimits();
        economicGroupLimits.setGroup(modelEconomicGroupLimits.getGrupo());
        economicGroupLimits.setLimit(mapOutLimit(modelEconomicGroupLimits.getLimite()));
        economicGroupLimits.setDelegationLevel(modelEconomicGroupLimits.getNivelDelegacion());
        return economicGroupLimits;
    }

    private Limit mapOutLimit(final ModelLimit limite) {
        if (limite == null) {
            return null;
        }
        Limit limit = new Limit();
        limit.setAmount(limite.getMonto());
        limit.setCurrency(limite.getMoneda());
        return limit;
    }

    private Bank mapOutBank(final ModelBank banco) {
        if (banco == null) {
            return null;
        }
        Bank bank = new Bank();
        bank.setId(banco.getCodigo());
        bank.setBranch(mapOutBranch(banco.getOficina()));
        return bank;
    }

    private Branch mapOutBranch(final ModelBranch oficina) {
        if (oficina == null) {
            return null;
        }
        Branch branch = new Branch();
        branch.setId(oficina.getCodigo());
        branch.setName(oficina.getNombre());
        return branch;
    }

    private Role mapOutRole(final ModelRole puesto) {
        if (puesto == null) {
            return null;
        }
        Role role = new Role();
        role.setId(puesto.getCodigo());
        role.setName(puesto.getNombre());
        return role;
    }
}
