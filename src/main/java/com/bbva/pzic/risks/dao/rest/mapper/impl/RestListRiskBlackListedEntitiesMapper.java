package com.bbva.pzic.risks.dao.rest.mapper.impl;

import com.bbva.pzic.risks.facade.v1.dto.RiskBlackListedEntity;
import com.bbva.pzic.risks.dao.model.blacklisted.ModelBlackListedEntity;
import com.bbva.pzic.risks.dao.rest.mapper.IRestListRiskBlackListedEntitiesMapper;
import com.bbva.pzic.risks.util.mappers.EnumMapper;
import com.bbva.pzic.risks.util.mappers.Mapper;
import com.bbva.pzic.risks.util.orika.MapperFactory;
import com.bbva.pzic.risks.util.orika.converter.builtin.BooleanToStringConverter;
import com.bbva.pzic.risks.util.orika.converter.builtin.DateToStringConverter;
import com.bbva.pzic.risks.util.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.List;

/**
 * @author Entelgy
 */
@Mapper
public class RestListRiskBlackListedEntitiesMapper extends ConfigurableMapper implements IRestListRiskBlackListedEntitiesMapper {

    @Autowired
    private EnumMapper enumMapper;

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);
        factory.getConverterFactory().registerConverter(new BooleanToStringConverter("1", "0"));
        factory.getConverterFactory().registerConverter(new DateToStringConverter("yyyy-MM-dd"));

        factory.classMap(RiskBlackListedEntity.class, ModelBlackListedEntity.class)
                .field("blocked", "indrechazo")
                .field("identityDocument.documentType.id", "tipdocumento")
                .field("identityDocument.documentNumber", "numdocumento")
                .field("entryDate", "fecharegistro")
                .register();
    }

    @Override
    public List<RiskBlackListedEntity> mapOut(final ModelBlackListedEntity output) {
        if (output == null) {
            return null;
        }

        RiskBlackListedEntity e = map(output, RiskBlackListedEntity.class);

        if (output.getTipdocumento() != null) {
            e.getIdentityDocument().getDocumentType().setId(
                    enumMapper.getEnumValue("documentType.id", output.getTipdocumento()));
        }

        return Collections.singletonList(e);
    }
}
