package com.bbva.pzic.risks.dao.model.hya4;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;



/**
 * Formato de datos <code>HYMR404</code> de la transacci&oacute;n <code>HYA4</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "HYMR404")
@RooJavaBean
@RooSerializable
public class FormatoHYMR404 {
	
	/**
	 * <p>Campo <code>PRINCIP</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 1, nombre = "PRINCIP", tipo = TipoCampo.ENTERO, longitudMinima = 1, longitudMaxima = 1)
	private Integer princip;
	
	/**
	 * <p>Campo <code>TIPPROF</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "TIPPROF", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String tipprof;
	
	/**
	 * <p>Campo <code>DESTPRO</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "DESTPRO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 50, longitudMaxima = 50)
	private String destpro;
	
	/**
	 * <p>Campo <code>MONTPRD</code>, &iacute;ndice: <code>4</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 4, nombre = "MONTPRD", tipo = TipoCampo.DECIMAL, longitudMinima = 15, longitudMaxima = 15, decimales = 2)
	private BigDecimal montprd;
	
	/**
	 * <p>Campo <code>DIVISA</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "DIVISA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String divisa;
	
	/**
	 * <p>Campo <code>TIPPLAZ</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "TIPPLAZ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipplaz;
	
	/**
	 * <p>Campo <code>VAPLAZO</code>, &iacute;ndice: <code>7</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 7, nombre = "VAPLAZO", tipo = TipoCampo.ENTERO, longitudMinima = 4, longitudMaxima = 4)
	private Integer vaplazo;
	
	/**
	 * <p>Campo <code>ESTPROD</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "ESTPROD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String estprod;
	
	/**
	 * <p>Campo <code>TEA</code>, &iacute;ndice: <code>9</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 9, nombre = "TEA", tipo = TipoCampo.DECIMAL, longitudMinima = 10, longitudMaxima = 10, decimales = 7)
	private BigDecimal tea;
	
	/**
	 * <p>Campo <code>MULTBIN</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "MULTBIN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String multbin;
	
	/**
	 * <p>Campo <code>NOMUBIN</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11, nombre = "NOMUBIN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String nomubin;
	
	/**
	 * <p>Campo <code>CONTRAT</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 12, nombre = "CONTRAT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String contrat;
	
}