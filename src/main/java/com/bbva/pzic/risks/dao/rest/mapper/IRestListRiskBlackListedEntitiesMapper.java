package com.bbva.pzic.risks.dao.rest.mapper;

import com.bbva.pzic.risks.facade.v1.dto.RiskBlackListedEntity;
import com.bbva.pzic.risks.dao.model.blacklisted.ModelBlackListedEntity;

import java.util.List;

/**
 * @author Entelgy
 */
public interface IRestListRiskBlackListedEntitiesMapper {

    List<RiskBlackListedEntity> mapOut(ModelBlackListedEntity output);
}
