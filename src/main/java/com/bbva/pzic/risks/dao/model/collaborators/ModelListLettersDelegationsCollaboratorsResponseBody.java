package com.bbva.pzic.risks.dao.model.collaborators;

import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.Message;

import java.util.List;

public class ModelListLettersDelegationsCollaboratorsResponseBody {

    private List<ModelCollaborators> data;

    private List<Message> messages;

    public List<ModelCollaborators> getData() {
        return data;
    }

    public void setData(List<ModelCollaborators> data) {
        this.data = data;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
