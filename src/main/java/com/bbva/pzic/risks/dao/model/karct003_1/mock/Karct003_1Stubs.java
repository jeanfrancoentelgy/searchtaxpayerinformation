package com.bbva.pzic.risks.dao.model.karct003_1.mock;


import com.bbva.pzic.risks.dao.model.karct003_1.RespuestaTransaccionKarct003_1;
import com.bbva.pzic.risks.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 12/02/2020.
 *
 * @author Entelgy
 */
public final class Karct003_1Stubs {

    private static final Karct003_1Stubs INSTANCE = new Karct003_1Stubs();
    private ObjectMapperHelper mapper = ObjectMapperHelper.getInstance();

    private Karct003_1Stubs() {
    }

    public static Karct003_1Stubs getInstance() {
        return INSTANCE;
    }

    public RespuestaTransaccionKarct003_1 getFinancialDebts() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/risks/dao/model/karct003_1/mock/respuestaTransaccionKarct003_1.json"), RespuestaTransaccionKarct003_1.class);
    }
}
