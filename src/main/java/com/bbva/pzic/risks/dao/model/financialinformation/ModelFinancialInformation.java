package com.bbva.pzic.risks.dao.model.financialinformation;

import java.util.List;

public class ModelFinancialInformation {

    private String customerId;

    private ModelIdentityDocument identityDocument;

    private List<ModelRelatedProduct> relatedProducts;

    private ModelInformationBbva informationBbva;

    private List<ModelMonthlyRegister> monthlyRegistation;

    private List<ModelSpread> spreadHistory;

    private List<ModelDebtInformation> debtSsff;

    private List<ModelDebtBreakdown> debtProductSssff;

    private ModelAdditionalInformation additional;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public ModelIdentityDocument getIdentityDocument() {
        return identityDocument;
    }

    public void setIdentityDocument(ModelIdentityDocument identityDocument) {
        this.identityDocument = identityDocument;
    }

    public List<ModelRelatedProduct> getRelatedProducts() {
        return relatedProducts;
    }

    public void setRelatedProducts(List<ModelRelatedProduct> relatedProducts) {
        this.relatedProducts = relatedProducts;
    }

    public ModelInformationBbva getInformationBbva() {
        return informationBbva;
    }

    public void setInformationBbva(ModelInformationBbva informationBbva) {
        this.informationBbva = informationBbva;
    }

    public List<ModelMonthlyRegister> getMonthlyRegistation() {
        return monthlyRegistation;
    }

    public void setMonthlyRegistation(List<ModelMonthlyRegister> monthlyRegistation) {
        this.monthlyRegistation = monthlyRegistation;
    }

    public List<ModelSpread> getSpreadHistory() {
        return spreadHistory;
    }

    public void setSpreadHistory(List<ModelSpread> spreadHistory) {
        this.spreadHistory = spreadHistory;
    }

    public List<ModelDebtInformation> getDebtSsff() {
        return debtSsff;
    }

    public void setDebtSsff(List<ModelDebtInformation> debtSsff) {
        this.debtSsff = debtSsff;
    }

    public List<ModelDebtBreakdown> getDebtProductSssff() {
        return debtProductSssff;
    }

    public void setDebtProductSssff(List<ModelDebtBreakdown> debtProductSssff) {
        this.debtProductSssff = debtProductSssff;
    }

    public ModelAdditionalInformation getAdditional() {
        return additional;
    }

    public void setAdditional(ModelAdditionalInformation additional) {
        this.additional = additional;
    }
}
