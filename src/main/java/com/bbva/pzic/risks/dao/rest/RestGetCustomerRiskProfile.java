package com.bbva.pzic.risks.dao.rest;

import com.bbva.pzic.risks.dao.model.profille.ModelCustomerRiskProfile;
import com.bbva.pzic.risks.util.connection.rest.RestGetConnection;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;

/**
 * Created on 18/12/2018.
 *
 * @author Entelgy
 */
@Component
public class RestGetCustomerRiskProfile extends RestGetConnection<ModelCustomerRiskProfile> {

    private static final String URL_PROPERTY = "servicing.url.risks.getCustomerRiskProfile";
    private static final String USE_PROXY_PROPERTY = "servicing.proxy.risks.getCustomerRiskProfile";

    @PostConstruct
    public void init() {
        useProxy = configurationManager.getBooleanProperty(USE_PROXY_PROPERTY, false);
    }

    public ModelCustomerRiskProfile invoke(final String customerId) {
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("customerid", customerId);

        return connect(URL_PROPERTY, queryParams);
    }

    @Override
    protected void evaluateResponse(final ModelCustomerRiskProfile response, final int statusCode) {
        evaluateMessagesResponse(response.getMessages(), "SMCPE1810384", statusCode);
    }
}
