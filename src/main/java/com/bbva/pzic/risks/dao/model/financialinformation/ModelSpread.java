package com.bbva.pzic.risks.dao.model.financialinformation;

import java.math.BigDecimal;

public class ModelSpread {
    private String initialMonth;

    private String finalMonth;

    private String currency;

    private BigDecimal tea;

    private BigDecimal amount;

    private BigDecimal percentageSpread;

    public String getInitialMonth() {
        return initialMonth;
    }

    public void setInitialMonth(String initialMonth) {
        this.initialMonth = initialMonth;
    }

    public String getFinalMonth() {
        return finalMonth;
    }

    public void setFinalMonth(String finalMonth) {
        this.finalMonth = finalMonth;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getTea() {
        return tea;
    }

    public void setTea(BigDecimal tea) {
        this.tea = tea;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getPercentageSpread() {
        return percentageSpread;
    }

    public void setPercentageSpread(BigDecimal percentageSpread) {
        this.percentageSpread = percentageSpread;
    }
}
