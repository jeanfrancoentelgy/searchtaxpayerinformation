package com.bbva.pzic.risks.dao.tx.mapper;

import com.bbva.pzic.risks.business.dto.InputSearchRiskInternalFilters;
import com.bbva.pzic.risks.dao.model.rif1.FormatoRIMRF01;
import com.bbva.pzic.risks.dao.model.rif1.FormatoRIMRF02;
import com.bbva.pzic.risks.facade.v0.dto.SearchRiskInternalFilters;

/**
 * Created on 15/06/2020.
 *
 * @author Entelgy.
 */
public interface ITxSearchRiskInternalFiltersMapper {

    FormatoRIMRF01 mapIn(InputSearchRiskInternalFilters inputSearchRiskInternalFilters);

    SearchRiskInternalFilters mapOut(FormatoRIMRF02 formatoHYMR403);
}
