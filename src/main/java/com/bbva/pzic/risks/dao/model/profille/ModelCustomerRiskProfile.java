package com.bbva.pzic.risks.dao.model.profille;

import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.Message;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Entelgy
 */
public class ModelCustomerRiskProfile {

    private String id;
    private String riskProfileType;
    private String name;
    private Long recalculationAttemptsLeft;
    private String status;
    private BigDecimal riskProfile;
    private List<Message> messages;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRiskProfileType() {
        return riskProfileType;
    }

    public void setRiskProfileType(String riskProfileType) {
        this.riskProfileType = riskProfileType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getRecalculationAttemptsLeft() {
        return recalculationAttemptsLeft;
    }

    public void setRecalculationAttemptsLeft(Long recalculationAttemptsLeft) {
        this.recalculationAttemptsLeft = recalculationAttemptsLeft;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getRiskProfile() {
        return riskProfile;
    }

    public void setRiskProfile(BigDecimal riskProfile) {
        this.riskProfile = riskProfile;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
