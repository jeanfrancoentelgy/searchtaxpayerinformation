package com.bbva.pzic.risks.dao.apx.mapper;

import com.bbva.pzic.risks.business.dto.InputListRiskFinancialDebts;
import com.bbva.pzic.risks.dao.model.karct003_1.PeticionTransaccionKarct003_1;
import com.bbva.pzic.risks.dao.model.karct003_1.RespuestaTransaccionKarct003_1;
import com.bbva.pzic.risks.facade.v1.dto.FinancialDebt;

import java.util.List;

/**
 * Created on 19/05/2020.
 *
 * @author Entelgy.
 */
public interface IApxListRiskFinancialDebtsMapper {

    PeticionTransaccionKarct003_1 mapIn(InputListRiskFinancialDebts input);

    List<FinancialDebt> mapOut(RespuestaTransaccionKarct003_1 response);
}
