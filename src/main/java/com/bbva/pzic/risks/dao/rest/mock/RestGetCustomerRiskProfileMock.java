package com.bbva.pzic.risks.dao.rest.mock;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.risks.dao.model.profille.ModelCustomerRiskProfile;
import com.bbva.pzic.risks.dao.rest.RestGetCustomerRiskProfile;
import com.bbva.pzic.risks.dao.rest.mock.stub.ResponseGetCustomerRiskProfileMock;
import com.bbva.pzic.risks.util.Errors;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created on 18/12/2018.
 *
 * @author Entelgy
 */
@Primary
@Component
public class RestGetCustomerRiskProfileMock extends RestGetCustomerRiskProfile {

    @Override
    public ModelCustomerRiskProfile connect(String urlPropertyValue, HashMap<String, String> queryParams) {
        ModelCustomerRiskProfile response;
        try {
            response = ResponseGetCustomerRiskProfileMock.getInstance().buildCustomerRiskProfile();
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
        evaluateResponse(response, 200);
        return response;
    }
}
