package com.bbva.pzic.risks.dao.model.rif1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;


/**
 * <p>Transacci&oacute;n <code>RIF1</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionRif1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionRif1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.RIF1.D1200908.txt
 * RIF1LISTADO DE FILTROS DE CLIENTES     HY        RI2C00F1     01 RIMRF01             RIF1  NN0000NNNNNN    SSTN     E  SNNSSNNN  NN                2020-06-09XP86102 2020-09-0506.12.35XP90749 2020-06-09-17.24.39.747558XP86102 0001-01-010001-01-01
 * 
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.RIMRF01.D1200908.txt
 * RIMRF01 �CODIGO DE CLIENTE             �F�03�00020�01�00001�TIPODOC�TIPO DE DOCUMENTO   �A�001�0�O�        �
 * RIMRF01 �CODIGO DE CLIENTE             �F�03�00020�02�00002�NUMEDOC�NUMERO DE DOCUMENTO �A�011�0�O�        �
 * RIMRF01 �CODIGO DE CLIENTE             �F�03�00020�03�00013�CODCENT�CODIGO DE CLIENTE   �A�008�0�O�        �
 * 
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.RIMRF02.D1200908.txt
 * RIMRF02 �SEGMENTO CAMPANA              �X�05�00076�01�00016�CLASBAN�CLASIFICACION BANCO �A�016�0�S�        �
 * RIMRF02 �SEGMENTO CAMPANA              �X�05�00076�02�00035�CLASSFF�CLASIFI. SISTEMA FIN�A�016�0�S�        �
 * RIMRF02 �SEGMENTO CAMPANA              �X�05�00076�03�00054�ETIRIES�ETIQUETA DE RIESGO  �A�040�0�S�        �
 * RIMRF02 �SEGMENTO CAMPANA              �X�05�00076�04�00097�NROENTI�NUM TOTAL ENTIDADES �N�002�0�S�        �
 * RIMRF02 �SEGMENTO CAMPANA              �X�05�00076�05�00102�BURO   �GRUPO DE BURO       �A�002�0�S�        �
 * 
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.RIF1.D1200908.txt
 * RIF1RIMRF02 RINCRF02RI2C00F11S126                          XP86102 2020-06-09-18.18.17.719931XP86102 2020-06-09-18.18.17.720027
 * 
</pre></code>
 * 
 * @see RespuestaTransaccionRif1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "RIF1",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionRif1.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoRIMRF01.class})
@RooJavaBean
@RooSerializable
public class PeticionTransaccionRif1 implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}