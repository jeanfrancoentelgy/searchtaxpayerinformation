package com.bbva.pzic.risks.dao.rest.mapper.impl;

import com.bbva.pzic.risks.dao.model.blacklisted.ModelProfileRequest;
import com.bbva.pzic.risks.dao.model.profille.ModelCustomerRiskProfile;
import com.bbva.pzic.risks.dao.rest.mapper.IRestModifyCustomerRiskProfileMapper;
import com.bbva.pzic.risks.util.mappers.Mapper;

import java.util.HashMap;
import java.util.Map;

@Mapper
public class RestModifyCustomerRiskProfileMapper implements IRestModifyCustomerRiskProfileMapper {

    @Override
    public ModelCustomerRiskProfile mapInPayload(ModelProfileRequest request) {
        if (request == null) {
            return null;
        }
        ModelCustomerRiskProfile customerRiskProfile = new ModelCustomerRiskProfile();
        customerRiskProfile.setRiskProfileType(request.getRiskProfileType());
        return customerRiskProfile;
    }

    @Override
    public Map<String, String> mapIn(ModelProfileRequest request) {
        if (request == null) {
            return null;
        }
        Map<String, String> mapIn = new HashMap<>();
        mapIn.put("profile-id", request.getProfileId());
        return mapIn;
    }
}
