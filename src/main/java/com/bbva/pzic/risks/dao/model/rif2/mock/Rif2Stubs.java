package com.bbva.pzic.risks.dao.model.rif2.mock;


import com.bbva.pzic.risks.dao.model.rif1.FormatoRIMRF02;
import com.bbva.pzic.risks.dao.model.rif2.FormatoRIMRF22;
import com.bbva.pzic.risks.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.List;

/**
 * Created on 12/02/2020.
 *
 * @author Entelgy
 */
public final class Rif2Stubs {

    private static final Rif2Stubs INSTANCE = new Rif2Stubs();
    private ObjectMapperHelper mapper = ObjectMapperHelper.getInstance();

    private Rif2Stubs() {
    }

    public static Rif2Stubs getInstance() {
        return INSTANCE;
    }

    public List<FormatoRIMRF22> buildFormatosRIMRF22() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/risks/dao/model/rif2/mock/RIMRF22.json"), new TypeReference<List<FormatoRIMRF22>>() {
        });
    }
}
