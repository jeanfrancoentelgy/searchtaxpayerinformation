package com.bbva.pzic.risks.dao.rest;

import com.bbva.pzic.risks.business.dto.InputListLettersDelegationsCollaborators;
import com.bbva.pzic.risks.dao.model.collaborators.ModelListLettersDelegationsCollaboratorsResponseBody;
import com.bbva.pzic.risks.dao.rest.mapper.IRestListLettersDelegationsCollaboratorsMapper;
import com.bbva.pzic.risks.facade.v0.dto.ListLettersDelegationsCollaborators;
import com.bbva.pzic.risks.util.connection.rest.RestGetConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

import static com.bbva.pzic.risks.facade.RegistryIds.SMC_REGISTRY_ID_OF_LIST_LETTERS_DELEGATIONS_COLLABORATORS;

@Component("restListLettersDelegationsCollaborators")
public class RestListLettersDelegationsCollaborators extends RestGetConnection<ModelListLettersDelegationsCollaboratorsResponseBody> {

    private static final String URL_PROPERTY = "servicing.smc.configuration.SMGG20200564.backend.url";

    @Autowired
    private IRestListLettersDelegationsCollaboratorsMapper mapper;

    @PostConstruct
    public void init(){
        useProxy = false;
    }

    public List<ListLettersDelegationsCollaborators> invoke(final InputListLettersDelegationsCollaborators input){
        return mapper.mapOut(connect(URL_PROPERTY,null ,mapper.mapInQueryParams(input),null));
    }

    @Override
    protected void evaluateResponse(final ModelListLettersDelegationsCollaboratorsResponseBody response,final int statusCode) {
        evaluateMessagesResponse(response.getMessages(),SMC_REGISTRY_ID_OF_LIST_LETTERS_DELEGATIONS_COLLABORATORS,statusCode);
    }
}
