package com.bbva.pzic.risks.dao.apx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.risks.business.dto.InputListRiskFinancialDebts;
import com.bbva.pzic.risks.dao.apx.mapper.IApxListRiskFinancialDebtsMapper;
import com.bbva.pzic.risks.dao.model.karct003_1.PeticionTransaccionKarct003_1;
import com.bbva.pzic.risks.dao.model.karct003_1.RespuestaTransaccionKarct003_1;
import com.bbva.pzic.risks.facade.v1.dto.FinancialDebt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created on 19/05/2020.
 *
 * @author Entelgy.
 */
@Component
public class ApxListRiskFinancialDebts {

    @Autowired
    private IApxListRiskFinancialDebtsMapper mapper;

    @Autowired
    private InvocadorTransaccion<PeticionTransaccionKarct003_1, RespuestaTransaccionKarct003_1> transaccion;

    public List<FinancialDebt> perform(final InputListRiskFinancialDebts input) {
        PeticionTransaccionKarct003_1 request = mapper.mapIn(input);
        RespuestaTransaccionKarct003_1 response = transaccion.invocar(request);
        return mapper.mapOut(response);
    }
}
