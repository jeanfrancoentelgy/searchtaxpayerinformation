package com.bbva.pzic.risks.dao.model.collaborators;

public class ModelMaximumTerm {

    private String tipoPlazo;

    private Integer Plazo;

    public String getTipoPlazo() {return tipoPlazo;}

    public void setTipoPlazo(String tipoPlazo) {
        this.tipoPlazo = tipoPlazo;
    }

    public Integer getPlazo() {
        return Plazo;
    }

    public void setPlazo(Integer plazo) {
        Plazo = plazo;
    }
}
