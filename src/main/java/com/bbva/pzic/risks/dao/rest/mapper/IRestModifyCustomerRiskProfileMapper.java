package com.bbva.pzic.risks.dao.rest.mapper;

import com.bbva.pzic.risks.dao.model.blacklisted.ModelProfileRequest;
import com.bbva.pzic.risks.dao.model.profille.ModelCustomerRiskProfile;
import java.util.Map;

public interface IRestModifyCustomerRiskProfileMapper {

    ModelCustomerRiskProfile mapInPayload(ModelProfileRequest request);

    Map<String, String> mapIn(ModelProfileRequest request);
}
