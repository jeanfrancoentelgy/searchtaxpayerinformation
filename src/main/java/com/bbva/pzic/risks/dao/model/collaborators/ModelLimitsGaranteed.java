package com.bbva.pzic.risks.dao.model.collaborators;


public class ModelLimitsGaranteed {

    private String grupo;

    private ModelLimit limite;

    private ModelMaximumTerm plazoMaximo;

    public String getGrupo() {return grupo;}

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public ModelLimit getLimite() {return limite;}

    public void setLimite(ModelLimit limite) {this.limite = limite;}

    public ModelMaximumTerm getPlazoMaximo() {
        return plazoMaximo;
    }

    public void setPlazoMaximo(ModelMaximumTerm plazoMaximo) {
        this.plazoMaximo = plazoMaximo;
    }
}
