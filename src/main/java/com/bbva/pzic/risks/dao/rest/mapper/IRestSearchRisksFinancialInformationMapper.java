package com.bbva.pzic.risks.dao.rest.mapper;

import com.bbva.pzic.risks.business.dto.InputSearchRisksFinancialInformation;
import com.bbva.pzic.risks.dao.model.financialinformation.ModelSearchRisksFinancialInformationResponseBody;
import com.bbva.pzic.risks.facade.v0.dto.SearchFinancialInformation;

import java.util.HashMap;

public interface IRestSearchRisksFinancialInformationMapper {

    HashMap<String, String> mapInQueryParams(InputSearchRisksFinancialInformation input);

    HashMap<String, String> mapInHeader(InputSearchRisksFinancialInformation input);

    SearchFinancialInformation mapOut(ModelSearchRisksFinancialInformationResponseBody model);
}
