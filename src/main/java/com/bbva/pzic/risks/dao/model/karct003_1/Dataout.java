package com.bbva.pzic.risks.dao.model.karct003_1;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>dataOut</code>, utilizado por la clase <code>Data</code></p>
 * 
 * @see Data
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Dataout {
	
	/**
	 * <p>Campo <code>id</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true, obligatorio = true)
	private String id;
	
	/**
	 * <p>Campo <code>financialDebtType</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "financialDebtType", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true, obligatorio = true)
	private String financialdebttype;
	
	/**
	 * <p>Campo <code>nonBBVADisposedAmount</code>, &iacute;ndice: <code>3</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 3, nombre = "nonBBVADisposedAmount", tipo = TipoCampo.DTO)
	private Nonbbvadisposedamount nonbbvadisposedamount;
	
	/**
	 * <p>Campo <code>disposedAmount</code>, &iacute;ndice: <code>4</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 4, nombre = "disposedAmount", tipo = TipoCampo.DTO)
	private Disposedamount disposedamount;
	
	/**
	 * <p>Campo <code>disposedPercentage</code>, &iacute;ndice: <code>5</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 5, nombre = "disposedPercentage", tipo = TipoCampo.DECIMAL, longitudMaxima = 5, signo = true, obligatorio = true)
	private BigDecimal disposedpercentage;
	
	/**
	 * <p>Campo <code>reportingDate</code>, &iacute;ndice: <code>6</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 6, nombre = "reportingDate", tipo = TipoCampo.FECHA, signo = true, formato = "yyyy-MM-dd", obligatorio = true)
	private Date reportingdate;
	
}