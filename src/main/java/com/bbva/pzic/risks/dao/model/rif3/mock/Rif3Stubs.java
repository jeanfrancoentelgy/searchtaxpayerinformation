package com.bbva.pzic.risks.dao.model.rif3.mock;

import com.bbva.pzic.risks.dao.model.rif1.FormatoRIMRF02;
import com.bbva.pzic.risks.dao.model.rif1.mock.Rif1Stubs;
import com.bbva.pzic.risks.dao.model.rif3.FormatoRIMRF32;
import com.bbva.pzic.risks.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 23/09/2020.
 *
 * @author Entelgy
 */
public class Rif3Stubs {
    private static final Rif3Stubs INSTANCE = new Rif3Stubs();
    private ObjectMapperHelper mapper = ObjectMapperHelper.getInstance();

    private Rif3Stubs() {
    }

    public static Rif3Stubs getInstance() {
        return INSTANCE;
    }

    public FormatoRIMRF32 getRespuestaTransaccionRif3() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/risks/dao/model/rif3/mock/respuestaTransaccionRif3.json"), FormatoRIMRF32.class);
    }

}
