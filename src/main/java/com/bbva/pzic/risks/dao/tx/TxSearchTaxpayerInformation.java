package com.bbva.pzic.risks.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.risks.business.dto.InputSearchTaxpayerInformation;
import com.bbva.pzic.risks.dao.model.rif3.FormatoRIMRF31;
import com.bbva.pzic.risks.dao.model.rif3.FormatoRIMRF32;
import com.bbva.pzic.risks.dao.model.rif3.PeticionTransaccionRif3;
import com.bbva.pzic.risks.dao.model.rif3.RespuestaTransaccionRif3;
import com.bbva.pzic.risks.dao.tx.mapper.ITxSearchTaxpayerInformationMapper;
import com.bbva.pzic.risks.facade.v0.dto.SearchTaxpayer;
import com.bbva.pzic.routine.commons.utils.host.templates.Tx;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.text.ParseException;

/**
 * Created on 22/09/2020.
 *
 * @author Entelgy
 */
@Tx
public class TxSearchTaxpayerInformation extends SingleOutputFormat<InputSearchTaxpayerInformation, FormatoRIMRF31, SearchTaxpayer, FormatoRIMRF32> {

    @Autowired
    private ITxSearchTaxpayerInformationMapper iTxSearchTaxpayerInformationMapper;

    public TxSearchTaxpayerInformation(@Qualifier("transaccionRif3") InvocadorTransaccion<PeticionTransaccionRif3, RespuestaTransaccionRif3> transaction) {
        super(transaction, PeticionTransaccionRif3::new, SearchTaxpayer::new , FormatoRIMRF32.class);
    }

    @Override
    protected FormatoRIMRF31 mapInput(InputSearchTaxpayerInformation inputSearchTaxpayerInformation) {
        return iTxSearchTaxpayerInformationMapper.mapIn(inputSearchTaxpayerInformation);
    }

    @Override
    protected SearchTaxpayer mapFirstOutputFormat(FormatoRIMRF32 formatoRIMRF32, InputSearchTaxpayerInformation inputSearchTaxpayerInformation, SearchTaxpayer searchTaxpayer) {
        try {
            return iTxSearchTaxpayerInformationMapper.mapOut(formatoRIMRF32);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
