package com.bbva.pzic.risks.dao.model.collaborators;

import java.util.List;

public class ModelProductLimits {

    private ModelProduct producto;

    private List<ModelLimits> limites;

    public ModelProduct getProducto() {
        return producto;
    }

    public void setProducto(ModelProduct producto) {
        this.producto = producto;
    }

    public List<ModelLimits> getLimites() {
        return limites;
    }

    public void setLimites(List<ModelLimits> limites) {
        this.limites = limites;
    }
}
