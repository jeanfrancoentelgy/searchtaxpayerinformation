package com.bbva.pzic.risks.dao.model.karct003_1;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>nonBBVADisposedAmount</code>, utilizado por la clase <code>Dataout</code></p>
 * 
 * @see Dataout
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Nonbbvadisposedamount {
	
	/**
	 * <p>Campo <code>amount</code>, &iacute;ndice: <code>1</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 1, nombre = "amount", tipo = TipoCampo.DECIMAL, longitudMaxima = 20, signo = true, obligatorio = true)
	private BigDecimal amount;
	
	/**
	 * <p>Campo <code>currency</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "currency", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true, obligatorio = true)
	private String currency;
	
}