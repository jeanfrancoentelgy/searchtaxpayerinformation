package com.bbva.pzic.risks.dao.tx.mapper;

import com.bbva.pzic.risks.business.dto.InputSearchTaxpayerInformation;
import com.bbva.pzic.risks.dao.model.rif3.FormatoRIMRF31;
import com.bbva.pzic.risks.dao.model.rif3.FormatoRIMRF32;
import com.bbva.pzic.risks.facade.v0.dto.SearchTaxpayer;

import java.text.ParseException;

/**
 * Created on 21/09/2020.
 *
 * @author Entelgy.
 */
public interface ITxSearchTaxpayerInformationMapper {

    FormatoRIMRF31 mapIn(InputSearchTaxpayerInformation inputSearchTaxpayerInformation);

    SearchTaxpayer mapOut(FormatoRIMRF32 formato) throws ParseException;

}
