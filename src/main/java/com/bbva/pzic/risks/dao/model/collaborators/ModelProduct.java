package com.bbva.pzic.risks.dao.model.collaborators;

public class ModelProduct {

    private String codigo;

    private String descripcion;

    private ModelMaximumTerm plazoMaximo;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ModelMaximumTerm getPlazoMaximo() {return plazoMaximo;}

    public void setPlazoMaximo(ModelMaximumTerm plazoMaximo) {this.plazoMaximo = plazoMaximo;}
}
