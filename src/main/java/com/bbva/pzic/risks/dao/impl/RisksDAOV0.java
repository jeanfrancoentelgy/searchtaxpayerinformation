package com.bbva.pzic.risks.dao.impl;

import com.bbva.pzic.risks.business.dto.InputListLettersDelegationsCollaborators;
import com.bbva.pzic.risks.business.dto.InputSearchRiskInternalFilters;
import com.bbva.pzic.risks.business.dto.InputSearchRisksFinancialInformation;
import com.bbva.pzic.risks.business.dto.InputSearchTaxpayerInformation;
import com.bbva.pzic.risks.dao.IRisksDAOV0;
import com.bbva.pzic.risks.dao.rest.RestListLettersDelegationsCollaborators;
import com.bbva.pzic.risks.dao.rest.RestSearchRisksFinancialInformation;
import com.bbva.pzic.risks.dao.tx.TxSearchRiskInternalFilters;
import com.bbva.pzic.risks.dao.tx.TxSearchTaxpayerInformation;
import com.bbva.pzic.risks.facade.v0.dto.ListLettersDelegationsCollaborators;
import com.bbva.pzic.risks.facade.v0.dto.SearchFinancialInformation;
import com.bbva.pzic.risks.facade.v0.dto.SearchRiskInternalFilters;
import com.bbva.pzic.risks.facade.v0.dto.SearchTaxpayer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created on 15/06/2020.
 *
 * @author Entelgy.
 */
@Repository
public class RisksDAOV0 implements IRisksDAOV0 {

    private static final Log LOG = LogFactory.getLog(RisksDAOV0.class);

    @Autowired
    private TxSearchRiskInternalFilters txSearchRiskInternalFilters;

    @Autowired
    private RestSearchRisksFinancialInformation restSearchRisksFinancialInformation;

    @Autowired
    private RestListLettersDelegationsCollaborators restListLettersDelegationsCollaborators;

    @Autowired
    private TxSearchTaxpayerInformation txSearchTaxpayerInformation;

    @Override
    public SearchRiskInternalFilters searchRiskInternalFilters(final InputSearchRiskInternalFilters input) {
        LOG.info("... called method RisksDAOV0.searchRiskInternalFilters ...");
        return txSearchRiskInternalFilters.perform(input);
    }

    @Override
    public SearchFinancialInformation searchRisksFinancialInformation(final InputSearchRisksFinancialInformation input) {
        LOG.info("... called method RisksDAOV0.searchRisksFinancialInformation ...");
        return restSearchRisksFinancialInformation.invoke(input);
    }

    @Override
    public List<ListLettersDelegationsCollaborators> listLettersDelegationsCollaborators(final InputListLettersDelegationsCollaborators input) {
        LOG.info("... called method RisksDAOV0.listLettersDelegationsCollaborators ...");
        return restListLettersDelegationsCollaborators.invoke(input);
    }

    @Override
    public SearchTaxpayer searchTaxpayerInformation(InputSearchTaxpayerInformation input) {
        LOG.info("... called method RisksDAOV0.searchTaxpayerInformation ...");
        return txSearchTaxpayerInformation.perform(input);
    }

}
