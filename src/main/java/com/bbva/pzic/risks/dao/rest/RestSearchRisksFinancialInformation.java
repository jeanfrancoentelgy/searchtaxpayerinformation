package com.bbva.pzic.risks.dao.rest;

import com.bbva.pzic.risks.business.dto.InputSearchRisksFinancialInformation;
import com.bbva.pzic.risks.dao.model.financialinformation.ModelSearchRisksFinancialInformationResponseBody;
import com.bbva.pzic.risks.dao.rest.mapper.IRestSearchRisksFinancialInformationMapper;
import com.bbva.pzic.risks.facade.v0.dto.SearchFinancialInformation;
import com.bbva.pzic.risks.util.connection.rest.RestGetConnection;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component("restSearchRisksFinancialInformation")
public class RestSearchRisksFinancialInformation extends RestGetConnection<ModelSearchRisksFinancialInformationResponseBody> {

    private static final String URL_PROPERTY = "servicing.smc.configuration.SMGG20200450.backend.url";

    @Resource(name = "restSearchRisksFinancialInformationMapper")
    private IRestSearchRisksFinancialInformationMapper mapper;

    public SearchFinancialInformation invoke(final InputSearchRisksFinancialInformation input) {
        return mapper.mapOut(connect(URL_PROPERTY, null, mapper.mapInQueryParams(input), mapper.mapInHeader(input)));
    }

    @Override
    protected void evaluateResponse(ModelSearchRisksFinancialInformationResponseBody response, int statusCode) {
    }
}
