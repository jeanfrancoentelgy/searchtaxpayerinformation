package com.bbva.pzic.risks.dao.impl;

import com.bbva.jee.arq.spring.core.servicing.configuration.ConfigurationManager;
import com.bbva.pzic.risks.business.dto.InputListRiskExclusions;
import com.bbva.pzic.risks.business.dto.InputListRiskFinancialDebts;
import com.bbva.pzic.risks.business.dto.InputListRiskOffers;
import com.bbva.pzic.risks.dao.IRisksDAOV1;
import com.bbva.pzic.risks.dao.apx.ApxListRiskFinancialDebts;
import com.bbva.pzic.risks.dao.model.OAuthCredential;
import com.bbva.pzic.risks.dao.model.blacklisted.ModelClientRequest;
import com.bbva.pzic.risks.dao.model.blacklisted.ModelProfileRequest;
import com.bbva.pzic.risks.dao.model.profille.ModelCustomerRiskProfile;
import com.bbva.pzic.risks.dao.rest.RestGetCustomerRiskProfile;
import com.bbva.pzic.risks.dao.rest.RestGetRimacToken;
import com.bbva.pzic.risks.dao.rest.RestListRiskBlackListedEntities;
import com.bbva.pzic.risks.dao.rest.RestModifyCustomerRiskProfile;
import com.bbva.pzic.risks.dao.tx.TxListRiskExclusions;
import com.bbva.pzic.risks.dao.tx.TxListRiskOffers;
import com.bbva.pzic.risks.facade.v1.dto.Exclusion;
import com.bbva.pzic.risks.facade.v1.dto.FinancialDebt;
import com.bbva.pzic.risks.facade.v1.dto.RiskBlackListedEntity;
import com.bbva.pzic.risks.facade.v1.dto.RiskOffer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
public class RisksDAOV1 implements IRisksDAOV1 {

    private static final Log LOG = LogFactory.getLog(RisksDAOV1.class);

    @Autowired
    private RestListRiskBlackListedEntities restListRiskBlackListedEntities;

    @Autowired
    private RestGetCustomerRiskProfile restGetCustomerRiskProfile;

    @Autowired
    private RestModifyCustomerRiskProfile restModifyCustomerRiskProfile;

    @Autowired
    private RestGetRimacToken restGetRimacToken;

    @Autowired
    private ApxListRiskFinancialDebts apxListRiskFinancialDebts;

    @Autowired
    private TxListRiskOffers txListRiskOffers;

    @Autowired
    private TxListRiskExclusions txListRiskExclusions;

    @Autowired
    protected ConfigurationManager configurationManager;

    @Override
    public List<RiskBlackListedEntity> listRiskBlackListedEntities(final ModelClientRequest request) {
        LOG.info("... called method RisksDAOV1.listRiskBlackListedEntities ...");
        OAuthCredential oAuthCredential = restGetRimacToken.invoke();
        String tokenType = configurationManager.getProperty("servicing.backend.rimac.authorization.tokenType", "Bearer");
        LOG.info(String.format("Tipo de token obtenido = '%s'", tokenType));
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Authorization", String.format("%s %s", tokenType, oAuthCredential.getAccessToken()));
        return restListRiskBlackListedEntities.invoke(request, headers);
    }

    @Override
    public ModelCustomerRiskProfile getCustomerRiskProfile(final String customerId) {
        LOG.info("... called method RisksDAOV1.getCustomerRiskProfile ...");
        return restGetCustomerRiskProfile.invoke(customerId);
    }

    @Override
    public void modifyCustomerRiskProfile(final ModelProfileRequest request) {
        LOG.info("... called method RisksDAOV1.modifyCustomerRiskProfile ...");
        restModifyCustomerRiskProfile.invoke(request);
    }

    @Override
    public List<FinancialDebt> listRiskFinancialDebts(final InputListRiskFinancialDebts input) {
        LOG.info("... called method RisksDAOV1.listRiskFinancialDebts ...");
        return apxListRiskFinancialDebts.perform(input);
    }

    @Override
    public List<RiskOffer> listRiskOffers(final InputListRiskOffers input) {
        LOG.info("... called method RisksDAOV1.listRiskOffers ...");
        return txListRiskOffers.perform(input);
    }

    @Override
    public List<Exclusion> listRiskExclusions(final InputListRiskExclusions input) {
        LOG.info("... Invoking method RisksDAOV1.listRiskExclusions ...");
        return txListRiskExclusions.perform(input);
    }
}
