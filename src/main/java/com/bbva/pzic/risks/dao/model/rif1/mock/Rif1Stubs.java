package com.bbva.pzic.risks.dao.model.rif1.mock;


import com.bbva.pzic.risks.dao.model.karct003_1.RespuestaTransaccionKarct003_1;
import com.bbva.pzic.risks.dao.model.rif1.FormatoRIMRF02;
import com.bbva.pzic.risks.dao.model.rif1.RespuestaTransaccionRif1;
import com.bbva.pzic.risks.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 12/02/2020.
 *
 * @author Entelgy
 */
public final class Rif1Stubs {

    private static final Rif1Stubs INSTANCE = new Rif1Stubs();
    private ObjectMapperHelper mapper = ObjectMapperHelper.getInstance();

    private Rif1Stubs() {
    }

    public static Rif1Stubs getInstance() {
        return INSTANCE;
    }

    public FormatoRIMRF02 getRespuestaTransaccionRif1() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/risks/dao/model/rif1/mock/respuestaTransaccionRif1.json"), FormatoRIMRF02.class);
    }
}
