package com.bbva.pzic.risks.dao.rest.mapper.impl;

import com.bbva.pzic.risks.business.dto.InputSearchRisksFinancialInformation;
import com.bbva.pzic.risks.dao.model.financialinformation.*;
import com.bbva.pzic.risks.dao.rest.mapper.IRestSearchRisksFinancialInformationMapper;
import com.bbva.pzic.risks.facade.v0.dto.*;
import com.bbva.pzic.risks.util.mappers.Mapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.apache.commons.lang.StringUtils.isEmpty;

@Mapper("restSearchRisksFinancialInformationMapper")
public class RestSearchRisksFinancialInformationMapper implements IRestSearchRisksFinancialInformationMapper {

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public HashMap<String, String> mapInQueryParams(final InputSearchRisksFinancialInformation input) {
        HashMap<String, String> queryParams = new HashMap<>();
        if (input.getCustomer() != null) {
            queryParams.put("customerId", input.getCustomer().getCustomerId());
        }
        if (input.getIdentityDocument() != null) {
            queryParams.put("identityDocument.documentType.id", input.getIdentityDocument().getDocumentType().getId());
            queryParams.put("identityDocument.documentNumber", input.getIdentityDocument().getDocumentNumber());
        }
        return queryParams;
    }

    @Override
    public HashMap<String, String> mapInHeader(final InputSearchRisksFinancialInformation input) {
        HashMap<String, String> headerParams = new HashMap<>();
        headerParams.put("consumerId", input.getConsumerId());
        headerParams.put("callingChannel", input.getCallingChannel());
        return headerParams;
    }

    @Override
    public SearchFinancialInformation mapOut(final ModelSearchRisksFinancialInformationResponseBody model) {
        if (model.getData() == null) {
            return null;
        }
        SearchFinancialInformation searchFinancialInformation = new SearchFinancialInformation();
        searchFinancialInformation.setCustomerId(model.getData().getCustomerId());
        searchFinancialInformation.setIdentityDocument(mapOutIdentityDocument(model.getData().getIdentityDocument()));
        searchFinancialInformation.setContractedProducts(mapOutContractedProducts(model.getData().getRelatedProducts()));
        searchFinancialInformation.setDirectDebt(mapOutDirectDebt(model.getData().getInformationBbva()));
        searchFinancialInformation.setMonthlyRegistation(mapOutMonthlyRegistation(model.getData().getMonthlyRegistation()));
        searchFinancialInformation.setHistoricalSpread(mapOutHistoricalSpread(model.getData().getSpreadHistory()));
        searchFinancialInformation.setTotalDebtInformation(mapOutTotalDebtInformation(model.getData().getDebtSsff()));
        searchFinancialInformation.setProductDebtBreakdown(mapOutProductDebtBreakdown(model.getData().getDebtProductSssff()));
        searchFinancialInformation.setAdditionalInfo(mapOutAdditionalInfo(model.getData().getAdditional()));
        return searchFinancialInformation;
    }

    private IdentityDocument mapOutIdentityDocument(final ModelIdentityDocument modelIdentityDocument) {
        if (modelIdentityDocument == null) {
            return null;
        }
        IdentityDocument identityDocument = new IdentityDocument();
        identityDocument.setDocumentNumber(modelIdentityDocument.getDocumentNumber());
        identityDocument.setDocumentType(mapOutDocumentType(modelIdentityDocument.getDocumentType()));
        return identityDocument;
    }

    private DocumentType mapOutDocumentType(final ModelDocumentType modelDocumentType) {
        if (modelDocumentType == null) {
            return null;
        }
        DocumentType documentType = new DocumentType();
        documentType.setId(translator.translateBackendEnumValueStrictly("risks.riskFinancialInformation.documentType.id", modelDocumentType.getId()));
        return documentType;
    }


    private List<ContractedProduct> mapOutContractedProducts(final List<ModelRelatedProduct> modelRelatedProducts) {
        if (CollectionUtils.isEmpty(modelRelatedProducts)) {
            return null;
        }
        return modelRelatedProducts.stream().filter(Objects::nonNull).map(this::mapOutContractedProduct).collect(Collectors.toList());
    }

    private ContractedProduct mapOutContractedProduct(final ModelRelatedProduct modelRelatedProduct) {
        ContractedProduct contractedProduct = new ContractedProduct();
        contractedProduct.setId(modelRelatedProduct.getId());
        contractedProduct.setName(modelRelatedProduct.getName());
        contractedProduct.setCurrentBalance(mapOutCurrentBalance(modelRelatedProduct.getCurrentBalanced()));
        contractedProduct.setIsLinked(modelRelatedProduct.getIsLinked());
        contractedProduct.setProductType(mapOutContractedProductProductType(modelRelatedProduct.getProductType()));
        return contractedProduct;
    }

    private Amount mapOutCurrentBalance(final ModelCurrentBalanced modelCurrentBalanced) {
        if (modelCurrentBalanced == null) {
            return null;
        }
        Amount amount = new Amount();
        amount.setAmount(modelCurrentBalanced.getAmount());
        amount.setCurrency(modelCurrentBalanced.getCurrency());
        return amount;
    }

    private ProductType mapOutContractedProductProductType(final ModelProductType modelProductType) {
        if (modelProductType == null) {
            return null;
        }
        ProductType productType = new ProductType();
        productType.setId(modelProductType.getId());
        productType.setName(modelProductType.getName());
        return productType;
    }

    private DirectDebt mapOutDirectDebt(final ModelInformationBbva modelInformationBbva) {
        if (modelInformationBbva == null) {
            return null;
        }
        DirectDebt directDebt = new DirectDebt();
        directDebt.setMonth(modelInformationBbva.getMonth());
        directDebt.setFinancialSystemDebt(mapOutFinancialSystemDebt(modelInformationBbva));
        directDebt.setBbvaDebt(mapOutBbvaDebt(modelInformationBbva));
        directDebt.setDirectFee(modelInformationBbva.getFeeDirect());
        directDebt.setGrossMargin(mapOutGrossMargin(modelInformationBbva));
        return directDebt;
    }

    private Amount mapOutFinancialSystemDebt(final ModelInformationBbva modelInformationBbva) {
        if (modelInformationBbva.getDebtSsff() == null && isEmpty(modelInformationBbva.getCurrencyDebtSsff())) {
            return null;
        }
        Amount amount = new Amount();
        amount.setAmount(modelInformationBbva.getDebtSsff());
        amount.setCurrency(modelInformationBbva.getCurrencyDebtSsff());
        return amount;
    }

    private Amount mapOutBbvaDebt(final ModelInformationBbva modelInformationBbva) {
        if (modelInformationBbva.getDebtBbva() == null && isEmpty(modelInformationBbva.getCurrencyDebtBbva())) {
            return null;
        }
        Amount amount = new Amount();
        amount.setAmount(modelInformationBbva.getDebtBbva());
        amount.setCurrency(modelInformationBbva.getCurrencyDebtBbva());
        return amount;
    }

    private Amount mapOutGrossMargin(final ModelInformationBbva modelInformationBbva) {
        if (modelInformationBbva.getMarginGross() == null && isEmpty(modelInformationBbva.getCurrencyMarginGross())) {
            return null;
        }
        Amount amount = new Amount();
        amount.setAmount(modelInformationBbva.getMarginGross());
        amount.setCurrency(modelInformationBbva.getCurrencyMarginGross());
        return amount;
    }


    private List<MonthlyRegister> mapOutMonthlyRegistation(final List<ModelMonthlyRegister> modelMonthlyRegistation) {
        if (CollectionUtils.isEmpty(modelMonthlyRegistation)) {
            return null;
        }
        return modelMonthlyRegistation.stream().filter(Objects::nonNull).map(this::mapOutMonthlyRegister).collect(Collectors.toList());
    }

    private MonthlyRegister mapOutMonthlyRegister(ModelMonthlyRegister modelMonthlyRegister) {
        MonthlyRegister monthlyRegister = new MonthlyRegister();
        monthlyRegister.setMonth(modelMonthlyRegister.getMonth());
        monthlyRegister.setProduct(mapOutProduct(modelMonthlyRegister.getProduct()));
        monthlyRegister.setBillingAmount(mapOutMonthlyRegisterBillingAmount(modelMonthlyRegister));
        monthlyRegister.setAnnualEffectiveRate(modelMonthlyRegister.getTea());
        monthlyRegister.setSpread(modelMonthlyRegister.getSpread());
        return monthlyRegister;
    }

    private Product mapOutProduct(final ModelProduct modelProduct) {
        if (modelProduct == null) {
            return null;
        }
        Product product = new Product();
        product.setId(modelProduct.getId());
        product.setName(modelProduct.getName());
        return product;
    }

    private Amount mapOutMonthlyRegisterBillingAmount(final ModelMonthlyRegister modelMonthlyRegister) {
        if (modelMonthlyRegister.getAmount() == null && isEmpty(modelMonthlyRegister.getCurrency())) {
            return null;
        }
        Amount amount = new Amount();
        amount.setAmount(modelMonthlyRegister.getAmount());
        amount.setCurrency(modelMonthlyRegister.getCurrency());
        return amount;
    }


    private List<Spread> mapOutHistoricalSpread(final List<ModelSpread> modelSpreadHistory) {
        if (CollectionUtils.isEmpty(modelSpreadHistory)) {
            return null;
        }
        return modelSpreadHistory.stream().filter(Objects::nonNull).map(this::mapOutSpread).collect(Collectors.toList());
    }

    private Spread mapOutSpread(final ModelSpread modelSpread) {
        Spread spread = new Spread();
        spread.setInitialMonth(modelSpread.getInitialMonth());
        spread.setFinalMonth(modelSpread.getFinalMonth());
        spread.setBillingAmount(mapOutSpreadBillingAmount(modelSpread));
        spread.setAnnualEffectiveRate(modelSpread.getTea());
        spread.setSpread(modelSpread.getPercentageSpread());
        return spread;
    }

    private Amount mapOutSpreadBillingAmount(final ModelSpread modelSpread) {
        if (modelSpread.getAmount() == null && isEmpty(modelSpread.getCurrency())) {
            return null;
        }
        Amount amount = new Amount();
        amount.setAmount(modelSpread.getAmount());
        amount.setCurrency(modelSpread.getCurrency());
        return amount;
    }

    private List<DebtInformation> mapOutTotalDebtInformation(final List<ModelDebtInformation> modelDebtSsff) {
        if (CollectionUtils.isEmpty(modelDebtSsff)) {
            return null;
        }
        return modelDebtSsff.stream().filter(Objects::nonNull).map(this::mapOutDebtInformation).collect(Collectors.toList());
    }

    private DebtInformation mapOutDebtInformation(final ModelDebtInformation modelDebtInformation) {
        DebtInformation debtInformation = new DebtInformation();
        debtInformation.setMonth(modelDebtInformation.getMonth());
        debtInformation.setFinancialSystemAmount(mapOutDebtInformationFinancialSystemAmount(modelDebtInformation));
        debtInformation.setBbvaAmount(mapOutBbvaAmount(modelDebtInformation));
        debtInformation.setDirectFee(modelDebtInformation.getMonthlyFee());
        return debtInformation;
    }

    private Amount mapOutDebtInformationFinancialSystemAmount(final ModelDebtInformation modelDebtInformation) {
        if (modelDebtInformation.getBalanceSsff() == null && isEmpty(modelDebtInformation.getCurrencySsff())) {
            return null;
        }
        Amount amount = new Amount();
        amount.setAmount(modelDebtInformation.getBalanceSsff());
        amount.setCurrency(modelDebtInformation.getCurrencySsff());
        return amount;
    }

    private Amount mapOutBbvaAmount(final ModelDebtInformation modelDebtInformation) {
        if (modelDebtInformation.getBalanceBbva() == null && isEmpty(modelDebtInformation.getCurrencyBbva())) {
            return null;
        }
        Amount amount = new Amount();
        amount.setAmount(modelDebtInformation.getBalanceBbva());
        amount.setCurrency(modelDebtInformation.getCurrencyBbva());
        return amount;
    }

    private List<DebtBreakdown> mapOutProductDebtBreakdown(final List<ModelDebtBreakdown> modelDebtProductSssff) {
        if (CollectionUtils.isEmpty(modelDebtProductSssff)) {
            return null;
        }
        return modelDebtProductSssff.stream().filter(Objects::nonNull).map(this::mapOutDebtBreakdown).collect(Collectors.toList());
    }

    private DebtBreakdown mapOutDebtBreakdown(final ModelDebtBreakdown modelDebtBreakdown) {
        DebtBreakdown debtBreakdown = new DebtBreakdown();
        debtBreakdown.setMonth(modelDebtBreakdown.getMonth());
        debtBreakdown.setProductType(mapOutDebtBreakdownProductType(modelDebtBreakdown.getProduct()));
        debtBreakdown.setBankDebt(mapOutBankDebtList(modelDebtBreakdown.getFinantialDebt()));
        debtBreakdown.setFinancialSystemAmount(mapOutDebtBreakdownFinancialSystemAmount(modelDebtBreakdown));
        return debtBreakdown;
    }

    private ProductType mapOutDebtBreakdownProductType(final ModelProduct modelProduct) {
        if (modelProduct == null) {
            return null;
        }
        ProductType productType = new ProductType();
        productType.setId(modelProduct.getId());
        productType.setName(modelProduct.getName());
        return productType;
    }

    private List<BankDebt> mapOutBankDebtList(final List<ModelFinantialDebt> modelFinantialDebt) {
        if (CollectionUtils.isEmpty(modelFinantialDebt)) {
            return null;
        }
        return modelFinantialDebt.stream().filter(Objects::nonNull).map(this::mapOutBankDebt).collect(Collectors.toList());
    }

    private BankDebt mapOutBankDebt(final ModelFinantialDebt modelFinantialDebt) {
        if (modelFinantialDebt == null) {
            return null;
        }
        BankDebt bankDebt = new BankDebt();
        bankDebt.setBankName(modelFinantialDebt.getBank() == null ? null : modelFinantialDebt.getBank().getName());
        bankDebt.setAmount(mapOutBankDebtAmount(modelFinantialDebt));
        bankDebt.setDirectFee(modelFinantialDebt.getMonthlyFee());
        return bankDebt;
    }

    private Amount mapOutBankDebtAmount(final ModelFinantialDebt modelFinantialDebt) {
        if (modelFinantialDebt.getAmount() == null && isEmpty(modelFinantialDebt.getCurrency())) {
            return null;
        }
        Amount amount = new Amount();
        amount.setAmount(modelFinantialDebt.getAmount());
        amount.setCurrency(modelFinantialDebt.getCurrency());
        return amount;
    }


    private Amount mapOutDebtBreakdownFinancialSystemAmount(final ModelDebtBreakdown modelDebtBreakdown) {
        if (modelDebtBreakdown.getDebtSsff() == null && isEmpty(modelDebtBreakdown.getCurrencyDebtSsff())) {
            return null;
        }
        Amount amount = new Amount();
        amount.setAmount(modelDebtBreakdown.getDebtSsff());
        amount.setCurrency(modelDebtBreakdown.getCurrencyDebtSsff());
        return amount;
    }


    private AdditionalInfo mapOutAdditionalInfo(final ModelAdditionalInformation modelAdditional) {
        if (modelAdditional == null) {
            return null;
        }
        AdditionalInfo additionalInfo = new AdditionalInfo();
        additionalInfo.setBureauCode(modelAdditional.getBuro());
        additionalInfo.setBranch(mapOutAdditionalInfoBranch(modelAdditional.getBranch()));
        additionalInfo.setBusinessAgent(mapOutBusinessAgent(modelAdditional.getManager()));
        additionalInfo.setRating(modelAdditional.getRating());
        additionalInfo.setEconomicGroup(modelAdditional.getEconomicGroup() == null ? null : modelAdditional.getEconomicGroup().getName());
        additionalInfo.setEntailmentCode(modelAdditional.getEntailmentCode());
        additionalInfo.setIsPaymentHolder(modelAdditional.getPaymentHolder());
        additionalInfo.setCreditLimit(mapOutCreditLimitList(modelAdditional.getLimitCredit()));
        additionalInfo.setSegment(mapOutSegment(modelAdditional.getSegment()));
        return additionalInfo;
    }

    private Branch mapOutAdditionalInfoBranch(final ModelBranch modelBranch) {
        if (modelBranch == null) {
            return null;
        }
        Branch branch = new Branch();
        branch.setId(modelBranch.getCode());
        branch.setName(modelBranch.getName());
        return branch;
    }

    private BusinessAgent mapOutBusinessAgent(final ModelManager manager) {
        if (manager == null) {
            return null;
        }
        BusinessAgent businessAgent = new BusinessAgent();
        businessAgent.setId(manager.getCode());
        businessAgent.setRegistrationCode(manager.getCodeRegistration());
        businessAgent.setFullName(manager.getName());
        return businessAgent;
    }

    private List<CreditLimit> mapOutCreditLimitList(final List<ModelLimitCredit> limitCreditList) {
        if (CollectionUtils.isEmpty(limitCreditList)) {
            return null;
        }
        return limitCreditList.stream().filter(Objects::nonNull).map(this::mapOutCreditLimit).collect(Collectors.toList());
    }

    private CreditLimit mapOutCreditLimit(final ModelLimitCredit modelLimitCredit) {
        CreditLimit creditLimit = new CreditLimit();
        creditLimit.setAmount(mapOutCreditLimitAmount(modelLimitCredit));
        creditLimit.setUsedPercentage(modelLimitCredit.getUsedPercentage());
        creditLimit.setContractNumber(modelLimitCredit.getContractNumber());
        creditLimit.setDueDate(modelLimitCredit.getDueDate());
        return creditLimit;
    }

    private Amount mapOutCreditLimitAmount(final ModelLimitCredit modelLimitCredit) {
        if (modelLimitCredit.getAmount() == null && isEmpty(modelLimitCredit.getCurrency())) {
            return null;
        }
        Amount amount = new Amount();
        amount.setAmount(modelLimitCredit.getAmount());
        amount.setCurrency(modelLimitCredit.getCurrency());
        return amount;
    }

    private Segment mapOutSegment(final ModelSegment modelSegment) {
        if (modelSegment == null) {
            return null;
        }
        Segment segment = new Segment();
        segment.setId(modelSegment.getId());
        segment.setName(modelSegment.getName());
        return segment;
    }
}