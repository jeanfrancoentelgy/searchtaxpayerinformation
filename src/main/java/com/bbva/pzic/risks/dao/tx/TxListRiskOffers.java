package com.bbva.pzic.risks.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.risks.business.dto.InputListRiskOffers;
import com.bbva.pzic.risks.dao.model.hya4.*;
import com.bbva.pzic.risks.dao.tx.mapper.ITxListRiskOffersMapper;
import com.bbva.pzic.risks.facade.v1.dto.RiskOffer;
import com.bbva.pzic.routine.commons.utils.host.templates.Tx;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.DoubleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 12/06/2020.
 *
 * @author Entelgy
 */
@Tx
public class TxListRiskOffers extends DoubleOutputFormat<InputListRiskOffers, FormatoHYMR405, List<RiskOffer>, FormatoHYMR403, FormatoHYMR404> {

    private final ITxListRiskOffersMapper txListRiskOffersMapper;

    @Autowired
    public TxListRiskOffers(@Qualifier("transaccionHya4") InvocadorTransaccion<PeticionTransaccionHya4, RespuestaTransaccionHya4> transaction,
                            ITxListRiskOffersMapper txListRiskOffersMapper) {
        super(transaction, PeticionTransaccionHya4::new, ArrayList::new, FormatoHYMR403.class, FormatoHYMR404.class);
        this.txListRiskOffersMapper = txListRiskOffersMapper;
    }

    @Override
    protected FormatoHYMR405 mapInput(InputListRiskOffers dtoIn) {
        return txListRiskOffersMapper.mapIn(dtoIn);
    }

    @Override
    protected List<RiskOffer> mapFirstOutputFormat(FormatoHYMR403 formatOutput, InputListRiskOffers dtoIn, List<RiskOffer> dtoOut) {
        return txListRiskOffersMapper.mapOut(formatOutput, dtoOut);
    }

    @Override
    protected List<RiskOffer> mapSecondOutputFormat(FormatoHYMR404 formatOutput, InputListRiskOffers dtoIn, List<RiskOffer> dtoOut) {
        return txListRiskOffersMapper.mapOut2(formatOutput, dtoOut);
    }
}
