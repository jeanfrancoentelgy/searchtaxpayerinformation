package com.bbva.pzic.risks.dao.model.collaborators;


import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import java.util.List;

public class ModelCollaborator {

    private String registro;

    @DatoAuditable(omitir = true)
    private String nombres;

    private ModelRole puesto;

    private ModelBank banco;

    private List<ModelEconomicGroupLimits> limitesPorGrupoEconomico;

    private List<ModelLimitsGaranteed> limitesGarantizados;

    private List<ModelProductLimits> limitePorProducto;

    public String getRegistro() {return registro;}

    public void setRegistro(String registro) {this.registro = registro;}

    public String getNombres() {return nombres;}

    public void setNombres(String nombres) {this.nombres = nombres;}

    public ModelRole getPuesto() {return puesto;}

    public void setPuesto(ModelRole puesto) {this.puesto = puesto;}

    public ModelBank getBanco() {return banco;}

    public void setBanco(ModelBank banco) {this.banco = banco;}

    public List<ModelEconomicGroupLimits> getLimitesPorGrupoEconomico() {return limitesPorGrupoEconomico;}

    public void setLimitesPorGrupoEconomico(List<ModelEconomicGroupLimits> limitesPorGrupoEconomico) {this.limitesPorGrupoEconomico = limitesPorGrupoEconomico;}

    public List<ModelProductLimits> getLimitePorProducto() {return limitePorProducto;}

    public void setLimitePorProducto(List<ModelProductLimits> limitePorProducto) {this.limitePorProducto = limitePorProducto;}

    public List<ModelLimitsGaranteed> getLimitesGarantizados() {return limitesGarantizados;}

    public void setLimitesGarantizados(List<ModelLimitsGaranteed> limitesGarantizados) {this.limitesGarantizados = limitesGarantizados;}
}
