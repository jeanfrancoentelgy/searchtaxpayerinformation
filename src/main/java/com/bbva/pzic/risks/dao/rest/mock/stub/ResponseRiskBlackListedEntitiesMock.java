package com.bbva.pzic.risks.dao.rest.mock.stub;

import com.bbva.pzic.risks.dao.model.blacklisted.ModelBlackListedEntity;
import com.bbva.pzic.risks.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 04/12/2018.
 *
 * @author Entelgy
 */

public class ResponseRiskBlackListedEntitiesMock {

    private static final ResponseRiskBlackListedEntitiesMock INSTANCE = new ResponseRiskBlackListedEntitiesMock();

    private ObjectMapperHelper objectMapper = ObjectMapperHelper.getInstance();

    private ResponseRiskBlackListedEntitiesMock() {
    }

    public static ResponseRiskBlackListedEntitiesMock getInstance() {
        return INSTANCE;
    }

    public ModelBlackListedEntity buildBlackListedEntity() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/risks/dao/rest/mock/response_list_risk_blackListed_entities.json"),
                ModelBlackListedEntity.class);
    }
}
