package com.bbva.pzic.risks.dao.model.rif2;

import java.util.Date;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;



/**
 * Formato de datos <code>RIMRF22</code> de la transacci&oacute;n <code>RIF2</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "RIMRF22")
@RooJavaBean
@RooSerializable
public class FormatoRIMRF22 {
	
	/**
	 * <p>Campo <code>IDEXCLU</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 1, nombre = "IDEXCLU", tipo = TipoCampo.ENTERO, longitudMinima = 7, longitudMaxima = 7)
	private Integer idexclu;
	
	/**
	 * <p>Campo <code>CODBANC</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "CODBANC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
	private String codbanc;
	
	/**
	 * <p>Campo <code>NOMBANC</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "NOMBANC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 12, longitudMaxima = 12)
	private String nombanc;
	
	/**
	 * <p>Campo <code>CODEXCL</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "CODEXCL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String codexcl;
	
	/**
	 * <p>Campo <code>NOMEXCL</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "NOMEXCL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String nomexcl;
	
	/**
	 * <p>Campo <code>CODINEL</code>, &iacute;ndice: <code>6</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 6, nombre = "CODINEL", tipo = TipoCampo.ENTERO, longitudMinima = 3, longitudMaxima = 3)
	private Integer codinel;
	
	/**
	 * <p>Campo <code>SBCOINE</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "SBCOINE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String sbcoine;
	
	/**
	 * <p>Campo <code>NIVEXCL</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "NIVEXCL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String nivexcl;
	
	/**
	 * <p>Campo <code>FECEXCL</code>, &iacute;ndice: <code>9</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 9, nombre = "FECEXCL", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecexcl;
	
}