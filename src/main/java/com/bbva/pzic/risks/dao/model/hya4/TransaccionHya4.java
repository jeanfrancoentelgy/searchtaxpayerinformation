package com.bbva.pzic.risks.dao.model.hya4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Created on 12/06/2020.
 * 
 * @author Entelgy
 */
@Component("transaccionHya4")
public class TransaccionHya4
		implements
			InvocadorTransaccion<PeticionTransaccionHya4, RespuestaTransaccionHya4> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionHya4 invocar(PeticionTransaccionHya4 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionHya4.class,
				RespuestaTransaccionHya4.class, transaccion);
	}

	@Override
	public RespuestaTransaccionHya4 invocarCache(
			PeticionTransaccionHya4 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionHya4.class,
				RespuestaTransaccionHya4.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		// this method does not have to be used anymore
	}
}