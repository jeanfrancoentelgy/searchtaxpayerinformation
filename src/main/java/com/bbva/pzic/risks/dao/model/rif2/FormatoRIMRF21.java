package com.bbva.pzic.risks.dao.model.rif2;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>RIMRF21</code> de la transacci&oacute;n <code>RIF2</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "RIMRF21")
@RooJavaBean
@RooSerializable
public class FormatoRIMRF21 {

	/**
	 * <p>Campo <code>CODCENT</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "CODCENT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String codcent;
	
}