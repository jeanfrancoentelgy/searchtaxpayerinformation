package com.bbva.pzic.risks.dao.model.financialinformation;

public class ModelEconomicGroup {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
