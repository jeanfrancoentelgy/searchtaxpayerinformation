package com.bbva.pzic.risks.dao.model.rif1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>RIF1</code>
 * 
 * @see PeticionTransaccionRif1
 * @see RespuestaTransaccionRif1
 */
@Component
public class TransaccionRif1 implements InvocadorTransaccion<PeticionTransaccionRif1,RespuestaTransaccionRif1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionRif1 invocar(PeticionTransaccionRif1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionRif1.class, RespuestaTransaccionRif1.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionRif1 invocarCache(PeticionTransaccionRif1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionRif1.class, RespuestaTransaccionRif1.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}