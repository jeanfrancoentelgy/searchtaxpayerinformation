package com.bbva.pzic.risks.dao.model.collaborators;


public class ModelCollaborators {

    private ModelCollaborator colaborador;

    private String id;

    public ModelCollaborator getColaborador() {
        return colaborador;
    }

    public void setColaborador(ModelCollaborator colaborador) {
        this.colaborador = colaborador;
    }

    public String getId() {return id;}

    public void setId(String id) {this.id = id;}
}
