package com.bbva.pzic.risks.dao;

import com.bbva.pzic.risks.business.dto.InputListRiskExclusions;
import com.bbva.pzic.risks.business.dto.InputListRiskFinancialDebts;
import com.bbva.pzic.risks.business.dto.InputListRiskOffers;
import com.bbva.pzic.risks.dao.model.blacklisted.ModelClientRequest;
import com.bbva.pzic.risks.dao.model.blacklisted.ModelProfileRequest;
import com.bbva.pzic.risks.dao.model.profille.ModelCustomerRiskProfile;
import com.bbva.pzic.risks.facade.v1.dto.Exclusion;
import com.bbva.pzic.risks.facade.v1.dto.FinancialDebt;
import com.bbva.pzic.risks.facade.v1.dto.RiskBlackListedEntity;
import com.bbva.pzic.risks.facade.v1.dto.RiskOffer;

import java.util.List;

public interface IRisksDAOV1 {

    List<RiskBlackListedEntity> listRiskBlackListedEntities(ModelClientRequest request);

    ModelCustomerRiskProfile getCustomerRiskProfile(String customerId);

    void modifyCustomerRiskProfile(ModelProfileRequest request);

    List<FinancialDebt> listRiskFinancialDebts(InputListRiskFinancialDebts input);

    List<RiskOffer> listRiskOffers(InputListRiskOffers input);

    List<Exclusion> listRiskExclusions(InputListRiskExclusions input);
}
