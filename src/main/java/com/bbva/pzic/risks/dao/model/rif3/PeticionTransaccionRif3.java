package com.bbva.pzic.risks.dao.model.rif3;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;


/**
 * <p>Transacci&oacute;n <code>RIF3</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionRif3</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionRif3</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.RIF3.D1200915
 * RIF3CONSULTA POSICION SIST.TRIBUTARIO  RI        RI2C00F3     01 RIMRF31             RIF3  NN0000NNNNNN    SSTN     E  SNNSSNNN  NN                2020-09-15XP90749 2020-09-1519.36.10XP90749 2020-09-15-19.25.06.334892XP90749 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.RIMRF31.D1200915
 * RIMRF31 �CODIGO DE CLIENTE             �F�02�00012�01�00001�TIPDOC �TIPO DE DOCUMENTO   �A�001�0�R�        �
 * RIMRF31 �CODIGO DE CLIENTE             �F�02�00012�02�00002�NUMDOC �NUMERO DOCUMENTO    �A�011�0�R�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.RIMRF32.D1200915
 * RIMRF32 �SALIDA POSICION TRIBUTARIA    �X�18�00309�01�00001�NOMAPE �NOMBRE CONTRIBUYENTE�A�080�0�S�        �
 * RIMRF32 �SALIDA POSICION TRIBUTARIA    �X�18�00309�02�00081�FCIERRE�FECHA INFORMACION TR�A�010�0�S�        �
 * RIMRF32 �SALIDA POSICION TRIBUTARIA    �X�18�00309�03�00091�IMPDEU �IMPORTE DEUDA VENCID�N�012�2�S�        �
 * RIMRF32 �SALIDA POSICION TRIBUTARIA    �X�18�00309�04�00103�DIVISA �DIVISA DE LA DEUDA  �A�003�0�S�        �
 * RIMRF32 �SALIDA POSICION TRIBUTARIA    �X�18�00309�05�00106�ESTCON �ESTADO CONTRIBUYENTE�A�002�0�S�        �
 * RIMRF32 �SALIDA POSICION TRIBUTARIA    �X�18�00309�06�00108�CONCON �CONDICION CONTRIBUYE�A�001�0�S�        �
 * RIMRF32 �SALIDA POSICION TRIBUTARIA    �X�18�00309�07�00109�TIPCON �TIPO DE CONTRIBUYENT�A�060�0�S�        �
 * RIMRF32 �SALIDA POSICION TRIBUTARIA    �X�18�00309�08�00169�FCONST �FECHA CONSTITUCION  �A�010�0�S�        �
 * RIMRF32 �SALIDA POSICION TRIBUTARIA    �X�18�00309�09�00179�FINIACT�FECHA INICIO ACTIVID�A�010�0�S�        �
 * RIMRF32 �SALIDA POSICION TRIBUTARIA    �X�18�00309�10�00189�CIIU   �CODIGO ACTI.ECONOMIC�A�005�0�S�        �
 * RIMRF32 �SALIDA POSICION TRIBUTARIA    �X�18�00309�11�00194�DEUBIG �DESCRIPCION UBIGEO  �A�006�0�S�        �
 * RIMRF32 �SALIDA POSICION TRIBUTARIA    �X�18�00309�12�00200�UBIGEO �COD.AGRUPACION GEOGR�A�006�0�S�        �
 * RIMRF32 �SALIDA POSICION TRIBUTARIA    �X�18�00309�13�00206�TIPVIA �TIPO DE VIA         �A�002�0�S�        �
 * RIMRF32 �SALIDA POSICION TRIBUTARIA    �X�18�00309�14�00208�DTIPVIA�DESCRIPCION TIPO VIA�A�025�0�S�        �
 * RIMRF32 �SALIDA POSICION TRIBUTARIA    �X�18�00309�15�00233�TIPZON �TIPO DE ZONA        �A�002�0�S�        �
 * RIMRF32 �SALIDA POSICION TRIBUTARIA    �X�18�00309�16�00235�DTIPZON�DESCRI.TIPO ZONA    �A�025�0�S�        �
 * RIMRF32 �SALIDA POSICION TRIBUTARIA    �X�18�00309�17�00260�NOMZONA�NOMBRE DE ZONA      �A�025�0�S�        �
 * RIMRF32 �SALIDA POSICION TRIBUTARIA    �X�18�00309�18�00285�NOMVIA �NOMBRE DE VIA       �A�025�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.RIF3.D1200915
 * RIF3RIMRF32 RINCRF32RI2C00F31S51                           XP90749 2020-09-15-23.29.01.996031XP90749 2020-09-15-23.29.01.996073
</pre></code>
 * 
 * @see RespuestaTransaccionRif3
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "RIF3",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionRif3.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoRIMRF31.class})
@RooJavaBean
@RooSerializable
public class PeticionTransaccionRif3 implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}