package com.bbva.pzic.risks.dao.model.karct003_1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Transacci&oacute;n <code>KARCT003</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionKarct003_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionKarct003_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: KARCT003-01-PE.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;KARCT003&quot; application=&quot;KARC&quot; version=&quot;01&quot; country=&quot;PE&quot;
 * language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;dto name=&quot;customer&quot; package=&quot;com.bbva.karc.dto.financialdebts.dtos.CustomerDTO&quot; artifactId=&quot;KARCC003&quot;
 * mandatory=&quot;1&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;list name=&quot;data&quot; order=&quot;1&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;dataOut&quot; package=&quot;com.bbva.karc.dto.financialdebts.dtos.DataDTO&quot; artifactId=&quot;KARCC003&quot; mandatory=&quot;0&quot;
 * order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;financialDebtType&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;disposedPercentage&quot; type=&quot;Double&quot; size=&quot;5&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;reportingDate&quot; type=&quot;Date (YYYY-MM-DD)&quot; size=&quot;0&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;nonBBVADisposedAmount&quot;
 * package=&quot;com.bbva.karc.dto.financialdebts.dtos.NonBBVADisposedAmountDTO&quot; artifactId=&quot;KARCC003&quot;
 * mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;amount&quot; type=&quot;Double&quot; size=&quot;20&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;5&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;disposedAmount&quot; package=&quot;com.bbva.karc.dto.financialdebts.dtos.DisposedAmountDTO&quot;
 * artifactId=&quot;KARCC003&quot; mandatory=&quot;1&quot; order=&quot;4&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;amount&quot; type=&quot;Double&quot; size=&quot;20&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;5&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Transaccion para obtener información de la deuda financiera de un cliente&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionKarct003_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "KARCT003",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionKarct003_1.class,
	atributos = {@Atributo(nombre = "country", valor = "PE")}
)
@RooJavaBean
@RooSerializable
public class PeticionTransaccionKarct003_1 {
		
		/**
	 * <p>Campo <code>customer</code>, &iacute;ndice: <code>1</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 1, nombre = "customer", tipo = TipoCampo.DTO)
	private Customer customer;
	
}