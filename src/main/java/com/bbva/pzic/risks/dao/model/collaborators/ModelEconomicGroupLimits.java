package com.bbva.pzic.risks.dao.model.collaborators;

public class ModelEconomicGroupLimits {

    private String grupo;

    private ModelLimit limite;

    private String nivelDelegacion;

    public String getGrupo() {return grupo;}

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public ModelLimit getLimite() {return limite;}

    public void setLimite(ModelLimit limite) {
        this.limite = limite;
    }

    public String getNivelDelegacion() {
        return nivelDelegacion;
    }

    public void setNivelDelegacion(String nivelDelegacion) {
        this.nivelDelegacion = nivelDelegacion;
    }
}
