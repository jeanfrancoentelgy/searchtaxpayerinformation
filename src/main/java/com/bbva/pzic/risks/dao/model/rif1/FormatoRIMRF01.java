package com.bbva.pzic.risks.dao.model.rif1;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>RIMRF01</code> de la transacci&oacute;n <code>RIF1</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "RIMRF01")
@RooJavaBean
@RooSerializable
public class FormatoRIMRF01 {

	/**
	 * <p>Campo <code>TIPODOC</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "TIPODOC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipodoc;
	
	/**
	 * <p>Campo <code>NUMEDOC</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "NUMEDOC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 11, longitudMaxima = 11)
	private String numedoc;
	
	/**
	 * <p>Campo <code>CODCENT</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "CODCENT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String codcent;
	
}