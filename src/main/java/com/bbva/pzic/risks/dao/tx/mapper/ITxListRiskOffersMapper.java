package com.bbva.pzic.risks.dao.tx.mapper;

import com.bbva.pzic.risks.business.dto.InputListRiskOffers;
import com.bbva.pzic.risks.dao.model.hya4.FormatoHYMR403;
import com.bbva.pzic.risks.dao.model.hya4.FormatoHYMR404;
import com.bbva.pzic.risks.dao.model.hya4.FormatoHYMR405;
import com.bbva.pzic.risks.facade.v1.dto.RiskOffer;

import java.util.List;

/**
 * Created on 12/06/2020.
 *
 * @author Entelgy
 */
public interface ITxListRiskOffersMapper {

    FormatoHYMR405 mapIn(InputListRiskOffers dtoIn);

    List<RiskOffer> mapOut(FormatoHYMR403 formatOutput, List<RiskOffer> dtoOut);

    List<RiskOffer> mapOut2(FormatoHYMR404 formatOutput, List<RiskOffer> dtoOut);
}
