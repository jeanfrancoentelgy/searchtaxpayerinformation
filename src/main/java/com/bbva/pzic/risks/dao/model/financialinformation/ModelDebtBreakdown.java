package com.bbva.pzic.risks.dao.model.financialinformation;

import java.math.BigDecimal;
import java.util.List;

public class ModelDebtBreakdown {

    private String month;

    private ModelProduct product;

    private String currencyDebtSsff;

    private BigDecimal debtSsff;

    private List<ModelFinantialDebt> finantialDebt;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public ModelProduct getProduct() {
        return product;
    }

    public void setProduct(ModelProduct product) {
        this.product = product;
    }

    public String getCurrencyDebtSsff() {
        return currencyDebtSsff;
    }

    public void setCurrencyDebtSsff(String currencyDebtSsff) {
        this.currencyDebtSsff = currencyDebtSsff;
    }

    public BigDecimal getDebtSsff() {
        return debtSsff;
    }

    public void setDebtSsff(BigDecimal debtSsff) {
        this.debtSsff = debtSsff;
    }

    public List<ModelFinantialDebt> getFinantialDebt() {
        return finantialDebt;
    }

    public void setFinantialDebt(List<ModelFinantialDebt> finantialDebt) {
        this.finantialDebt = finantialDebt;
    }
}
