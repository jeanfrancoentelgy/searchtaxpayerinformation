package com.bbva.pzic.risks.dao.rest.mock;

import com.bbva.jee.arq.spring.core.rest.FilePart;
import com.bbva.pzic.risks.dao.model.OAuthCredential;
import com.bbva.pzic.risks.dao.rest.RestGetRimacToken;
import com.bbva.pzic.risks.util.NotAplly;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.List;

@Primary
@Component
public class RestGetRimacTokenMock extends RestGetRimacToken {

    @Override
    protected OAuthCredential connect(final String urlPropertyValue, final NotAplly entityPayload, List<FilePart> fileParts) {
        final OAuthCredential oAuthCredential = new OAuthCredential();
        oAuthCredential.setAccessToken("adad9q931313");
        oAuthCredential.setTokenType("bearer");
        return oAuthCredential;
    }

}
