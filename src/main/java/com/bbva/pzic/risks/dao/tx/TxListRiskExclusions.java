package com.bbva.pzic.risks.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.risks.business.dto.InputListRiskExclusions;
import com.bbva.pzic.risks.dao.model.rif2.FormatoRIMRF21;
import com.bbva.pzic.risks.dao.model.rif2.FormatoRIMRF22;
import com.bbva.pzic.risks.dao.model.rif2.PeticionTransaccionRif2;
import com.bbva.pzic.risks.dao.model.rif2.RespuestaTransaccionRif2;
import com.bbva.pzic.risks.dao.tx.mapper.ITxListRiskExclusionsMapper;
import com.bbva.pzic.risks.facade.v1.dto.Exclusion;
import com.bbva.pzic.routine.commons.utils.host.templates.Tx;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 17/06/2020.
 *
 * @author Entelgy
 */
@Tx
public class TxListRiskExclusions extends SingleOutputFormat<InputListRiskExclusions, FormatoRIMRF21, List<Exclusion>, FormatoRIMRF22> {

    private final ITxListRiskExclusionsMapper txListRiskExclusionsMapper;

    @Autowired
    public TxListRiskExclusions(@Qualifier("transaccionRif2") InvocadorTransaccion<PeticionTransaccionRif2, RespuestaTransaccionRif2> transaction,
                                ITxListRiskExclusionsMapper txListRiskExclusionsMapper) {
        super(transaction, PeticionTransaccionRif2::new, ArrayList::new, FormatoRIMRF22.class);
        this.txListRiskExclusionsMapper = txListRiskExclusionsMapper;
    }

    @Override
    protected FormatoRIMRF21 mapInput(InputListRiskExclusions input) {
        return txListRiskExclusionsMapper.mapIn(input);
    }

    @Override
    protected List<Exclusion> mapFirstOutputFormat(FormatoRIMRF22 formatOut, InputListRiskExclusions input, List<Exclusion> exclusions) {
        return txListRiskExclusionsMapper.mapOut(formatOut, exclusions);
    }
}
