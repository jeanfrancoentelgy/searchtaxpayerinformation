package com.bbva.pzic.risks.dao.model.blacklisted;

import com.bbva.pzic.risks.business.dto.ValidationGroup;

import javax.validation.constraints.NotNull;

/**
 * Created on 21/12/2018.
 *
 * @author Entelgy
 */
public class ModelProfileRequest {

    @NotNull(groups = ValidationGroup.ModifyCustomerRiskProfile.class)
    private String profileId;

    private String riskProfileType;

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getRiskProfileType() {
        return riskProfileType;
    }

    public void setRiskProfileType(String riskProfileType) {
        this.riskProfileType = riskProfileType;
    }
}
