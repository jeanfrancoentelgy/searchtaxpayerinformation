package com.bbva.pzic.risks.dao.model.collaborators;

public class ModelLimits {

    private String grupo;

    private String rango;

    private ModelLimit limite;

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getRango() {
        return rango;
    }

    public void setRango(String rango) {
        this.rango = rango;
    }

    public ModelLimit getLimite() {
        return limite;
    }

    public void setLimite(ModelLimit limite) {
        this.limite = limite;
    }
}
