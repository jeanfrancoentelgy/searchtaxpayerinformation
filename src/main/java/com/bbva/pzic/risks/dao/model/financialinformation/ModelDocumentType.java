package com.bbva.pzic.risks.dao.model.financialinformation;

public class ModelDocumentType {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
