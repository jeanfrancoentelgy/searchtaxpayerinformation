package com.bbva.pzic.risks.dao.model.financialinformation;

public class ModelIdentityDocument {

    private ModelDocumentType documentType;

    private String documentNumber;

    public ModelDocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(ModelDocumentType documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }
}
