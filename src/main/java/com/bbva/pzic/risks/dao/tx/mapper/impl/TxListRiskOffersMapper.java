package com.bbva.pzic.risks.dao.tx.mapper.impl;

import com.bbva.pzic.risks.business.dto.InputListRiskOffers;
import com.bbva.pzic.risks.dao.model.hya4.FormatoHYMR403;
import com.bbva.pzic.risks.dao.model.hya4.FormatoHYMR404;
import com.bbva.pzic.risks.dao.model.hya4.FormatoHYMR405;
import com.bbva.pzic.risks.dao.tx.mapper.ITxListRiskOffersMapper;
import com.bbva.pzic.risks.facade.v1.dto.*;
import com.bbva.pzic.risks.util.BusinessServiceUtil;
import com.bbva.pzic.risks.util.mappers.Mapper;
import com.bbva.pzic.risks.util.orika.converter.builtin.BooleanToStringConverter;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created on 12/06/2020.
 *
 * @author Entelgy
 */
@Mapper
public class TxListRiskOffersMapper implements ITxListRiskOffersMapper {

    private static final Log LOG = LogFactory.getLog(TxListRiskOffersMapper.class);

    private final Translator translator;
    private final BooleanToStringConverter booleanToStringConverter;

    public TxListRiskOffersMapper(Translator translator) {
        this.translator = translator;
        this.booleanToStringConverter = new BooleanToStringConverter("1", "0");
    }

    @Override
    public FormatoHYMR405 mapIn(final InputListRiskOffers dtoIn) {
        LOG.info("... called method TxListRiskOffersMapper.mapIn ...");
        FormatoHYMR405 formatoHYMR405 = new FormatoHYMR405();
        formatoHYMR405.setCodcent(dtoIn.getCustomerId());
        return formatoHYMR405;
    }

    @Override
    public List<RiskOffer> mapOut(final FormatoHYMR403 formatOutput,
                                  final List<RiskOffer> dtoOut) {
        LOG.info("... called method TxListRiskOffersMapper.mapOut ...");
        RiskOffer riskOffer = new RiskOffer();
        riskOffer.setId(formatOutput.getId());
        riskOffer.setOfferType(translator.translateBackendEnumValueStrictly("risks.riskOffers.offerType", formatOutput.getTipofer()));
        riskOffer.setRiskSegment(translator.translateBackendEnumValueStrictly("risks.riskOffers.riskSegment", formatOutput.getRiesseg()));
        riskOffer.setPeriod(formatToPeriod(formatOutput.getInivige(), formatOutput.getFinvige()));
        riskOffer.setStatus(translator.translateBackendEnumValueStrictly("risks.riskOffers.status", formatOutput.getEstado()));
        riskOffer.setRiskOfferSegment(formatOutput.getSegcam());

        dtoOut.add(riskOffer);
        return dtoOut;
    }

    private Period formatToPeriod(final String inivige, final String finvige) {
        if (inivige == null && finvige == null) {
            return null;
        }
        Period period = new Period();
        period.setStartDate(BusinessServiceUtil.toDate(inivige));
        period.setEndDate(BusinessServiceUtil.toDate(finvige));
        return period;
    }

    @Override
    public List<RiskOffer> mapOut2(final FormatoHYMR404 formatOutput,
                                   final List<RiskOffer> dtoOut) {
        LOG.info("... called method TxListRiskOffersMapper.mapOut2 ...");
        Product product = new Product();
        product.setId(formatOutput.getMultbin());
        product.setName(formatOutput.getNomubin());
        product.setIsMandatory(booleanToStringConverter.convertFrom(String.valueOf(formatOutput.getPrincip()), null));
        product.setFinancialProductType(formatToFinancialProductType(formatOutput.getTipprof(), formatOutput.getDestpro()));
        product.setProductAmount(formatToProductAmount(formatOutput.getMontprd(), formatOutput.getDivisa()));
        product.setProductTerm(formatToProductTerm(formatOutput.getTipplaz(), formatOutput.getVaplazo()));
        product.setStatus(translator.translateBackendEnumValueStrictly("risks.riskOffers.products.status", formatOutput.getEstprod()));
        product.setContract(formatToContract(formatOutput.getContrat()));
        product.setRates(formatToRates(formatOutput.getTea()));

        RiskOffer riskOffer = dtoOut.get(dtoOut.size() - 1);
        if (CollectionUtils.isEmpty(riskOffer.getProducts())) {
            riskOffer.setProducts(new ArrayList<>());
        }
        riskOffer.getProducts().add(product);
        return dtoOut;
    }

    private FinancialProductType formatToFinancialProductType(final String tipprof, final String destpro) {
        if (tipprof == null && destpro == null) {
            return null;
        }
        FinancialProductType financialProductType = new FinancialProductType();
        financialProductType.setId(tipprof);
        financialProductType.setName(destpro);
        return financialProductType;
    }

    private List<ProductAmount> formatToProductAmount(final BigDecimal montprd, final String divisa) {
        ProductAmount productAmount = new ProductAmount();
        productAmount.setAmount(montprd);
        productAmount.setCurrency(divisa);
        return Collections.singletonList(productAmount);
    }

    private ProductTerm formatToProductTerm(final String tipplaz, final Integer vaplazo) {
        ProductTerm productTerm = new ProductTerm();
        productTerm.setFrequency(translator.translateBackendEnumValueStrictly("risks.riskOffers.products.productTerm.frequency", tipplaz));
        productTerm.setValue(vaplazo);
        return productTerm;
    }

    private Contract formatToContract(final String contrat) {
        if (contrat == null) {
            return null;
        }
        Contract contract = new Contract();
        contract.setId(contrat);
        return contract;
    }

    private Rates formatToRates(final BigDecimal tea) {
        PercentageMode itemizeRateUnit = new PercentageMode();
        itemizeRateUnit.setPercentage(tea);

        ItemizeRate itemizeRate = new ItemizeRate();
        itemizeRate.setRateType("TEA");
        itemizeRate.setItemizeRateUnit(itemizeRateUnit);

        Rates rates = new Rates();
        rates.setItemizeRates(Collections.singletonList(itemizeRate));
        return rates;
    }
}
