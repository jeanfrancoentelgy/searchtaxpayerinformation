package com.bbva.pzic.risks.dao.model.rif3;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>RIMRF31</code> de la transacci&oacute;n <code>RIF3</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "RIMRF31")
@RooJavaBean
@RooSerializable
public class FormatoRIMRF31 {

	/**
	 * <p>Campo <code>TIPDOC</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "TIPDOC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipdoc;
	
	/**
	 * <p>Campo <code>NUMDOC</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "NUMDOC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 11, longitudMaxima = 11)
	private String numdoc;
	
}