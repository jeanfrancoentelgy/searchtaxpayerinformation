package com.bbva.pzic.risks.dao.model.karct003_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>data</code>, utilizado por la clase <code>RespuestaTransaccionKarct003_1</code></p>
 * 
 * @see RespuestaTransaccionKarct003_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Data {
	
	/**
	 * <p>Campo <code>dataOut</code>, &iacute;ndice: <code>1</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 1, nombre = "dataOut", tipo = TipoCampo.DTO)
	private Dataout dataout;
	
}