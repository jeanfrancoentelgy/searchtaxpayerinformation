package com.bbva.pzic.risks.dao.rest;

import com.bbva.pzic.risks.dao.model.blacklisted.ModelProfileRequest;
import com.bbva.pzic.risks.dao.model.profille.ModelCustomerRiskProfile;
import com.bbva.pzic.risks.dao.rest.mapper.IRestModifyCustomerRiskProfileMapper;
import com.bbva.pzic.risks.util.connection.rest.RestPatchConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class RestModifyCustomerRiskProfile extends RestPatchConnection<ModelCustomerRiskProfile, Object> {

    private static final String URL_PROPERTY = "servicing.url.risks.modifyCustomerRiskProfile";
    private static final String USE_PROXY_PROPERTY = "servicing.proxy.risks.modifyCustomerRiskProfile";

    @Autowired
    private IRestModifyCustomerRiskProfileMapper mapper;

    @PostConstruct
    public void init() {
        useProxy = configurationManager.getBooleanProperty(USE_PROXY_PROPERTY, false);
    }

    public void invoke(final ModelProfileRequest request) {
        connect(URL_PROPERTY, mapper.mapIn(request), mapper.mapInPayload(request));
    }

    @Override
    protected void evaluateResponse(Object response, int statusCode) {
        // Nothing to do
    }
}
