package com.bbva.pzic.risks.dao.model.financialinformation;

public class ModelSearchRisksFinancialInformationResponseBody {

    private ModelFinancialInformation data;

    public ModelFinancialInformation getData() {
        return data;
    }

    public void setData(ModelFinancialInformation data) {
        this.data = data;
    }
}
