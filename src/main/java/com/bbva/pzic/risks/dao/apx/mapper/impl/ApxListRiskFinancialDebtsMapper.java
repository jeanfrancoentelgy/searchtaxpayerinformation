package com.bbva.pzic.risks.dao.apx.mapper.impl;

import com.bbva.pzic.risks.business.dto.InputListRiskFinancialDebts;
import com.bbva.pzic.risks.dao.apx.mapper.IApxListRiskFinancialDebtsMapper;
import com.bbva.pzic.risks.dao.model.karct003_1.*;
import com.bbva.pzic.risks.facade.v1.dto.Amount;
import com.bbva.pzic.risks.facade.v1.dto.FinancialDebt;
import com.bbva.pzic.risks.util.mappers.Mapper;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 19/05/2020.
 *
 * @author Entelgy.
 */
@Mapper
public class ApxListRiskFinancialDebtsMapper implements IApxListRiskFinancialDebtsMapper {

    @Override
    public PeticionTransaccionKarct003_1 mapIn(final InputListRiskFinancialDebts input) {
        PeticionTransaccionKarct003_1 peticion = new PeticionTransaccionKarct003_1();
        peticion.setCustomer(mapInCustomer(input.getCustomerId()));
        return peticion;
    }

    private Customer mapInCustomer(final String customerId) {
        Customer customer = new Customer();
        customer.setId(customerId);
        return customer;
    }

    @Override
    public List<FinancialDebt> mapOut(final RespuestaTransaccionKarct003_1 response) {
        if (response == null || CollectionUtils.isEmpty(response.getData())) {
            return null;
        }

        return response.getData().stream().filter(Objects::nonNull).map(this::mapOutData).collect(Collectors.toList());
    }

    private FinancialDebt mapOutData(final Data data) {
        if (data.getDataout() == null) {
            return null;
        }

        FinancialDebt financialDebt = new FinancialDebt();
        financialDebt.setId(data.getDataout().getId());
        financialDebt.setFinancialDebtType(data.getDataout().getFinancialdebttype());
        financialDebt.setNonBBVADisposedAmount(mapOutNonBBVADisposedAmount(data.getDataout().getNonbbvadisposedamount()));
        financialDebt.setDisposedAmount(mapOutDisposedAmount(data.getDataout().getDisposedamount()));
        financialDebt.setDisposedPercentage(data.getDataout().getDisposedpercentage());
        financialDebt.setReportingDate(data.getDataout().getReportingdate());
        return financialDebt;
    }

    private Amount mapOutDisposedAmount(final Disposedamount disposedamount) {
        if (disposedamount == null) {
            return null;
        }

        return mapOutAmount(disposedamount.getAmount(), disposedamount.getCurrency());
    }

    private Amount mapOutNonBBVADisposedAmount(final Nonbbvadisposedamount nonbbvadisposedamount) {
        if (nonbbvadisposedamount == null) {
            return null;
        }

        return mapOutAmount(nonbbvadisposedamount.getAmount(), nonbbvadisposedamount.getCurrency());
    }

    private Amount mapOutAmount(final BigDecimal amount, final String currency) {
        Amount result = new Amount();
        result.setAmount(amount);
        result.setCurrency(currency);
        return result;
    }
}
