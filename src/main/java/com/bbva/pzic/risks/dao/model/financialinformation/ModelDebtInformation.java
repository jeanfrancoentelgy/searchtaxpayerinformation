package com.bbva.pzic.risks.dao.model.financialinformation;

import java.math.BigDecimal;

public class ModelDebtInformation {

    private String month;

    private String currencyBbva;

    private BigDecimal balanceBbva;

    private String currencySsff;

    private BigDecimal balanceSsff;

    private BigDecimal monthlyFee;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getCurrencyBbva() {
        return currencyBbva;
    }

    public void setCurrencyBbva(String currencyBbva) {
        this.currencyBbva = currencyBbva;
    }

    public BigDecimal getBalanceBbva() {
        return balanceBbva;
    }

    public void setBalanceBbva(BigDecimal balanceBbva) {
        this.balanceBbva = balanceBbva;
    }

    public String getCurrencySsff() {
        return currencySsff;
    }

    public void setCurrencySsff(String currencySsff) {
        this.currencySsff = currencySsff;
    }

    public BigDecimal getBalanceSsff() {
        return balanceSsff;
    }

    public void setBalanceSsff(BigDecimal balanceSsff) {
        this.balanceSsff = balanceSsff;
    }

    public BigDecimal getMonthlyFee() {
        return monthlyFee;
    }

    public void setMonthlyFee(BigDecimal monthlyFee) {
        this.monthlyFee = monthlyFee;
    }
}
