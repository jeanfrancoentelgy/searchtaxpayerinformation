package com.bbva.pzic.risks.dao.rest.mock.stub;

import com.bbva.pzic.risks.dao.model.profille.ModelCustomerRiskProfile;
import com.bbva.pzic.risks.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 18/12/2018.
 *
 * @author Entelgy
 */
public class ResponseGetCustomerRiskProfileMock {

    private static final ResponseGetCustomerRiskProfileMock INSTANCE = new ResponseGetCustomerRiskProfileMock();

    private ObjectMapperHelper objectMapper = ObjectMapperHelper.getInstance();

    private ResponseGetCustomerRiskProfileMock() {
    }

    public static ResponseGetCustomerRiskProfileMock getInstance() {
        return INSTANCE;
    }

    public ModelCustomerRiskProfile buildCustomerRiskProfile() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                        .getResourceAsStream("com/bbva/pzic/risks/dao/rest/mock/response_get_profile.json"),
                ModelCustomerRiskProfile.class);
    }
}
