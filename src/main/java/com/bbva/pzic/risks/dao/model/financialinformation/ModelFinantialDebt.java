package com.bbva.pzic.risks.dao.model.financialinformation;

import java.math.BigDecimal;

public class ModelFinantialDebt {

    private ModelBank bank;

    private String currency;

    private BigDecimal amount;

    private BigDecimal monthlyFee;

    public ModelBank getBank() {
        return bank;
    }

    public void setBank(ModelBank bank) {
        this.bank = bank;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getMonthlyFee() {
        return monthlyFee;
    }

    public void setMonthlyFee(BigDecimal monthlyFee) {
        this.monthlyFee = monthlyFee;
    }
}
