package com.bbva.pzic.risks.dao.model.financialinformation;

public class ModelRelatedProduct {

    private String id;

    private String name;

    private ModelCurrentBalanced currentBalanced;

    private Boolean isLinked;

    private ModelProductType productType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ModelCurrentBalanced getCurrentBalanced() {
        return currentBalanced;
    }

    public void setCurrentBalanced(ModelCurrentBalanced currentBalanced) {
        this.currentBalanced = currentBalanced;
    }

    public Boolean getIsLinked() {
        return isLinked;
    }

    public void setIsLinked(Boolean linked) {
        isLinked = linked;
    }

    public ModelProductType getProductType() {
        return productType;
    }

    public void setProductType(ModelProductType productType) {
        this.productType = productType;
    }
}
