package com.bbva.pzic.risks.dao;

import com.bbva.pzic.risks.business.dto.InputListLettersDelegationsCollaborators;
import com.bbva.pzic.risks.business.dto.InputSearchRiskInternalFilters;
import com.bbva.pzic.risks.business.dto.InputSearchRisksFinancialInformation;
import com.bbva.pzic.risks.business.dto.InputSearchTaxpayerInformation;
import com.bbva.pzic.risks.facade.v0.dto.ListLettersDelegationsCollaborators;
import com.bbva.pzic.risks.facade.v0.dto.SearchFinancialInformation;
import com.bbva.pzic.risks.facade.v0.dto.SearchRiskInternalFilters;
import com.bbva.pzic.risks.facade.v0.dto.SearchTaxpayer;

import java.util.List;

public interface IRisksDAOV0 {

    SearchRiskInternalFilters searchRiskInternalFilters(InputSearchRiskInternalFilters input);

    SearchFinancialInformation searchRisksFinancialInformation(InputSearchRisksFinancialInformation input);

    List<ListLettersDelegationsCollaborators> listLettersDelegationsCollaborators(InputListLettersDelegationsCollaborators input);

    SearchTaxpayer searchTaxpayerInformation(InputSearchTaxpayerInformation input);

}
