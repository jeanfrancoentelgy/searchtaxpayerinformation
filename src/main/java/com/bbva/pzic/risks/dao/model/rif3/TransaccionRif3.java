package com.bbva.pzic.risks.dao.model.rif3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>RIF3</code>
 * 
 * @see PeticionTransaccionRif3
 * @see RespuestaTransaccionRif3
 */
@Component
public class TransaccionRif3 implements InvocadorTransaccion<PeticionTransaccionRif3,RespuestaTransaccionRif3> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionRif3 invocar(PeticionTransaccionRif3 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionRif3.class, RespuestaTransaccionRif3.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionRif3 invocarCache(PeticionTransaccionRif3 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionRif3.class, RespuestaTransaccionRif3.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}