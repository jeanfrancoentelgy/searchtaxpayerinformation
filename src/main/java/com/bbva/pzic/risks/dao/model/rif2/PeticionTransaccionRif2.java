package com.bbva.pzic.risks.dao.model.rif2;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;


/**
 * <p>Transacci&oacute;n <code>RIF2</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionRif2</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionRif2</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.RIF2.D1200915.txt
 * RIF2CONSULTA DE EXCLUSIONES            RI        RI2C00F2     01 RIMRF21             RIF2  NN0000NNNNNN    SSTN     E  SNNSSNNN  NN                2020-06-14XP86102 2020-09-1422.45.37XP90749 2020-06-14-13.57.48.649524XP86102 0001-01-010001-01-01
 * 
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.RIMRF21.D1200915.txt
 * RIMRF21 �LISTADO DE EXCLUSIONES        �F�01�00008�01�00001�CODCENT�CODIGO DE CLIENTE   �A�008�0�R�        �
 * 
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.RIMRF22.D1200915.txt
 * RIMRF22 �LISTADO DE EXCLUSIONES        �X�09�00051�01�00001�IDEXCLU�IDENTIFICADOR EXCLUS�N�007�0�S�        �
 * RIMRF22 �LISTADO DE EXCLUSIONES        �X�09�00051�02�00008�CODBANC�CODIGO DEL BANCO    �A�004�0�S�        �
 * RIMRF22 �LISTADO DE EXCLUSIONES        �X�09�00051�03�00012�NOMBANC�NOMBRE DEL BANCO    �A�012�0�S�        �
 * RIMRF22 �LISTADO DE EXCLUSIONES        �X�09�00051�04�00024�CODEXCL�CODIGO DE LA EXCLUSI�A�003�0�S�        �
 * RIMRF22 �LISTADO DE EXCLUSIONES        �X�09�00051�05�00027�NOMEXCL�NOMBRE DE LA EXCLUSI�A�010�0�S�        �
 * RIMRF22 �LISTADO DE EXCLUSIONES        �X�09�00051�06�00037�CODINEL�COD. INELEGIBLE CLIE�N�003�0�S�        �
 * RIMRF22 �LISTADO DE EXCLUSIONES        �X�09�00051�07�00040�SBCOINE�SUBCODIGO INELEGIBLE�A�001�0�S�        �
 * RIMRF22 �LISTADO DE EXCLUSIONES        �X�09�00051�08�00041�NIVEXCL�NIVEL DE EXCLUSION  �A�001�0�S�        �
 * RIMRF22 �LISTADO DE EXCLUSIONES        �X�09�00051�09�00042�FECEXCL�FECHA EMISION EXCLUS�A�010�0�S�        �
 * 
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.RIF2.D1200915.txt
 * RIF2RIMRF22 RINCRF22RI2C00F21S51                           XP86102 2020-06-14-14.36.00.143618XP86102 2020-06-14-14.36.00.143662
 * 
</pre></code>
 * 
 * @see RespuestaTransaccionRif2
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "RIF2",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionRif2.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoRIMRF21.class})
@RooJavaBean
@RooSerializable
public class PeticionTransaccionRif2 implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}