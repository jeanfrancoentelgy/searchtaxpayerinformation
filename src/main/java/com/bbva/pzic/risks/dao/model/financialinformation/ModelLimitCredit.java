package com.bbva.pzic.risks.dao.model.financialinformation;

import java.math.BigDecimal;
import java.util.Date;

public class ModelLimitCredit {

    private String currency;

    private BigDecimal amount;

    private BigDecimal usedPercentage;

    private String contractNumber;

    private Date dueDate;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getUsedPercentage() {
        return usedPercentage;
    }

    public void setUsedPercentage(BigDecimal usedPercentage) {
        this.usedPercentage = usedPercentage;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }
}
