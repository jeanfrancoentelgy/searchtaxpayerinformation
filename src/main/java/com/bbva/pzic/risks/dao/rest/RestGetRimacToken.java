package com.bbva.pzic.risks.dao.rest;

import com.bbva.jee.arq.spring.core.rest.FilePart;
import com.bbva.pzic.risks.dao.model.OAuthCredential;
import com.bbva.pzic.risks.util.NotAplly;
import com.bbva.pzic.risks.util.connection.rest.RestPostConnection;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 18/12/2018.
 *
 * @author Entelgy
 */
@Component
public class RestGetRimacToken extends RestPostConnection<NotAplly, OAuthCredential> {

    private static final String URL_PROPERTY = "servicing.backend.rimac.authorization.url";
    private static final String BLACK_LISTED_ENTITIES_USE_PROXY_RISKS = "servicing.proxy.risks.listRiskBlackListedEntities";

    private static final String CLIENT_ID_PROPERTY = "servicing.backend.rimac.authorization.clientId";
    private static final String CLIENT_SECRET_PROPERTY = "servicing.backend.rimac.authorization.clientSecret";
    private static final String SCOPE_PROPERTY = "servicing.backend.rimac.authorization.scope";

    @PostConstruct
    public void init() {
        useProxy = configurationManager.getBooleanProperty(BLACK_LISTED_ENTITIES_USE_PROXY_RISKS, false);
    }

    public OAuthCredential invoke() {
        List<FilePart> fileParts = new ArrayList<>();
        fileParts.add(new FilePart("client_credentials".getBytes(), "grant_type", ".", " "));
        fileParts.add(new FilePart(getProperty(CLIENT_ID_PROPERTY).getBytes(), "client_id", ".", " "));
        fileParts.add(new FilePart(getProperty(CLIENT_SECRET_PROPERTY).getBytes(), "client_secret", ".", " "));
        fileParts.add(new FilePart(getProperty(SCOPE_PROPERTY).getBytes(), "scope", ".", " "));
        return connect(URL_PROPERTY, null, fileParts);
    }

    @Override
    protected void evaluateResponse(OAuthCredential response, int statusCode) {
        // Pending implementation
    }

}
