package com.bbva.pzic.risks.dao.model.financialinformation;

public class ModelManager {

    private String code;

    private String codeRegistration;

    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeRegistration() {
        return codeRegistration;
    }

    public void setCodeRegistration(String codeRegistration) {
        this.codeRegistration = codeRegistration;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
