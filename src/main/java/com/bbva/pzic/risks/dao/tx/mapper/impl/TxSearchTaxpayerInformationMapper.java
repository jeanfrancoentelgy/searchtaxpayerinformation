package com.bbva.pzic.risks.dao.tx.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.risks.business.dto.InputSearchTaxpayerInformation;
import com.bbva.pzic.risks.dao.model.rif3.FormatoRIMRF31;
import com.bbva.pzic.risks.dao.model.rif3.FormatoRIMRF32;
import com.bbva.pzic.risks.dao.tx.mapper.ITxSearchTaxpayerInformationMapper;
import com.bbva.pzic.risks.facade.v0.dto.*;
import com.bbva.pzic.risks.util.mappers.Mapper;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created on 21/09/2020.
 *
 * @author Entelgy.
 */
@Mapper
public class TxSearchTaxpayerInformationMapper implements ITxSearchTaxpayerInformationMapper {

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public FormatoRIMRF31 mapIn(final InputSearchTaxpayerInformation inputSearchTaxpayerInformation) {
        if (inputSearchTaxpayerInformation == null) {
            return null;
        }
        FormatoRIMRF31 formato = new FormatoRIMRF31();
        formato.setNumdoc(inputSearchTaxpayerInformation.getDocumentNumber());
        formato.setTipdoc(inputSearchTaxpayerInformation.getDocumentType());
        return formato;
    }

    @Override
    public SearchTaxpayer mapOut(final FormatoRIMRF32 formato) {
        if (formato == null) {
            return null;
        }
        SearchTaxpayer searchTaxpayer = new SearchTaxpayer();
        searchTaxpayer.setFullName(formato.getNomape());
        searchTaxpayer.setClossingDate(mapOutParseDate(formato.getFcierre()));
        searchTaxpayer.setDebtAmount(mapOutDebtAmount(formato.getImpdeu(), formato.getDivisa()));
        searchTaxpayer.setTaxPayerStatus(translator.
                translateBackendEnumValueStrictly("risks.taxpayerInformation.search.taxpayerStatus.id",
                        formato.getEstcon()));
        searchTaxpayer.setTaxPayerCondition(translator.
                translateBackendEnumValueStrictly("risks.taxpayerInformation.search.taxpayerCondition.id",
                        formato.getConcon()));
        searchTaxpayer.setTaxPayerType(formato.getTipcon());
        searchTaxpayer.setFormationDate(mapOutParseDate(formato.getFconst()));
        searchTaxpayer.setActivityStartDate(mapOutParseDate(formato.getFiniact()));
        searchTaxpayer.setEconomicActivity(mapOutEconomicActivity(formato.getCiiu()));
        searchTaxpayer.setAddress(mapOutAddress(formato));
        return searchTaxpayer;
    }

    private Date mapOutParseDate(final String date) {
        if (StringUtils.isEmpty(date)){
            return null;
        }
        try {
            return DateUtils.toDate(date, "YYYY-MM-DD");
        } catch (ParseException e){
            throw new BusinessServiceException("wrongDate", e);
        }
    }

    private Address mapOutAddress(final FormatoRIMRF32 formato) {
        if (formato == null){
            return null;
        }
        Address address = new Address();
        address.setLocation(mapOutLocation(formato));
        return address;
    }

    private Location mapOutLocation(final FormatoRIMRF32 formato) {
        if (formato == null){
            return null;
        }
        Location location = new Location();
        location.setAddressComponents(mapAddressComponents(formato));
        return location;
    }

    private List<AddressComponent> mapAddressComponents(final FormatoRIMRF32 formato) {
        if (formato == null && StringUtils.isEmpty(formato.getDeubig()) && StringUtils.isEmpty(formato.getUbigeo()) &&
                StringUtils.isEmpty(formato.getTipvia()) && StringUtils.isEmpty(formato.getDtipvia()) && StringUtils.isEmpty(formato.getTipzon()) &&
                StringUtils.isEmpty(formato.getDtipzon()) && StringUtils.isEmpty(formato.getNomzona()) && StringUtils.isEmpty(formato.getNomvia())){
            return null;
        }
        List<AddressComponent> result = new ArrayList<>();
        result.add(mapAddressComponent(formato.getDeubig(), formato.getUbigeo(), null));
        result.add(mapAddressComponent(formato.getTipvia(), formato.getDtipvia(), formato.getNomvia()));
        result.add(mapAddressComponent(formato.getTipzon(), formato.getDtipzon(), formato.getNomzona()));
        return result;
    }

    private AddressComponent mapAddressComponent(String componentType, String code, String name) {
        if (StringUtils.isEmpty(componentType) && StringUtils.isEmpty(code)){
            return null;
        }
        AddressComponent addressComponent = new AddressComponent();
        List<String> componentTypes = new ArrayList<>();
        componentTypes.add(componentType);
        addressComponent.setComponentTypes(componentTypes);
        addressComponent.setCode(code);
        addressComponent.setName(name);
        return addressComponent;
    }

    private EconomicActivity mapOutEconomicActivity(final String ciiu) {
        if (StringUtils.isEmpty(ciiu)) {
            return null;
        }
        EconomicActivity economicActivity = new EconomicActivity();
        economicActivity.setId(ciiu);
        return economicActivity;
    }

    private Amount mapOutDebtAmount(final BigDecimal impdeu, final String divisa) {
        if (impdeu == null && StringUtils.isEmpty(divisa)){
            return null;
        }
        Amount amount = new Amount();
        amount.setAmount(impdeu);
        amount.setCurrency(divisa);
        return amount;
    }
}
