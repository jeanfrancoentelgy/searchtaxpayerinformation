package com.bbva.pzic.risks.dao.rest;

import com.bbva.pzic.risks.dao.model.blacklisted.ModelBlackListedEntity;
import com.bbva.pzic.risks.dao.model.blacklisted.ModelClientRequest;
import com.bbva.pzic.risks.dao.rest.mapper.IRestListRiskBlackListedEntitiesMapper;
import com.bbva.pzic.risks.facade.v1.dto.RiskBlackListedEntity;
import com.bbva.pzic.risks.util.connection.rest.RestPostConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.bbva.pzic.risks.facade.RegistryIds.SMC_REGISTRY_ID_OF_LIST_RISK_BLACK_LISTED_ENTITIES;

/**
 * Created on 4/12/2018.
 *
 * @author Entelgy
 */
@Component
public class RestListRiskBlackListedEntities extends RestPostConnection<ModelClientRequest, ModelBlackListedEntity> {

    private static final String BLACK_LISTED_ENTITIES_URL = "servicing.url.risks.listRiskBlackListedEntities";
    private static final String BLACK_LISTED_ENTITIES_USE_PROXY_RISKS = "servicing.proxy.risks.listRiskBlackListedEntities";

    @Autowired
    private IRestListRiskBlackListedEntitiesMapper mapper;

    @PostConstruct
    public void init() {
        useProxy = configurationManager.getBooleanProperty(BLACK_LISTED_ENTITIES_USE_PROXY_RISKS, false);
    }

    public List<RiskBlackListedEntity> invoke(final ModelClientRequest input, HashMap<String, String> headers) {
        return mapper.mapOut(connect(BLACK_LISTED_ENTITIES_URL, input, headers));
    }

    @Override
    protected void evaluateResponse(ModelBlackListedEntity response, int statusCode) {
        evaluateMessagesResponse(Collections.emptyList(), SMC_REGISTRY_ID_OF_LIST_RISK_BLACK_LISTED_ENTITIES, statusCode);
    }
}
