package com.bbva.pzic.risks.dao.model.financialinformation;

import java.util.List;

public class ModelAdditionalInformation {

    private String buro;

    private ModelBranch branch;

    private ModelManager manager;

    private String rating;

    private ModelEconomicGroup economicGroup;

    private Boolean paymentHolder;

    private List<ModelLimitCredit> limitCredit;

    private String entailmentCode;

    private ModelSegment segment;

    public String getBuro() {
        return buro;
    }

    public void setBuro(String buro) {
        this.buro = buro;
    }

    public ModelBranch getBranch() {
        return branch;
    }

    public void setBranch(ModelBranch branch) {
        this.branch = branch;
    }

    public ModelManager getManager() {
        return manager;
    }

    public void setManager(ModelManager manager) {
        this.manager = manager;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public ModelEconomicGroup getEconomicGroup() {
        return economicGroup;
    }

    public void setEconomicGroup(ModelEconomicGroup economicGroup) {
        this.economicGroup = economicGroup;
    }

    public Boolean getPaymentHolder() {
        return paymentHolder;
    }

    public void setPaymentHolder(Boolean paymentHolder) {
        this.paymentHolder = paymentHolder;
    }

    public List<ModelLimitCredit> getLimitCredit() {
        return limitCredit;
    }

    public void setLimitCredit(List<ModelLimitCredit> limitCredit) {
        this.limitCredit = limitCredit;
    }

    public String getEntailmentCode() {
        return entailmentCode;
    }

    public void setEntailmentCode(String entailmentCode) {
        this.entailmentCode = entailmentCode;
    }

    public ModelSegment getSegment() {
        return segment;
    }

    public void setSegment(ModelSegment segment) {
        this.segment = segment;
    }
}
