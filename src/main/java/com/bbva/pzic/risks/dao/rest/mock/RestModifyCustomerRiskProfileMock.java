package com.bbva.pzic.risks.dao.rest.mock;

import com.bbva.pzic.risks.dao.model.profille.ModelCustomerRiskProfile;
import com.bbva.pzic.risks.dao.rest.RestModifyCustomerRiskProfile;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Primary
public class RestModifyCustomerRiskProfileMock extends RestModifyCustomerRiskProfile {

    @Override
    protected Object connect(String urlPropertyValue, Map pathParams, ModelCustomerRiskProfile entityPayload) {
        return null;
    }
}
