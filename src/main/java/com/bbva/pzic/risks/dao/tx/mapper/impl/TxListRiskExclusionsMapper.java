package com.bbva.pzic.risks.dao.tx.mapper.impl;

import com.bbva.pzic.risks.business.dto.InputListRiskExclusions;
import com.bbva.pzic.risks.dao.model.rif2.FormatoRIMRF21;
import com.bbva.pzic.risks.dao.model.rif2.FormatoRIMRF22;
import com.bbva.pzic.risks.dao.tx.mapper.ITxListRiskExclusionsMapper;
import com.bbva.pzic.risks.facade.v1.dto.Bank;
import com.bbva.pzic.risks.facade.v1.dto.Exclusion;
import com.bbva.pzic.risks.facade.v1.dto.ExclusionType;
import com.bbva.pzic.risks.facade.v1.dto.InternalCode;
import com.bbva.pzic.risks.util.mappers.Mapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created on 17/06/2020.
 *
 * @author Entelgy
 */
@Mapper
public class TxListRiskExclusionsMapper implements ITxListRiskExclusionsMapper {

    private static final Log LOG = LogFactory.getLog(TxListRiskExclusionsMapper.class);

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public FormatoRIMRF21 mapIn(final InputListRiskExclusions input) {
        LOG.info("... called method TxListRiskExclusionsMapper.mapIn ...");
        FormatoRIMRF21 formatoRIMRF21 = new FormatoRIMRF21();
        formatoRIMRF21.setCodcent(input.getCustomerId());
        return formatoRIMRF21;
    }

    @Override
    public List<Exclusion> mapOut(final FormatoRIMRF22 formatOut, final List<Exclusion> exclusions) {
        LOG.info("... called method TxListRiskExclusionsMapper.mapOut ...");
        Exclusion exclusion = new Exclusion();
        exclusion.setId(String.valueOf(formatOut.getIdexclu()));
        exclusion.setBank(formatToBank(formatOut.getCodbanc(), formatOut.getNombanc()));
        exclusion.setIssuedDate(formatOut.getFecexcl());
        exclusion.setExclusionType(formatToExclusionType(formatOut.getCodexcl(), formatOut.getNomexcl(), formatOut.getCodinel(), formatOut.getSbcoine()));
        exclusion.setExclusionLevel(translator.translateBackendEnumValueStrictly("risks.exclusions.exclusionLevel", formatOut.getNivexcl()));

        exclusions.add(exclusion);
        return exclusions;
    }

    private Bank formatToBank(final String codbanc, final String nombanc) {
        if (StringUtils.isEmpty(codbanc) && StringUtils.isEmpty(nombanc)) {
            return null;
        }

        Bank bank = new Bank();
        bank.setId(codbanc);
        bank.setName(nombanc);
        return bank;
    }

    private ExclusionType formatToExclusionType(final String codexcl, final String nomexcl, final Integer codinel, final String sbcoine) {
        if (StringUtils.isEmpty(codexcl) && StringUtils.isEmpty(nomexcl) && codinel == 0 && StringUtils.isEmpty(sbcoine)) {
            return null;
        }
        ExclusionType exclusionType = new ExclusionType();
        exclusionType.setId(translator.translateBackendEnumValueStrictly("risks.exclusions.exclusionType.id", codexcl));
        exclusionType.setName(nomexcl);
        exclusionType.setInternalCode(formatToExclusionTypeInternalCode(codinel, sbcoine));
        return exclusionType;
    }

    private InternalCode formatToExclusionTypeInternalCode(final Integer codinel, final String sbcoine) {
        if (codinel == 0 && StringUtils.isEmpty(sbcoine)) {
            return null;
        }

        InternalCode internalCode = new InternalCode();
        internalCode.setId(codinel);
        internalCode.setSubCode(sbcoine);
        return internalCode;
    }
}
