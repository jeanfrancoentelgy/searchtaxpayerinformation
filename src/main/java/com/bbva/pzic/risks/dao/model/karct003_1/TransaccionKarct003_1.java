package com.bbva.pzic.risks.dao.model.karct003_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>KARCT003</code>
 * 
 * @see PeticionTransaccionKarct003_1
 * @see RespuestaTransaccionKarct003_1
 */
@Component
public class TransaccionKarct003_1 implements InvocadorTransaccion<PeticionTransaccionKarct003_1,RespuestaTransaccionKarct003_1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionKarct003_1 invocar(PeticionTransaccionKarct003_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionKarct003_1.class, RespuestaTransaccionKarct003_1.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionKarct003_1 invocarCache(PeticionTransaccionKarct003_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionKarct003_1.class, RespuestaTransaccionKarct003_1.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}