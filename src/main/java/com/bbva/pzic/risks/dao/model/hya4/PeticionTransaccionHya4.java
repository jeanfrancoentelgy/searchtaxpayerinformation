package com.bbva.pzic.risks.dao.model.hya4;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;


/**
 * <p>Transacci&oacute;n <code>HYA4</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionHya4</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionHya4</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.HYA4.D1200601.txt
 * HYA4LISTADO DE OFERTAS                 HY        HY2C00A4     01 HYMR405             HYA4  NN0000CNNNNN    SSTN     E  SNNSSNNN  NN                2020-05-25XP86102 2020-06-0115.50.44XP86102 2020-05-25-10.52.47.673710XP86102 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.HYMR403.D1200601.txt
 * HYMR403 �LISTADO DE OFERTAS            �X�07�00040�01�00001�ID     �IDENTIFICADOR UNICO �A�010�0�S�        �
 * HYMR403 �LISTADO DE OFERTAS            �X�07�00040�02�00011�TIPOFER�TIPO DE OFERTA      �A�005�0�S�        �
 * HYMR403 �LISTADO DE OFERTAS            �X�07�00040�03�00016�RIESSEG�SEGMENTO DE RIESGO  �A�001�0�S�        �
 * HYMR403 �LISTADO DE OFERTAS            �X�07�00040�04�00017�INIVIGE�FECHA INICIO VIGENCI�A�010�0�S�        �
 * HYMR403 �LISTADO DE OFERTAS            �X�07�00040�05�00027�FINVIGE�FECHA FIN VIGENCIA  �A�010�0�S�        �
 * HYMR403 �LISTADO DE OFERTAS            �X�07�00040�06�00037�ESTADO �ESTADO DE OFERTA    �A�002�0�S�        �
 * HYMR403 �LISTADO DE OFERTAS            �X�07�00040�07�00039�SEGCAM �IDEN. SEGMENTO CAMPA�A�002�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.HYMR404.D1200601.txt
 * HYMR404 �CONSULTA DE CAMPA�AS          �X�12�00130�01�00001�PRINCIP�FLAG DE OFERTA      �N�001�0�S�        �
 * HYMR404 �CONSULTA DE CAMPA�AS          �X�12�00130�02�00002�TIPPROF�TIPO DE PRODUCTO    �A�002�0�S�        �
 * HYMR404 �CONSULTA DE CAMPA�AS          �X�12�00130�03�00004�DESTPRO�NOMBRE PRODUCTO     �A�050�0�S�        �
 * HYMR404 �CONSULTA DE CAMPA�AS          �X�12�00130�04�00054�MONTPRD�IMPORTE MAXIMO PRODU�N�015�2�S�        �
 * HYMR404 �CONSULTA DE CAMPA�AS          �X�12�00130�05�00069�DIVISA �DIVISA DEL IMPORTE  �A�003�0�S�        �
 * HYMR404 �CONSULTA DE CAMPA�AS          �X�12�00130�06�00072�TIPPLAZ�FRECUENCIA DE PLAZO �A�001�0�S�        �
 * HYMR404 �CONSULTA DE CAMPA�AS          �X�12�00130�07�00073�VAPLAZO�VALOR FRECUENCIA PLA�N�004�0�S�        �
 * HYMR404 �CONSULTA DE CAMPA�AS          �X�12�00130�08�00077�ESTPROD�ESTADO PRODUCTO     �A�002�0�S�        �
 * HYMR404 �CONSULTA DE CAMPA�AS          �X�12�00130�09�00079�TEA    �TASA EFECTIVA       �N�010�7�S�        �
 * HYMR404 �CONSULTA DE CAMPA�AS          �X�12�00130�10�00089�MULTBIN�IDENTIFICADOR PRODUC�A�002�0�S�        �
 * HYMR404 �CONSULTA DE CAMPA�AS          �X�12�00130�11�00091�NOMUBIN�NOMBRE PRODUCTO     �A�020�0�S�        �
 * HYMR404 �CONSULTA DE CAMPA�AS          �X�12�00130�12�00111�CONTRAT�NUMERO DE CONTRATO  �A�020�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.HYMR405.D1200601.txt
 * HYMR405 �LISTADO DE OFERTAS            �F�01�00008�01�00001�CODCENT�CODIGO DE CLIENTE   �A�008�0�R�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.HYA4.D1200601.txt
 * HYA4HYMR404 HYNCR404HY2C00A41S126                          XP86102 2020-05-26-20.07.20.574785XP86102 2020-05-27-01.34.59.238269
 * HYA4HYMR403 HYNCR403HY2C00A41S40                           XP86102 2020-05-26-20.43.02.275680XP86102 2020-05-27-01.33.43.516767
</pre></code>
 * 
 * @see RespuestaTransaccionHya4
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "HYA4",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionHya4.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoHYMR405.class})
@RooJavaBean
@RooSerializable
public class PeticionTransaccionHya4 implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}