package com.bbva.pzic.risks.dao.model.collaborators;

public class ModelBank {

    private String codigo;

    private ModelBranch oficina;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public ModelBranch getOficina() {
        return oficina;
    }

    public void setOficina(ModelBranch oficina) {
        this.oficina = oficina;
    }
}
