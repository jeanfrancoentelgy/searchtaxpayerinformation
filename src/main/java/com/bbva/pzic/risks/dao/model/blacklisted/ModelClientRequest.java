package com.bbva.pzic.risks.dao.model.blacklisted;

import com.bbva.pzic.risks.business.dto.ValidationGroup;

import javax.validation.constraints.Size;

/**
 * Created on 04/12/2018.
 *
 * @author Entelgy
 */
public class ModelClientRequest {

    @Size(max = 1, groups = {ValidationGroup.ListRiskBlackListedEntities.class})
    private String tipdocumento;

    @Size(max = 20, groups = {ValidationGroup.ListRiskBlackListedEntities.class})
    private String numdocumento;

    public String getTipdocumento() {
        return tipdocumento;
    }

    public void setTipdocumento(String tipdocumento) {
        this.tipdocumento = tipdocumento;
    }

    public String getNumdocumento() {
        return numdocumento;
    }

    public void setNumdocumento(String numdocumento) {
        this.numdocumento = numdocumento;
    }
}
