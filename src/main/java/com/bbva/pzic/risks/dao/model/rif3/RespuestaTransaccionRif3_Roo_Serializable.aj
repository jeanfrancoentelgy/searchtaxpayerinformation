// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.risks.dao.model.rif3;

import com.bbva.pzic.risks.dao.model.rif3.RespuestaTransaccionRif3;
import java.io.Serializable;

privileged aspect RespuestaTransaccionRif3_Roo_Serializable {
    
    declare parents: RespuestaTransaccionRif3 implements Serializable;
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    private static final long RespuestaTransaccionRif3.serialVersionUID = 1L;
    
}
