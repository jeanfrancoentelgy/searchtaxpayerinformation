package com.bbva.pzic.risks.dao.tx.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.risks.business.dto.InputSearchRiskInternalFilters;
import com.bbva.pzic.risks.dao.model.rif1.FormatoRIMRF01;
import com.bbva.pzic.risks.dao.model.rif1.FormatoRIMRF02;
import com.bbva.pzic.risks.dao.tx.mapper.ITxSearchRiskInternalFiltersMapper;
import com.bbva.pzic.risks.facade.v0.dto.Qualification;
import com.bbva.pzic.risks.facade.v0.dto.SearchRiskInternalFilters;
import com.bbva.pzic.risks.util.Errors;
import com.bbva.pzic.risks.util.mappers.Mapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 15/06/2020.
 *
 * @author Entelgy.
 */
@Mapper
public class TxSearchRiskInternalFiltersMapper implements ITxSearchRiskInternalFiltersMapper {

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public FormatoRIMRF01 mapIn(final InputSearchRiskInternalFilters input) {
        FormatoRIMRF01 format = new FormatoRIMRF01();
        if (StringUtils.isNotEmpty(input.getDocumentTypeId())) {
            format.setTipodoc(translator.translateFrontendEnumValueStrictly("risks.riskInternalFilters.search.documentType.id", input.getDocumentTypeId()));
        }
        format.setNumedoc(input.getDocumentNumber());
        format.setCodcent(input.getCustomerId());
        return format;
    }

    @Override
    public SearchRiskInternalFilters mapOut(final FormatoRIMRF02 formato) {
        SearchRiskInternalFilters result = new SearchRiskInternalFilters();
        result.setBureau(formato.getBuro());
        result.setLabel(formato.getEtiries());
        result.setNumberOfFinancialEntities(formato.getNroenti());
        result.setQualifications(mapOutQualifications(formato.getClasban(), formato.getClassff()));
        return result;
    }

    private List<Qualification> mapOutQualifications(final String firstResult, final String secondResult) {
        List<Qualification> result = new ArrayList<>();
        result.add(mapOutQualification("BBVA_QUALIFICATION", firstResult));
        result.add(mapOutQualification("SBS_QUALIFICATION", secondResult));
        return result;
    }

    private Qualification mapOutQualification(String id, final String result) {
        Qualification qualification = new Qualification();
        qualification.setId(id);
        qualification.setResult(result);
        return qualification;
    }
}
