package com.bbva.pzic.risks.dao.tx.mapper;

import com.bbva.pzic.risks.business.dto.InputListRiskExclusions;
import com.bbva.pzic.risks.dao.model.rif2.FormatoRIMRF21;
import com.bbva.pzic.risks.dao.model.rif2.FormatoRIMRF22;
import com.bbva.pzic.risks.facade.v1.dto.Exclusion;

import java.util.List;

/**
 * Created on 17/06/2020.
 *
 * @author Entelgy
 */
public interface ITxListRiskExclusionsMapper {

    FormatoRIMRF21 mapIn(InputListRiskExclusions input);

    List<Exclusion> mapOut(FormatoRIMRF22 formatOut, List<Exclusion> exclusions);
}
