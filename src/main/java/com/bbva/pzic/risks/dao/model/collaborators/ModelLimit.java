package com.bbva.pzic.risks.dao.model.collaborators;

import java.math.BigDecimal;

public class ModelLimit {

    private BigDecimal monto;

    private String moneda;

    public BigDecimal getMonto() {return monto;}

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public String getMoneda() {return moneda;}

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }
}
