package com.bbva.pzic.risks.dao.rest.mock;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.risks.dao.model.blacklisted.ModelBlackListedEntity;
import com.bbva.pzic.risks.dao.model.blacklisted.ModelClientRequest;
import com.bbva.pzic.risks.dao.rest.RestListRiskBlackListedEntities;
import com.bbva.pzic.risks.dao.rest.mock.stub.ResponseRiskBlackListedEntitiesMock;
import com.bbva.pzic.risks.util.Errors;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created on 04/12/2018.
 *
 * @author Entelgy
 */
@Primary
@Component
public class RestListRiskBlackListedEntitiesMock extends RestListRiskBlackListedEntities {

    public static final String ERROR_RESPONSE = "99999995";
    public static final String EMPTY_RESPONSE = "99999994";

    @Override
    public ModelBlackListedEntity connect(String urlPropertyValue, ModelClientRequest input, HashMap<String, String> headers) {

        if (EMPTY_RESPONSE.equals(input.getNumdocumento())) {
            return null;
        }

        ModelBlackListedEntity response;
        try {
            response = ResponseRiskBlackListedEntitiesMock.getInstance()
                    .buildBlackListedEntity();
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }

        if (ERROR_RESPONSE.equals(input.getNumdocumento())) {
            response = null;
        }

        evaluateResponse(response, 200);
        return response;
    }
}
