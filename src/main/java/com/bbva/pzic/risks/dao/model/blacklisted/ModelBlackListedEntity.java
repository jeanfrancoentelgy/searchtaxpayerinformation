package com.bbva.pzic.risks.dao.model.blacklisted;

/**
 * Created on 04/12/2018.
 *
 * @author Entelgy
 */
public class ModelBlackListedEntity {

   private String indrechazo;
   private String tipdocumento;
   private String numdocumento;
   private String fecharegistro;

    public String getIndrechazo() {
        return indrechazo;
    }

    public void setIndrechazo(String indrechazo) {
        this.indrechazo = indrechazo;
    }

    public String getTipdocumento() {
        return tipdocumento;
    }

    public void setTipdocumento(String tipdocumento) {
        this.tipdocumento = tipdocumento;
    }

    public String getNumdocumento() {
        return numdocumento;
    }

    public void setNumdocumento(String numdocumento) {
        this.numdocumento = numdocumento;
    }

    public String getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(String fecharegistro) {
        this.fecharegistro = fecharegistro;
    }
}
