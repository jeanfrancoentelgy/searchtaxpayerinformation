package com.bbva.pzic.risks.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.risks.business.dto.InputSearchRiskInternalFilters;
import com.bbva.pzic.risks.dao.model.rif1.FormatoRIMRF01;
import com.bbva.pzic.risks.dao.model.rif1.FormatoRIMRF02;
import com.bbva.pzic.risks.dao.model.rif1.PeticionTransaccionRif1;
import com.bbva.pzic.risks.dao.model.rif1.RespuestaTransaccionRif1;
import com.bbva.pzic.risks.dao.tx.mapper.ITxSearchRiskInternalFiltersMapper;
import com.bbva.pzic.risks.facade.v0.dto.SearchRiskInternalFilters;
import com.bbva.pzic.routine.commons.utils.host.templates.Tx;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * Created on 12/06/2020.
 *
 * @author Entelgy
 */
@Tx
public class TxSearchRiskInternalFilters extends SingleOutputFormat<InputSearchRiskInternalFilters, FormatoRIMRF01, SearchRiskInternalFilters, FormatoRIMRF02> {

    @Autowired
    private ITxSearchRiskInternalFiltersMapper txSearchRiskInternalFiltersMapper;

    @Autowired
    public TxSearchRiskInternalFilters(@Qualifier("transaccionRif1") InvocadorTransaccion<PeticionTransaccionRif1, RespuestaTransaccionRif1> transaction) {
        super(transaction, PeticionTransaccionRif1::new, SearchRiskInternalFilters::new, FormatoRIMRF02.class);
    }

    @Override
    protected FormatoRIMRF01 mapInput(InputSearchRiskInternalFilters inputSearchRiskInternalFilters) {
        return txSearchRiskInternalFiltersMapper.mapIn(inputSearchRiskInternalFilters);
    }

    @Override
    protected SearchRiskInternalFilters mapFirstOutputFormat(FormatoRIMRF02 formatoRIMRF02, InputSearchRiskInternalFilters inputSearchRiskInternalFilters, SearchRiskInternalFilters searchRiskInternalFilters) {
        return txSearchRiskInternalFiltersMapper.mapOut(formatoRIMRF02);
    }
}
