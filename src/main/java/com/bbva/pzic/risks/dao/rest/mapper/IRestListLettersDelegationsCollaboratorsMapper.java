package com.bbva.pzic.risks.dao.rest.mapper;

import com.bbva.pzic.risks.business.dto.InputListLettersDelegationsCollaborators;
import com.bbva.pzic.risks.dao.model.collaborators.ModelListLettersDelegationsCollaboratorsResponseBody;
import com.bbva.pzic.risks.facade.v0.dto.ListLettersDelegationsCollaborators;

import java.util.HashMap;
import java.util.List;

public interface IRestListLettersDelegationsCollaboratorsMapper {

    HashMap<String, String> mapInQueryParams(InputListLettersDelegationsCollaborators inputListLettersDelegationsCollaborators);

    List<ListLettersDelegationsCollaborators> mapOut(ModelListLettersDelegationsCollaboratorsResponseBody model);
}
