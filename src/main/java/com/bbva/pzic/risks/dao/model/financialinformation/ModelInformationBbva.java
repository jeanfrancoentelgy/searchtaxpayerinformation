package com.bbva.pzic.risks.dao.model.financialinformation;

import java.math.BigDecimal;

public class ModelInformationBbva {

    private String month;

    private String currencyDebtSsff;

    private BigDecimal debtSsff;

    private String currencyDebtBbva;

    private BigDecimal debtBbva;

    private BigDecimal feeDirect;

    private String currencyMarginGross;

    private BigDecimal marginGross;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getCurrencyDebtSsff() {
        return currencyDebtSsff;
    }

    public void setCurrencyDebtSsff(String currencyDebtSsff) {
        this.currencyDebtSsff = currencyDebtSsff;
    }

    public BigDecimal getDebtSsff() {
        return debtSsff;
    }

    public void setDebtSsff(BigDecimal debtSsff) {
        this.debtSsff = debtSsff;
    }

    public String getCurrencyDebtBbva() {
        return currencyDebtBbva;
    }

    public void setCurrencyDebtBbva(String currencyDebtBbva) {
        this.currencyDebtBbva = currencyDebtBbva;
    }

    public BigDecimal getDebtBbva() {
        return debtBbva;
    }

    public void setDebtBbva(BigDecimal debtBbva) {
        this.debtBbva = debtBbva;
    }

    public BigDecimal getFeeDirect() {
        return feeDirect;
    }

    public void setFeeDirect(BigDecimal feeDirect) {
        this.feeDirect = feeDirect;
    }

    public String getCurrencyMarginGross() {
        return currencyMarginGross;
    }

    public void setCurrencyMarginGross(String currencyMarginGross) {
        this.currencyMarginGross = currencyMarginGross;
    }

    public BigDecimal getMarginGross() {
        return marginGross;
    }

    public void setMarginGross(BigDecimal marginGross) {
        this.marginGross = marginGross;
    }
}
