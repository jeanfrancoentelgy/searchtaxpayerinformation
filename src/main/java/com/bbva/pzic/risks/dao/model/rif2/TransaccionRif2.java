package com.bbva.pzic.risks.dao.model.rif2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>RIF2</code>
 * 
 * @see PeticionTransaccionRif2
 * @see RespuestaTransaccionRif2
 */
@Component
public class TransaccionRif2 implements InvocadorTransaccion<PeticionTransaccionRif2,RespuestaTransaccionRif2> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionRif2 invocar(PeticionTransaccionRif2 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionRif2.class, RespuestaTransaccionRif2.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionRif2 invocarCache(PeticionTransaccionRif2 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionRif2.class, RespuestaTransaccionRif2.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}