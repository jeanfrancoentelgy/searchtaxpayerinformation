package com.bbva.pzic.risks.util.connection.rest;

import com.bbva.jee.arq.spring.core.rest.RestConnectorResponse;
import com.bbva.pzic.risks.util.connection.BasicRestConnectionProcessor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created on 20/12/2018.
 *
 * @author Entelgy
 */
public abstract class RestPatchConnection<P, S> extends BasicRestConnectionProcessor {

    private static final Log LOG = LogFactory.getLog(RestPatchConnection.class);

    protected S connect(final String urlPropertyValue, final Map<String, String> pathParams, final P entityPayload) {
        return connect(urlPropertyValue, pathParams, null, null, entityPayload);
    }

    protected S connect(final String urlPropertyValue, final Map<String, String> pathParams, final HashMap<String, String> queryParams, final Map<String, String> headers, final P entityPayload) {
        String url = getProperty(urlPropertyValue);
        String payload = buildPayload(entityPayload);

        if (pathParams != null) {
            url = replacePathParamToUrl(url, pathParams);
        }

        if (queryParams != null) {
            LOG.info("Request query params: " + Arrays.toString(queryParams.entrySet().toArray()));
        }

        RestConnectorResponse rcr = proxyRestConnector.doPatch(url, queryParams, buildOptionalHeaders(headers), payload, useProxy);

        final S response = buildResponse(rcr, 1);

        evaluateResponse(response, rcr.getStatusCode());

        return response;
    }

    protected abstract void evaluateResponse(S response, int statusCode);
}
