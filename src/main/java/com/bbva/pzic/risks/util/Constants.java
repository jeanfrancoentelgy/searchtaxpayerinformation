package com.bbva.pzic.risks.util;

public final class Constants {

    public static final String AAP_PROPERTY = "aap";
    public static final String CALLING_CHANNEL_PROPERTY = "callingChannel";
    public static final String EMPLOYEE_ID = "employeeId";
    public static final String BRANCH_ID = "branchId";

    private Constants() {
    }
}
