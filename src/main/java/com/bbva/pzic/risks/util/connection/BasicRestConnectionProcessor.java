package com.bbva.pzic.risks.util.connection;

import com.bbva.jee.arq.spring.core.rest.IProxyRestConnector;
import com.bbva.jee.arq.spring.core.rest.RestConnectorType;

import javax.annotation.PostConstruct;

/**
 * Created on 05/03/2019.
 *
 * @author Entelgy
 */
public abstract class BasicRestConnectionProcessor extends RestConnectionProcessor {

    @PostConstruct
    private void init() {
        String backend = getProperty(BACKEND_ID_PROPERTY);
        proxyRestConnector = (IProxyRestConnector) restConnectorFactory.getRestConnector(RestConnectorType.BASIC, backend);
    }
}
