package com.bbva.pzic.risks.util.orika.converter.builtin;

import com.bbva.pzic.risks.util.orika.converter.BidirectionalConverter;
import com.bbva.pzic.risks.util.orika.metadata.Type;

/**
 * Created on 31/07/2015.
 *
 * @author Entelgy
 */
public class BooleanToStringConverter extends BidirectionalConverter<Boolean, String> {

    private static String TRUE = "S";
    private static String FALSE = "N";

    public BooleanToStringConverter() {
        super();
    }

    public BooleanToStringConverter(String sTrue, String sFalse) {
        super();
        TRUE = sTrue;
        FALSE = sFalse;
    }

    @Override
    public String convertTo(Boolean source, Type<String> destinationType) {
        return source != null ? (source ? TRUE : FALSE) : null;
    }

    @Override
    public Boolean convertFrom(String source, Type<Boolean> destinationType) {
        return source == null ? null : (TRUE.equalsIgnoreCase(source) ? Boolean.TRUE : Boolean.FALSE);
    }
}
