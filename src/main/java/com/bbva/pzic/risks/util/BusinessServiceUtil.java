package com.bbva.pzic.risks.util;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.routine.commons.utils.DateUtils;

import java.text.ParseException;
import java.util.Date;

/**
 * @author Entelgy
 */
public final class BusinessServiceUtil {

    private BusinessServiceUtil() {
        // Prevent instantiation
    }

    public static Date toDate(final String source) {
        try {
            return DateUtils.toDate(source);
        } catch (ParseException e) {
            throw new BusinessServiceException(Errors.WRONG_DATE, e, source);
        }
    }
}
