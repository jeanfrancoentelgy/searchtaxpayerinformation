package com.bbva.pzic.risks.util.connection;

import com.bbva.jee.arq.spring.core.rest.IProxyRestConnector;
import com.bbva.jee.arq.spring.core.rest.RestConnectorType;

import javax.annotation.PostConstruct;

/**
 * Created on 05/03/2019.
 *
 * @author Entelgy
 */
public abstract class OauthRestConnectionProcessor extends RestConnectionProcessor {

    @PostConstruct
    private void init() {
        String backend = getProperty(BACKEND_ID_PROPERTY);
        String appId = getProperty("servicing.connector.rest.app.rimac.id");
        proxyRestConnector = (IProxyRestConnector) restConnectorFactory.getRestConnector(RestConnectorType.OAUTH, backend, appId);
    }
}
