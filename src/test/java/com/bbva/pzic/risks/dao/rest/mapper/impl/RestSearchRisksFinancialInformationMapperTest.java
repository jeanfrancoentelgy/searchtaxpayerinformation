package com.bbva.pzic.risks.dao.rest.mapper.impl;

import com.bbva.pzic.risks.EntityMock;
import com.bbva.pzic.risks.business.dto.InputSearchRisksFinancialInformation;
import com.bbva.pzic.risks.dao.model.financialinformation.ModelFinancialInformation;
import com.bbva.pzic.risks.dao.model.financialinformation.ModelSearchRisksFinancialInformationResponseBody;
import com.bbva.pzic.risks.facade.v0.dto.SearchFinancialInformation;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.bbva.pzic.risks.EntityMock.ENUM_RISKS_RISK_FINANCIAL_INFORMATION_DOCUMENT_TYPE_ID_DNI;
import static com.bbva.pzic.risks.EntityMock.ENUM_RISKS_RISK_FINANCIAL_INFORMATION_DOCUMENT_TYPE_ID_L;
import static org.junit.Assert.*;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RestSearchRisksFinancialInformationMapperTest {

    @InjectMocks
    private RestSearchRisksFinancialInformationMapper mapper;

    @Mock
    private Translator translator;

    private EntityMock entityMock = EntityMock.getInstance();

    @Before
    public void init() {
        when(translator.translateBackendEnumValueStrictly("risks.riskFinancialInformation.documentType.id",
                ENUM_RISKS_RISK_FINANCIAL_INFORMATION_DOCUMENT_TYPE_ID_L))
                .thenReturn(ENUM_RISKS_RISK_FINANCIAL_INFORMATION_DOCUMENT_TYPE_ID_DNI);
    }


    @Test
    public void mapInQueryParamsFullTest() throws IOException {
        InputSearchRisksFinancialInformation input = entityMock.getInputSearchRisksFinancialInformationMock();

        HashMap<String, String> result = mapper.mapInQueryParams(input);

        assertNotNull(result);
        assertNotNull(result.get("customerId"));
        assertNotNull(result.get("identityDocument.documentNumber"));
        assertNotNull(result.get("identityDocument.documentType.id"));

        assertEquals(input.getCustomer().getCustomerId(), result.get("customerId"));
        assertEquals(input.getIdentityDocument().getDocumentNumber(), result.get("identityDocument.documentNumber"));
        assertEquals(input.getIdentityDocument().getDocumentType().getId(), result.get("identityDocument.documentType.id"));
    }

    @Test
    public void mapInQueryParamsWithoutCustomerIdTest() throws IOException {
        InputSearchRisksFinancialInformation input = entityMock.getInputSearchRisksFinancialInformationMock();
        input.setCustomer(null);

        HashMap<String, String> result = mapper.mapInQueryParams(input);

        assertNotNull(result);
        assertFalse(result.containsKey("customerId"));
        assertNotNull(result.get("identityDocument.documentNumber"));
        assertNotNull(result.get("identityDocument.documentType.id"));

        assertEquals(input.getIdentityDocument().getDocumentNumber(), result.get("identityDocument.documentNumber"));
        assertEquals(input.getIdentityDocument().getDocumentType().getId(), result.get("identityDocument.documentType.id"));
    }

    @Test
    public void mapInQueryParamsWithoutIdentityDocumentTest() throws IOException {
        InputSearchRisksFinancialInformation input = entityMock.getInputSearchRisksFinancialInformationMock();
        input.setIdentityDocument(null);

        HashMap<String, String> result = mapper.mapInQueryParams(input);

        assertNotNull(result);
        assertNotNull(result.get("customerId"));
        assertFalse(result.containsKey("identityDocument.documentNumber"));
        assertFalse(result.containsKey("identityDocument.documentType.id"));

        assertEquals(input.getCustomer().getCustomerId(), result.get("customerId"));
    }

    @Test
    public void mapInHeadersFullTest() throws IOException {
        InputSearchRisksFinancialInformation input = entityMock.getInputSearchRisksFinancialInformationMock();

        Map<String, String> result = mapper.mapInHeader(input);

        assertNotNull(result);
        assertNotNull(result.get("consumerId"));
        assertNotNull(result.get("callingChannel"));

        assertEquals(input.getConsumerId(), result.get("consumerId"));
        assertEquals(input.getCallingChannel(), result.get("callingChannel"));
    }

    @Test
    public void mapOutFullTest() throws IOException {
        ModelSearchRisksFinancialInformationResponseBody input = entityMock.getModelSearchRisksFinancialInformationResponseBodyMock();

        SearchFinancialInformation result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getCustomerId());
        assertNotNull(result.getIdentityDocument());
        assertNotNull(result.getIdentityDocument().getDocumentType());
        assertNotNull(result.getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.getIdentityDocument().getDocumentNumber());
        assertNotNull(result.getContractedProducts());
        assertNotNull(result.getContractedProducts().get(0));
        assertNotNull(result.getContractedProducts().get(0).getId());
        assertNotNull(result.getContractedProducts().get(0).getName());
        assertNotNull(result.getContractedProducts().get(0).getCurrentBalance());
        assertNotNull(result.getContractedProducts().get(0).getCurrentBalance().getAmount());
        assertNotNull(result.getContractedProducts().get(0).getCurrentBalance().getCurrency());
        assertNotNull(result.getContractedProducts().get(0).getIsLinked());
        assertNotNull(result.getContractedProducts().get(0).getProductType());
        assertNotNull(result.getContractedProducts().get(0).getProductType().getId());
        assertNotNull(result.getContractedProducts().get(0).getProductType().getName());
        assertNotNull(result.getDirectDebt());
        assertNotNull(result.getDirectDebt().getMonth());
        assertNotNull(result.getDirectDebt().getFinancialSystemDebt());
        assertNotNull(result.getDirectDebt().getFinancialSystemDebt().getAmount());
        assertNotNull(result.getDirectDebt().getFinancialSystemDebt().getCurrency());
        assertNotNull(result.getDirectDebt().getBbvaDebt());
        assertNotNull(result.getDirectDebt().getBbvaDebt().getAmount());
        assertNotNull(result.getDirectDebt().getBbvaDebt().getCurrency());
        assertNotNull(result.getDirectDebt().getDirectFee());
        assertNotNull(result.getDirectDebt().getGrossMargin());
        assertNotNull(result.getDirectDebt().getGrossMargin().getAmount());
        assertNotNull(result.getDirectDebt().getGrossMargin().getCurrency());
        assertNotNull(result.getMonthlyRegistation());
        assertNotNull(result.getMonthlyRegistation().get(0));
        assertNotNull(result.getMonthlyRegistation().get(0).getMonth());
        assertNotNull(result.getMonthlyRegistation().get(0).getProduct());
        assertNotNull(result.getMonthlyRegistation().get(0).getProduct().getId());
        assertNotNull(result.getMonthlyRegistation().get(0).getProduct().getName());
        assertNotNull(result.getMonthlyRegistation().get(0).getBillingAmount());
        assertNotNull(result.getMonthlyRegistation().get(0).getBillingAmount().getAmount());
        assertNotNull(result.getMonthlyRegistation().get(0).getBillingAmount().getCurrency());
        assertNotNull(result.getMonthlyRegistation().get(0).getAnnualEffectiveRate());
        assertNotNull(result.getMonthlyRegistation().get(0).getSpread());
        assertNotNull(result.getHistoricalSpread());
        assertNotNull(result.getHistoricalSpread().get(0));
        assertNotNull(result.getHistoricalSpread().get(0).getInitialMonth());
        assertNotNull(result.getHistoricalSpread().get(0).getFinalMonth());
        assertNotNull(result.getHistoricalSpread().get(0).getBillingAmount());
        assertNotNull(result.getHistoricalSpread().get(0).getBillingAmount().getAmount());
        assertNotNull(result.getHistoricalSpread().get(0).getBillingAmount().getCurrency());
        assertNotNull(result.getHistoricalSpread().get(0).getAnnualEffectiveRate());
        assertNotNull(result.getHistoricalSpread().get(0).getSpread());
        assertNotNull(result.getTotalDebtInformation());
        assertNotNull(result.getTotalDebtInformation().get(0));
        assertNotNull(result.getTotalDebtInformation().get(0).getMonth());
        assertNotNull(result.getTotalDebtInformation().get(0).getFinancialSystemAmount());
        assertNotNull(result.getTotalDebtInformation().get(0).getFinancialSystemAmount().getAmount());
        assertNotNull(result.getTotalDebtInformation().get(0).getFinancialSystemAmount().getCurrency());
        assertNotNull(result.getTotalDebtInformation().get(0).getBbvaAmount());
        assertNotNull(result.getTotalDebtInformation().get(0).getBbvaAmount().getAmount());
        assertNotNull(result.getTotalDebtInformation().get(0).getBbvaAmount().getCurrency());
        assertNotNull(result.getTotalDebtInformation().get(0).getDirectFee());
        assertNotNull(result.getProductDebtBreakdown());
        assertNotNull(result.getProductDebtBreakdown().get(0));
        assertNotNull(result.getProductDebtBreakdown().get(0).getMonth());
        assertNotNull(result.getProductDebtBreakdown().get(0).getProductType());
        assertNotNull(result.getProductDebtBreakdown().get(0).getProductType().getId());
        assertNotNull(result.getProductDebtBreakdown().get(0).getProductType().getName());
        assertNotNull(result.getProductDebtBreakdown().get(0).getBankDebt());
        assertNotNull(result.getProductDebtBreakdown().get(0).getBankDebt().get(0));
        assertNotNull(result.getProductDebtBreakdown().get(0).getBankDebt().get(0).getBankName());
        assertNotNull(result.getProductDebtBreakdown().get(0).getBankDebt().get(0).getAmount());
        assertNotNull(result.getProductDebtBreakdown().get(0).getBankDebt().get(0).getAmount().getAmount());
        assertNotNull(result.getProductDebtBreakdown().get(0).getBankDebt().get(0).getAmount().getCurrency());
        assertNotNull(result.getProductDebtBreakdown().get(0).getBankDebt().get(0).getDirectFee());
        assertNotNull(result.getProductDebtBreakdown().get(0).getFinancialSystemAmount());
        assertNotNull(result.getProductDebtBreakdown().get(0).getFinancialSystemAmount().getAmount());
        assertNotNull(result.getProductDebtBreakdown().get(0).getFinancialSystemAmount().getCurrency());
        assertNotNull(result.getAdditionalInfo());
        assertNotNull(result.getAdditionalInfo().getBureauCode());
        assertNotNull(result.getAdditionalInfo().getBranch());
        assertNotNull(result.getAdditionalInfo().getBranch().getId());
        assertNotNull(result.getAdditionalInfo().getBranch().getName());
        assertNotNull(result.getAdditionalInfo().getBusinessAgent());
        assertNotNull(result.getAdditionalInfo().getBusinessAgent().getId());
        assertNotNull(result.getAdditionalInfo().getBusinessAgent().getRegistrationCode());
        assertNotNull(result.getAdditionalInfo().getBusinessAgent().getFullName());
        assertNotNull(result.getAdditionalInfo().getRating());
        assertNotNull(result.getAdditionalInfo().getEconomicGroup());
        assertNotNull(result.getAdditionalInfo().getEntailmentCode());
        assertNotNull(result.getAdditionalInfo().getIsPaymentHolder());
        assertNotNull(result.getAdditionalInfo().getCreditLimit());
        assertNotNull(result.getAdditionalInfo().getCreditLimit().get(0));
        assertNotNull(result.getAdditionalInfo().getCreditLimit().get(0).getAmount());
        assertNotNull(result.getAdditionalInfo().getCreditLimit().get(0).getAmount().getAmount());
        assertNotNull(result.getAdditionalInfo().getCreditLimit().get(0).getAmount().getCurrency());
        assertNotNull(result.getAdditionalInfo().getCreditLimit().get(0).getUsedPercentage());
        assertNotNull(result.getAdditionalInfo().getCreditLimit().get(0).getContractNumber());
        assertNotNull(result.getAdditionalInfo().getCreditLimit().get(0).getDueDate());
        assertNotNull(result.getAdditionalInfo().getSegment());
        assertNotNull(result.getAdditionalInfo().getSegment().getId());
        assertNotNull(result.getAdditionalInfo().getSegment().getName());

        assertEquals(input.getData().getCustomerId(), result.getCustomerId());
        assertEquals(ENUM_RISKS_RISK_FINANCIAL_INFORMATION_DOCUMENT_TYPE_ID_DNI, result.getIdentityDocument().getDocumentType().getId());
        assertEquals(input.getData().getIdentityDocument().getDocumentNumber(), result.getIdentityDocument().getDocumentNumber());
        assertEquals(input.getData().getRelatedProducts().get(0).getId(), result.getContractedProducts().get(0).getId());
        assertEquals(input.getData().getRelatedProducts().get(0).getName(), result.getContractedProducts().get(0).getName());
        assertEquals(input.getData().getRelatedProducts().get(0).getCurrentBalanced().getAmount(), result.getContractedProducts().get(0).getCurrentBalance().getAmount());
        assertEquals(input.getData().getRelatedProducts().get(0).getCurrentBalanced().getCurrency(), result.getContractedProducts().get(0).getCurrentBalance().getCurrency());
        assertEquals(input.getData().getRelatedProducts().get(0).getIsLinked(), result.getContractedProducts().get(0).getIsLinked());
        assertEquals(input.getData().getRelatedProducts().get(0).getProductType().getId(), result.getContractedProducts().get(0).getProductType().getId());
        assertEquals(input.getData().getRelatedProducts().get(0).getProductType().getName(), result.getContractedProducts().get(0).getProductType().getName());
        assertEquals(input.getData().getInformationBbva().getMonth(), result.getDirectDebt().getMonth());
        assertEquals(input.getData().getInformationBbva().getDebtSsff(), result.getDirectDebt().getFinancialSystemDebt().getAmount());
        assertEquals(input.getData().getInformationBbva().getCurrencyDebtSsff(), result.getDirectDebt().getFinancialSystemDebt().getCurrency());
        assertEquals(input.getData().getInformationBbva().getDebtBbva(), result.getDirectDebt().getBbvaDebt().getAmount());
        assertEquals(input.getData().getInformationBbva().getCurrencyDebtBbva(), result.getDirectDebt().getBbvaDebt().getCurrency());
        assertEquals(input.getData().getInformationBbva().getFeeDirect(), result.getDirectDebt().getDirectFee());
        assertEquals(input.getData().getInformationBbva().getMarginGross(), result.getDirectDebt().getGrossMargin().getAmount());
        assertEquals(input.getData().getInformationBbva().getCurrencyMarginGross(), result.getDirectDebt().getGrossMargin().getCurrency());
        assertEquals(input.getData().getMonthlyRegistation().get(0).getMonth(), result.getMonthlyRegistation().get(0).getMonth());
        assertEquals(input.getData().getMonthlyRegistation().get(0).getProduct().getId(), result.getMonthlyRegistation().get(0).getProduct().getId());
        assertEquals(input.getData().getMonthlyRegistation().get(0).getProduct().getName(), result.getMonthlyRegistation().get(0).getProduct().getName());
        assertEquals(input.getData().getMonthlyRegistation().get(0).getAmount(), result.getMonthlyRegistation().get(0).getBillingAmount().getAmount());
        assertEquals(input.getData().getMonthlyRegistation().get(0).getCurrency(), result.getMonthlyRegistation().get(0).getBillingAmount().getCurrency());
        assertEquals(input.getData().getMonthlyRegistation().get(0).getTea(), result.getMonthlyRegistation().get(0).getAnnualEffectiveRate());
        assertEquals(input.getData().getMonthlyRegistation().get(0).getSpread(), result.getMonthlyRegistation().get(0).getSpread());
        assertEquals(input.getData().getSpreadHistory().get(0).getInitialMonth(), result.getHistoricalSpread().get(0).getInitialMonth());
        assertEquals(input.getData().getSpreadHistory().get(0).getFinalMonth(), result.getHistoricalSpread().get(0).getFinalMonth());
        assertEquals(input.getData().getSpreadHistory().get(0).getAmount(), result.getHistoricalSpread().get(0).getBillingAmount().getAmount());
        assertEquals(input.getData().getSpreadHistory().get(0).getCurrency(), result.getHistoricalSpread().get(0).getBillingAmount().getCurrency());
        assertEquals(input.getData().getSpreadHistory().get(0).getTea(), result.getHistoricalSpread().get(0).getAnnualEffectiveRate());
        assertEquals(input.getData().getSpreadHistory().get(0).getPercentageSpread(), result.getHistoricalSpread().get(0).getSpread());
        assertEquals(input.getData().getDebtSsff().get(0).getMonth(), result.getTotalDebtInformation().get(0).getMonth());
        assertEquals(input.getData().getDebtSsff().get(0).getBalanceSsff(), result.getTotalDebtInformation().get(0).getFinancialSystemAmount().getAmount());
        assertEquals(input.getData().getDebtSsff().get(0).getCurrencySsff(), result.getTotalDebtInformation().get(0).getFinancialSystemAmount().getCurrency());
        assertEquals(input.getData().getDebtSsff().get(0).getBalanceBbva(), result.getTotalDebtInformation().get(0).getBbvaAmount().getAmount());
        assertEquals(input.getData().getDebtSsff().get(0).getCurrencyBbva(), result.getTotalDebtInformation().get(0).getBbvaAmount().getCurrency());
        assertEquals(input.getData().getDebtSsff().get(0).getMonthlyFee(), result.getTotalDebtInformation().get(0).getDirectFee());
        assertEquals(input.getData().getDebtProductSssff().get(0).getMonth(), result.getProductDebtBreakdown().get(0).getMonth());
        assertEquals(input.getData().getDebtProductSssff().get(0).getProduct().getId(), result.getProductDebtBreakdown().get(0).getProductType().getId());
        assertEquals(input.getData().getDebtProductSssff().get(0).getProduct().getName(), result.getProductDebtBreakdown().get(0).getProductType().getName());
        assertEquals(input.getData().getDebtProductSssff().get(0).getFinantialDebt().get(0).getBank().getName(), result.getProductDebtBreakdown().get(0).getBankDebt().get(0).getBankName());
        assertEquals(input.getData().getDebtProductSssff().get(0).getFinantialDebt().get(0).getAmount(), result.getProductDebtBreakdown().get(0).getBankDebt().get(0).getAmount().getAmount());
        assertEquals(input.getData().getDebtProductSssff().get(0).getFinantialDebt().get(0).getCurrency(), result.getProductDebtBreakdown().get(0).getBankDebt().get(0).getAmount().getCurrency());
        assertEquals(input.getData().getDebtProductSssff().get(0).getFinantialDebt().get(0).getMonthlyFee(), result.getProductDebtBreakdown().get(0).getBankDebt().get(0).getDirectFee());
        assertEquals(input.getData().getDebtProductSssff().get(0).getDebtSsff(), result.getProductDebtBreakdown().get(0).getFinancialSystemAmount().getAmount());
        assertEquals(input.getData().getDebtProductSssff().get(0).getCurrencyDebtSsff(), result.getProductDebtBreakdown().get(0).getFinancialSystemAmount().getCurrency());
        assertEquals(input.getData().getAdditional().getBuro(), result.getAdditionalInfo().getBureauCode());
        assertEquals(input.getData().getAdditional().getBranch().getCode(), result.getAdditionalInfo().getBranch().getId());
        assertEquals(input.getData().getAdditional().getBranch().getName(), result.getAdditionalInfo().getBranch().getName());
        assertEquals(input.getData().getAdditional().getManager().getCode(), result.getAdditionalInfo().getBusinessAgent().getId());
        assertEquals(input.getData().getAdditional().getManager().getCodeRegistration(), result.getAdditionalInfo().getBusinessAgent().getRegistrationCode());
        assertEquals(input.getData().getAdditional().getManager().getName(), result.getAdditionalInfo().getBusinessAgent().getFullName());
        assertEquals(input.getData().getAdditional().getRating(), result.getAdditionalInfo().getRating());
        assertEquals(input.getData().getAdditional().getEconomicGroup().getName(), result.getAdditionalInfo().getEconomicGroup());
        assertEquals(input.getData().getAdditional().getEntailmentCode(), result.getAdditionalInfo().getEntailmentCode());
        assertEquals(input.getData().getAdditional().getPaymentHolder(), result.getAdditionalInfo().getIsPaymentHolder());
        assertEquals(input.getData().getAdditional().getLimitCredit().get(0).getAmount(), result.getAdditionalInfo().getCreditLimit().get(0).getAmount().getAmount());
        assertEquals(input.getData().getAdditional().getLimitCredit().get(0).getCurrency(), result.getAdditionalInfo().getCreditLimit().get(0).getAmount().getCurrency());
        assertEquals(input.getData().getAdditional().getLimitCredit().get(0).getUsedPercentage(), result.getAdditionalInfo().getCreditLimit().get(0).getUsedPercentage());
        assertEquals(input.getData().getAdditional().getLimitCredit().get(0).getContractNumber(), result.getAdditionalInfo().getCreditLimit().get(0).getContractNumber());
        assertEquals(input.getData().getAdditional().getLimitCredit().get(0).getDueDate(), result.getAdditionalInfo().getCreditLimit().get(0).getDueDate());
        assertEquals(input.getData().getAdditional().getSegment().getId(), result.getAdditionalInfo().getSegment().getId());
        assertEquals(input.getData().getAdditional().getSegment().getName(), result.getAdditionalInfo().getSegment().getName());
    }

    @Test
    public void mapOutWithEmptyOptionalFieldsTest() throws IOException {
        ModelSearchRisksFinancialInformationResponseBody input = entityMock.getModelSearchRisksFinancialInformationResponseBodyMock();
        input.getData().getInformationBbva().setDebtSsff(null);
        input.getData().getInformationBbva().setCurrencyDebtSsff(null);
        input.getData().getInformationBbva().setDebtBbva(null);
        input.getData().getInformationBbva().setCurrencyDebtBbva(null);
        input.getData().getInformationBbva().setMarginGross(null);
        input.getData().getInformationBbva().setCurrencyMarginGross(null);
        input.getData().getMonthlyRegistation().get(0).setAmount(null);
        input.getData().getMonthlyRegistation().get(0).setCurrency(null);
        input.getData().getSpreadHistory().get(0).setAmount(null);
        input.getData().getSpreadHistory().get(0).setCurrency(null);
        input.getData().getDebtSsff().get(0).setBalanceSsff(null);
        input.getData().getDebtSsff().get(0).setCurrencySsff(null);
        input.getData().getDebtSsff().get(0).setBalanceBbva(null);
        input.getData().getDebtSsff().get(0).setCurrencyBbva(null);
        input.getData().getDebtProductSssff().get(0).getFinantialDebt().get(0).setAmount(null);
        input.getData().getDebtProductSssff().get(0).getFinantialDebt().get(0).setCurrency(null);
        input.getData().getDebtProductSssff().get(0).setDebtSsff(null);
        input.getData().getDebtProductSssff().get(0).setCurrencyDebtSsff(null);
        input.getData().getAdditional().getLimitCredit().get(0).setAmount(null);
        input.getData().getAdditional().getLimitCredit().get(0).setCurrency(null);


        SearchFinancialInformation result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getCustomerId());
        assertNotNull(result.getIdentityDocument());
        assertNotNull(result.getIdentityDocument().getDocumentType());
        assertNotNull(result.getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.getIdentityDocument().getDocumentNumber());
        assertNotNull(result.getContractedProducts());
        assertNotNull(result.getContractedProducts().get(0));
        assertNotNull(result.getContractedProducts().get(0).getId());
        assertNotNull(result.getContractedProducts().get(0).getName());
        assertNotNull(result.getContractedProducts().get(0).getCurrentBalance());
        assertNotNull(result.getContractedProducts().get(0).getCurrentBalance().getAmount());
        assertNotNull(result.getContractedProducts().get(0).getCurrentBalance().getCurrency());
        assertNotNull(result.getContractedProducts().get(0).getIsLinked());
        assertNotNull(result.getContractedProducts().get(0).getProductType());
        assertNotNull(result.getContractedProducts().get(0).getProductType().getId());
        assertNotNull(result.getContractedProducts().get(0).getProductType().getName());
        assertNotNull(result.getDirectDebt());
        assertNotNull(result.getDirectDebt().getMonth());
        assertNull(result.getDirectDebt().getFinancialSystemDebt());
        assertNull(result.getDirectDebt().getBbvaDebt());
        assertNotNull(result.getDirectDebt().getDirectFee());
        assertNull(result.getDirectDebt().getGrossMargin());
        assertNotNull(result.getMonthlyRegistation());
        assertNotNull(result.getMonthlyRegistation().get(0));
        assertNotNull(result.getMonthlyRegistation().get(0).getMonth());
        assertNotNull(result.getMonthlyRegistation().get(0).getProduct());
        assertNotNull(result.getMonthlyRegistation().get(0).getProduct().getId());
        assertNotNull(result.getMonthlyRegistation().get(0).getProduct().getName());
        assertNull(result.getMonthlyRegistation().get(0).getBillingAmount());
        assertNotNull(result.getMonthlyRegistation().get(0).getAnnualEffectiveRate());
        assertNotNull(result.getMonthlyRegistation().get(0).getSpread());
        assertNotNull(result.getHistoricalSpread());
        assertNotNull(result.getHistoricalSpread().get(0));
        assertNotNull(result.getHistoricalSpread().get(0).getInitialMonth());
        assertNotNull(result.getHistoricalSpread().get(0).getFinalMonth());
        assertNull(result.getHistoricalSpread().get(0).getBillingAmount());
        assertNotNull(result.getHistoricalSpread().get(0).getAnnualEffectiveRate());
        assertNotNull(result.getHistoricalSpread().get(0).getSpread());
        assertNotNull(result.getTotalDebtInformation());
        assertNotNull(result.getTotalDebtInformation().get(0));
        assertNotNull(result.getTotalDebtInformation().get(0).getMonth());
        assertNull(result.getTotalDebtInformation().get(0).getFinancialSystemAmount());
        assertNull(result.getTotalDebtInformation().get(0).getBbvaAmount());
        assertNotNull(result.getTotalDebtInformation().get(0).getDirectFee());
        assertNotNull(result.getProductDebtBreakdown());
        assertNotNull(result.getProductDebtBreakdown().get(0));
        assertNotNull(result.getProductDebtBreakdown().get(0).getMonth());
        assertNotNull(result.getProductDebtBreakdown().get(0).getProductType());
        assertNotNull(result.getProductDebtBreakdown().get(0).getProductType().getId());
        assertNotNull(result.getProductDebtBreakdown().get(0).getProductType().getName());
        assertNotNull(result.getProductDebtBreakdown().get(0).getBankDebt());
        assertNotNull(result.getProductDebtBreakdown().get(0).getBankDebt().get(0));
        assertNotNull(result.getProductDebtBreakdown().get(0).getBankDebt().get(0).getBankName());
        assertNull(result.getProductDebtBreakdown().get(0).getBankDebt().get(0).getAmount());
        assertNotNull(result.getProductDebtBreakdown().get(0).getBankDebt().get(0).getDirectFee());
        assertNull(result.getProductDebtBreakdown().get(0).getFinancialSystemAmount());
        assertNotNull(result.getAdditionalInfo());
        assertNotNull(result.getAdditionalInfo().getBureauCode());
        assertNotNull(result.getAdditionalInfo().getBranch());
        assertNotNull(result.getAdditionalInfo().getBranch().getId());
        assertNotNull(result.getAdditionalInfo().getBranch().getName());
        assertNotNull(result.getAdditionalInfo().getBusinessAgent());
        assertNotNull(result.getAdditionalInfo().getBusinessAgent().getId());
        assertNotNull(result.getAdditionalInfo().getBusinessAgent().getRegistrationCode());
        assertNotNull(result.getAdditionalInfo().getBusinessAgent().getFullName());
        assertNotNull(result.getAdditionalInfo().getRating());
        assertNotNull(result.getAdditionalInfo().getEconomicGroup());
        assertNotNull(result.getAdditionalInfo().getEntailmentCode());
        assertNotNull(result.getAdditionalInfo().getIsPaymentHolder());
        assertNotNull(result.getAdditionalInfo().getCreditLimit());
        assertNotNull(result.getAdditionalInfo().getCreditLimit().get(0));
        assertNull(result.getAdditionalInfo().getCreditLimit().get(0).getAmount());
        assertNotNull(result.getAdditionalInfo().getCreditLimit().get(0).getUsedPercentage());
        assertNotNull(result.getAdditionalInfo().getCreditLimit().get(0).getContractNumber());
        assertNotNull(result.getAdditionalInfo().getCreditLimit().get(0).getDueDate());
        assertNotNull(result.getAdditionalInfo().getSegment());
        assertNotNull(result.getAdditionalInfo().getSegment().getId());
        assertNotNull(result.getAdditionalInfo().getSegment().getName());

        assertEquals(input.getData().getCustomerId(), result.getCustomerId());
        assertEquals(ENUM_RISKS_RISK_FINANCIAL_INFORMATION_DOCUMENT_TYPE_ID_DNI, result.getIdentityDocument().getDocumentType().getId());
        assertEquals(input.getData().getIdentityDocument().getDocumentNumber(), result.getIdentityDocument().getDocumentNumber());
        assertEquals(input.getData().getRelatedProducts().get(0).getId(), result.getContractedProducts().get(0).getId());
        assertEquals(input.getData().getRelatedProducts().get(0).getName(), result.getContractedProducts().get(0).getName());
        assertEquals(input.getData().getRelatedProducts().get(0).getCurrentBalanced().getAmount(), result.getContractedProducts().get(0).getCurrentBalance().getAmount());
        assertEquals(input.getData().getRelatedProducts().get(0).getCurrentBalanced().getCurrency(), result.getContractedProducts().get(0).getCurrentBalance().getCurrency());
        assertEquals(input.getData().getRelatedProducts().get(0).getIsLinked(), result.getContractedProducts().get(0).getIsLinked());
        assertEquals(input.getData().getRelatedProducts().get(0).getProductType().getId(), result.getContractedProducts().get(0).getProductType().getId());
        assertEquals(input.getData().getRelatedProducts().get(0).getProductType().getName(), result.getContractedProducts().get(0).getProductType().getName());
        assertEquals(input.getData().getInformationBbva().getMonth(), result.getDirectDebt().getMonth());
        assertEquals(input.getData().getInformationBbva().getFeeDirect(), result.getDirectDebt().getDirectFee());
        assertEquals(input.getData().getMonthlyRegistation().get(0).getMonth(), result.getMonthlyRegistation().get(0).getMonth());
        assertEquals(input.getData().getMonthlyRegistation().get(0).getProduct().getId(), result.getMonthlyRegistation().get(0).getProduct().getId());
        assertEquals(input.getData().getMonthlyRegistation().get(0).getProduct().getName(), result.getMonthlyRegistation().get(0).getProduct().getName());
        assertEquals(input.getData().getMonthlyRegistation().get(0).getTea(), result.getMonthlyRegistation().get(0).getAnnualEffectiveRate());
        assertEquals(input.getData().getMonthlyRegistation().get(0).getSpread(), result.getMonthlyRegistation().get(0).getSpread());
        assertEquals(input.getData().getSpreadHistory().get(0).getInitialMonth(), result.getHistoricalSpread().get(0).getInitialMonth());
        assertEquals(input.getData().getSpreadHistory().get(0).getFinalMonth(), result.getHistoricalSpread().get(0).getFinalMonth());
        assertEquals(input.getData().getSpreadHistory().get(0).getTea(), result.getHistoricalSpread().get(0).getAnnualEffectiveRate());
        assertEquals(input.getData().getSpreadHistory().get(0).getPercentageSpread(), result.getHistoricalSpread().get(0).getSpread());
        assertEquals(input.getData().getDebtSsff().get(0).getMonth(), result.getTotalDebtInformation().get(0).getMonth());
        assertEquals(input.getData().getDebtSsff().get(0).getMonthlyFee(), result.getTotalDebtInformation().get(0).getDirectFee());
        assertEquals(input.getData().getDebtProductSssff().get(0).getMonth(), result.getProductDebtBreakdown().get(0).getMonth());
        assertEquals(input.getData().getDebtProductSssff().get(0).getProduct().getId(), result.getProductDebtBreakdown().get(0).getProductType().getId());
        assertEquals(input.getData().getDebtProductSssff().get(0).getProduct().getName(), result.getProductDebtBreakdown().get(0).getProductType().getName());
        assertEquals(input.getData().getDebtProductSssff().get(0).getFinantialDebt().get(0).getBank().getName(), result.getProductDebtBreakdown().get(0).getBankDebt().get(0).getBankName());
        assertEquals(input.getData().getDebtProductSssff().get(0).getFinantialDebt().get(0).getMonthlyFee(), result.getProductDebtBreakdown().get(0).getBankDebt().get(0).getDirectFee());
        assertEquals(input.getData().getAdditional().getBuro(), result.getAdditionalInfo().getBureauCode());
        assertEquals(input.getData().getAdditional().getBranch().getCode(), result.getAdditionalInfo().getBranch().getId());
        assertEquals(input.getData().getAdditional().getBranch().getName(), result.getAdditionalInfo().getBranch().getName());
        assertEquals(input.getData().getAdditional().getManager().getCode(), result.getAdditionalInfo().getBusinessAgent().getId());
        assertEquals(input.getData().getAdditional().getManager().getCodeRegistration(), result.getAdditionalInfo().getBusinessAgent().getRegistrationCode());
        assertEquals(input.getData().getAdditional().getManager().getName(), result.getAdditionalInfo().getBusinessAgent().getFullName());
        assertEquals(input.getData().getAdditional().getRating(), result.getAdditionalInfo().getRating());
        assertEquals(input.getData().getAdditional().getEconomicGroup().getName(), result.getAdditionalInfo().getEconomicGroup());
        assertEquals(input.getData().getAdditional().getEntailmentCode(), result.getAdditionalInfo().getEntailmentCode());
        assertEquals(input.getData().getAdditional().getPaymentHolder(), result.getAdditionalInfo().getIsPaymentHolder());
        assertEquals(input.getData().getAdditional().getLimitCredit().get(0).getUsedPercentage(), result.getAdditionalInfo().getCreditLimit().get(0).getUsedPercentage());
        assertEquals(input.getData().getAdditional().getLimitCredit().get(0).getContractNumber(), result.getAdditionalInfo().getCreditLimit().get(0).getContractNumber());
        assertEquals(input.getData().getAdditional().getLimitCredit().get(0).getDueDate(), result.getAdditionalInfo().getCreditLimit().get(0).getDueDate());
        assertEquals(input.getData().getAdditional().getSegment().getId(), result.getAdditionalInfo().getSegment().getId());
        assertEquals(input.getData().getAdditional().getSegment().getName(), result.getAdditionalInfo().getSegment().getName());
    }

    @Test
    public void mapOutWithoutNonMandatoryParametersTest() {
        ModelSearchRisksFinancialInformationResponseBody input = new ModelSearchRisksFinancialInformationResponseBody();
        input.setData(new ModelFinancialInformation());
        input.getData().setCustomerId("sdf1a3fda352fd3as5");

        SearchFinancialInformation result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getCustomerId());
        assertNull(result.getIdentityDocument());
        assertNull(result.getContractedProducts());
        assertNull(result.getDirectDebt());
        assertNull(result.getMonthlyRegistation());
        assertNull(result.getHistoricalSpread());
        assertNull(result.getTotalDebtInformation());
        assertNull(result.getProductDebtBreakdown());
        assertNull(result.getAdditionalInfo());

        assertEquals(input.getData().getCustomerId(), result.getCustomerId());
    }

    @Test
    public void mapOutEmptyTest() {
        SearchFinancialInformation result = mapper.mapOut(new ModelSearchRisksFinancialInformationResponseBody());

        assertNull(result);
    }
}