package com.bbva.pzic.risks.dao.model.rif3;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test de la transacci&oacute;n <code>RIF3</code>
 *
 * @author Arquitectura Spring BBVA
 */

@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionRif3 {

    @InjectMocks
    private TransaccionRif3 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() {
        PeticionTransaccionRif3 rq = new PeticionTransaccionRif3();
        RespuestaTransaccionRif3 rs = new RespuestaTransaccionRif3();

        when(servicioTransacciones.invocar(PeticionTransaccionRif3.class, RespuestaTransaccionRif3.class, rq)).thenReturn(rs);

        RespuestaTransaccionRif3 result = transaccion.invocar(rq);
        assertEquals(result, rs);
    }
}