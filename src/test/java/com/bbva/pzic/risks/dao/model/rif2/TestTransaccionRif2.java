package com.bbva.pzic.risks.dao.model.rif2;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test de la transacci&oacute;n <code>RIF2</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionRif2 {

    @InjectMocks
    private TransaccionRif2 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() {
        PeticionTransaccionRif2 rq = new PeticionTransaccionRif2();
        RespuestaTransaccionRif2 rs = new RespuestaTransaccionRif2();

        when(servicioTransacciones.invocar(PeticionTransaccionRif2.class, RespuestaTransaccionRif2.class, rq)).thenReturn(rs);

        RespuestaTransaccionRif2 result = transaccion.invocar(rq);
        assertEquals(result, rs);
    }
}