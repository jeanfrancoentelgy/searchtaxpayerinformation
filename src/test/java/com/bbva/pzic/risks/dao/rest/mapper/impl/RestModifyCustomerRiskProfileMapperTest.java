package com.bbva.pzic.risks.dao.rest.mapper.impl;

import com.bbva.pzic.risks.dao.model.blacklisted.ModelProfileRequest;
import com.bbva.pzic.risks.dao.model.profille.ModelCustomerRiskProfile;
import com.bbva.pzic.risks.dao.rest.mapper.IRestModifyCustomerRiskProfileMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

public class RestModifyCustomerRiskProfileMapperTest {

    private IRestModifyCustomerRiskProfileMapper mapper;

    @Before
    public void setUp() {
        mapper = new RestModifyCustomerRiskProfileMapper();
    }

    @Test
    public void mapInEmptyTest() {
        Map<String, String> result = mapper.mapIn(new ModelProfileRequest());
        Assert.assertNotNull(result);
        Assert.assertNull(result.get("profile-id"));
    }

    @Test
    public void mapInTest() {
        ModelProfileRequest input = new ModelProfileRequest();
        input.setProfileId("abc");
        Map<String, String> result = mapper.mapIn(input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.get("profile-id"));
        Assert.assertEquals("abc", result.get("profile-id"));
    }

    @Test
    public void mapInPayloadEmptyTest() {
        ModelCustomerRiskProfile result = mapper.mapInPayload(new ModelProfileRequest());
        Assert.assertNotNull(result);
        Assert.assertNull(result.getRiskProfileType());
    }

    @Test
    public void mapInPayloadTest() {
        ModelProfileRequest input = new ModelProfileRequest();
        input.setRiskProfileType("123");
        ModelCustomerRiskProfile result = mapper.mapInPayload(input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getRiskProfileType());
        Assert.assertEquals("123", result.getRiskProfileType());
    }


}
