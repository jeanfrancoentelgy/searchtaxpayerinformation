package com.bbva.pzic.risks.dao.apx.mapper.impl;

import com.bbva.pzic.risks.EntityMock;
import com.bbva.pzic.risks.business.dto.InputListRiskFinancialDebts;
import com.bbva.pzic.risks.dao.apx.mapper.IApxListRiskFinancialDebtsMapper;
import com.bbva.pzic.risks.dao.model.karct003_1.PeticionTransaccionKarct003_1;
import com.bbva.pzic.risks.dao.model.karct003_1.RespuestaTransaccionKarct003_1;
import com.bbva.pzic.risks.dao.model.karct003_1.mock.Karct003_1Stubs;
import com.bbva.pzic.risks.facade.v1.dto.FinancialDebt;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created on 19/05/2020.
 *
 * @author Entelgy.
 */
public class ApxListRiskFinancialDebtsMapperTest {

    private IApxListRiskFinancialDebtsMapper mapper;

    @Before
    public void setUp() {
        mapper = new ApxListRiskFinancialDebtsMapper();
    }

    @Test
    public void mapInFullTest() {
        InputListRiskFinancialDebts input = EntityMock.getInstance().buildInputListRiskFinancialDebts();
        PeticionTransaccionKarct003_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCustomer());
        assertNotNull(result.getCustomer().getId());

        assertEquals(input.getCustomerId(), result.getCustomer().getId());
    }

    @Test
    public void mapInEmptyTest() {
        PeticionTransaccionKarct003_1 result = mapper.mapIn(new InputListRiskFinancialDebts());

        assertNotNull(result);
        assertNotNull(result.getCustomer());
        assertNull(result.getCustomer().getId());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        RespuestaTransaccionKarct003_1 input = Karct003_1Stubs.getInstance().getFinancialDebts();
        List<FinancialDebt> result = mapper.mapOut(input);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getFinancialDebtType());
        assertNotNull(result.get(0).getNonBBVADisposedAmount());
        assertNotNull(result.get(0).getNonBBVADisposedAmount().getAmount());
        assertNotNull(result.get(0).getNonBBVADisposedAmount().getCurrency());
        assertNotNull(result.get(0).getDisposedAmount());
        assertNotNull(result.get(0).getDisposedAmount().getAmount());
        assertNotNull(result.get(0).getDisposedAmount().getCurrency());
        assertNotNull(result.get(0).getDisposedPercentage());
        assertNotNull(result.get(0).getReportingDate());
        assertNotNull(result.get(1).getId());
        assertNotNull(result.get(1).getFinancialDebtType());
        assertNotNull(result.get(1).getNonBBVADisposedAmount());
        assertNotNull(result.get(1).getNonBBVADisposedAmount().getAmount());
        assertNotNull(result.get(1).getNonBBVADisposedAmount().getCurrency());
        assertNotNull(result.get(1).getDisposedAmount());
        assertNotNull(result.get(1).getDisposedAmount().getAmount());
        assertNotNull(result.get(1).getDisposedAmount().getCurrency());
        assertNotNull(result.get(1).getDisposedPercentage());
        assertNotNull(result.get(1).getReportingDate());

        assertEquals(input.getData().get(0).getDataout().getId(), result.get(0).getId());
        assertEquals(input.getData().get(0).getDataout().getFinancialdebttype(), result.get(0).getFinancialDebtType());
        assertEquals(input.getData().get(0).getDataout().getNonbbvadisposedamount().getAmount(), result.get(0).getNonBBVADisposedAmount().getAmount());
        assertEquals(input.getData().get(0).getDataout().getNonbbvadisposedamount().getCurrency(), result.get(0).getNonBBVADisposedAmount().getCurrency());
        assertEquals(input.getData().get(0).getDataout().getDisposedamount().getAmount(), result.get(0).getDisposedAmount().getAmount());
        assertEquals(input.getData().get(0).getDataout().getDisposedamount().getCurrency(), result.get(0).getDisposedAmount().getCurrency());
        assertEquals(input.getData().get(0).getDataout().getDisposedpercentage(), result.get(0).getDisposedPercentage());
        assertEquals(input.getData().get(0).getDataout().getReportingdate(), result.get(0).getReportingDate());
        assertEquals(input.getData().get(1).getDataout().getId(), result.get(1).getId());
        assertEquals(input.getData().get(1).getDataout().getFinancialdebttype(), result.get(1).getFinancialDebtType());
        assertEquals(input.getData().get(1).getDataout().getNonbbvadisposedamount().getAmount(), result.get(1).getNonBBVADisposedAmount().getAmount());
        assertEquals(input.getData().get(1).getDataout().getNonbbvadisposedamount().getCurrency(), result.get(1).getNonBBVADisposedAmount().getCurrency());
        assertEquals(input.getData().get(1).getDataout().getDisposedamount().getAmount(), result.get(1).getDisposedAmount().getAmount());
        assertEquals(input.getData().get(1).getDataout().getDisposedamount().getCurrency(), result.get(1).getDisposedAmount().getCurrency());
        assertEquals(input.getData().get(1).getDataout().getDisposedpercentage(), result.get(1).getDisposedPercentage());
        assertEquals(input.getData().get(1).getDataout().getReportingdate(), result.get(1).getReportingDate());
    }

    @Test
    public void mapOutNullTest() {
        List<FinancialDebt> result = mapper.mapOut(new RespuestaTransaccionKarct003_1());

        assertNull(result);
    }

    @Test
    public void mapOutEmptyTest() throws IOException {
        RespuestaTransaccionKarct003_1 input = Karct003_1Stubs.getInstance().getFinancialDebts();
        input.getData().get(0).getDataout().setId(null);
        input.getData().get(0).getDataout().setFinancialdebttype(null);
        input.getData().get(0).getDataout().setNonbbvadisposedamount(null);
        input.getData().get(0).getDataout().setDisposedamount(null);
        input.getData().get(0).getDataout().setDisposedpercentage(null);
        input.getData().get(0).getDataout().setReportingdate(null);
        List<FinancialDebt> result = mapper.mapOut(input);

        assertNotNull(result);
        assertNull(result.get(0).getId());
        assertNull(result.get(0).getFinancialDebtType());
        assertNull(result.get(0).getNonBBVADisposedAmount());
        assertNull(result.get(0).getDisposedAmount());
        assertNull(result.get(0).getDisposedPercentage());
        assertNull(result.get(0).getReportingDate());
    }
}