package com.bbva.pzic.risks.dao.tx.mapper.impl;

import com.bbva.pzic.risks.EntityMock;
import com.bbva.pzic.risks.business.dto.InputSearchRiskInternalFilters;
import com.bbva.pzic.risks.dao.model.rif1.FormatoRIMRF01;
import com.bbva.pzic.risks.dao.model.rif1.FormatoRIMRF02;
import com.bbva.pzic.risks.dao.model.rif1.mock.Rif1Stubs;
import com.bbva.pzic.risks.facade.v0.dto.SearchRiskInternalFilters;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created on 16/06/2020.
 *
 * @author Entelgy.
 */
@RunWith(MockitoJUnitRunner.class)
public class TxSearchRiskInternalFiltersMapperTest {

    @InjectMocks
    private TxSearchRiskInternalFiltersMapper mapper;

    @Mock
    private Translator translator;

    @Test
    public void mapInFullTest() throws IOException {
        InputSearchRiskInternalFilters input = EntityMock.getInstance().buildInputSearchRiskInternalFilters();
        Mockito.when(translator.translateFrontendEnumValueStrictly("risks.riskInternalFilters.search.documentType.id", input.getDocumentTypeId())).thenReturn(EntityMock.DOCUMENT_TYPE_DNI_BACKEND);
        FormatoRIMRF01 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCodcent());
        assertNotNull(result.getTipodoc());
        assertNotNull(result.getNumedoc());

        assertEquals(input.getCustomerId(), result.getCodcent());
        assertEquals(EntityMock.DOCUMENT_TYPE_DNI_BACKEND, result.getTipodoc());
        assertEquals(input.getDocumentNumber(), result.getNumedoc());
    }

    @Test
    public void mapInEmptyTest() {
        FormatoRIMRF01 result = mapper.mapIn(new InputSearchRiskInternalFilters());

        assertNotNull(result);
        assertNull(result.getCodcent());
        assertNull(result.getTipodoc());
        assertNull(result.getNumedoc());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        FormatoRIMRF02 input = Rif1Stubs.getInstance().getRespuestaTransaccionRif1();
        SearchRiskInternalFilters result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getBureau());
        assertNotNull(result.getLabel());
        assertNotNull(result.getNumberOfFinancialEntities());
        assertNotNull(result.getQualifications());
        assertEquals(2, result.getQualifications().size());
        assertNotNull(result.getQualifications().get(0).getId());
        assertNotNull(result.getQualifications().get(0).getResult());
        assertNotNull(result.getQualifications().get(1).getId());
        assertNotNull(result.getQualifications().get(1).getResult());

        assertEquals(input.getBuro(), result.getBureau());
        assertEquals(input.getEtiries(), result.getLabel());
        assertEquals(input.getNroenti(), result.getNumberOfFinancialEntities());
        assertEquals("BBVA_QUALIFICATION", result.getQualifications().get(0).getId());
        assertEquals(input.getClasban(), result.getQualifications().get(0).getResult());
        assertEquals("SBS_QUALIFICATION", result.getQualifications().get(1).getId());
        assertEquals(input.getClassff(), result.getQualifications().get(1).getResult());
    }

    @Test
    public void mapOutEmptyTest() {
        SearchRiskInternalFilters result = mapper.mapOut(new FormatoRIMRF02());

        assertNotNull(result);
        assertNull(result.getBureau());
        assertNull(result.getLabel());
        assertNull(result.getNumberOfFinancialEntities());
        assertNotNull(result.getQualifications());
        assertEquals(2, result.getQualifications().size());
        assertNotNull(result.getQualifications().get(0).getId());
        assertNull(result.getQualifications().get(0).getResult());
        assertNotNull(result.getQualifications().get(1).getId());
        assertNull(result.getQualifications().get(1).getResult());

        assertEquals("BBVA_QUALIFICATION", result.getQualifications().get(0).getId());
        assertEquals("SBS_QUALIFICATION", result.getQualifications().get(1).getId());
    }
}