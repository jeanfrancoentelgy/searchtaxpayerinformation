package com.bbva.pzic.risks.dao.tx.mapper.impl;

import com.bbva.pzic.risks.EntityMock;
import com.bbva.pzic.risks.business.dto.InputListRiskOffers;
import com.bbva.pzic.risks.dao.model.hya4.FormatoHYMR403;
import com.bbva.pzic.risks.dao.model.hya4.FormatoHYMR404;
import com.bbva.pzic.risks.dao.model.hya4.FormatoHYMR405;
import com.bbva.pzic.risks.facade.v1.dto.Product;
import com.bbva.pzic.risks.facade.v1.dto.RiskOffer;
import com.bbva.pzic.routine.translator.facade.Translator;
import com.bbva.pzic.utilTest.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 12/06/2020.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxListRiskOffersMapperTest {

    @InjectMocks
    private TxListRiskOffersMapper mapper;
    @Mock
    private Translator translator;

    @Test
    public void mapInTest() {
        InputListRiskOffers input = EntityMock.getInstance().buildInputListRiskOffers();

        FormatoHYMR405 result = mapper.mapIn(input);

        assertNotNull(result.getCodcent());
        assertEquals(input.getCustomerId(), result.getCodcent());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        FormatoHYMR403 format = EntityMock.getInstance().buildFormatoHYMR403();
        List<RiskOffer> riskOffers = new ArrayList<>();

        when(translator.translateBackendEnumValueStrictly("risks.riskOffers.offerType", format.getTipofer()))
                .thenReturn("PRE_ANALYZED");
        when(translator.translateBackendEnumValueStrictly("risks.riskOffers.riskSegment", format.getRiesseg()))
                .thenReturn("RETAIL_CREDIT");
        when(translator.translateBackendEnumValueStrictly("risks.riskOffers.status", format.getEstado()))
                .thenReturn("DISMISSED");

        riskOffers = mapper.mapOut(format, riskOffers);

        RiskOffer result = riskOffers.get(0);

        assertNotNull(result.getId());
        assertNotNull(result.getOfferType());
        assertNotNull(result.getRiskSegment());
        assertNotNull(result.getPeriod().getStartDate());
        assertNotNull(result.getPeriod().getEndDate());
        assertNotNull(result.getStatus());
        assertNotNull(result.getRiskOfferSegment());

        assertEquals(format.getId(), result.getId());
        assertEquals("PRE_ANALYZED", result.getOfferType());
        assertEquals("RETAIL_CREDIT", result.getRiskSegment());
        assertEquals(format.getInivige(), DateUtils.getFormatDefaultHost(result.getPeriod().getStartDate()));
        assertEquals(format.getFinvige(), DateUtils.getFormatDefaultHost(result.getPeriod().getEndDate()));
        assertEquals("DISMISSED", result.getStatus());
        assertEquals(format.getSegcam(), result.getRiskOfferSegment());
    }

    @Test
    public void mapOutProductsTest() throws IOException {
        List<FormatoHYMR404> formats = EntityMock.getInstance().buildFormatosHYMR404();
        List<RiskOffer> riskOffers = new ArrayList<>();
        riskOffers.add(new RiskOffer());

        // Index 0
        FormatoHYMR404 f = formats.get(0);

        when(translator.translateBackendEnumValueStrictly("risks.riskOffers.products.productTerm.frequency", f.getTipplaz()))
                .thenReturn("MONTHLY");
        when(translator.translateBackendEnumValueStrictly("risks.riskOffers.products.status", f.getEstprod()))
                .thenReturn("IN_PROCESS");

        riskOffers = mapper.mapOut2(f, riskOffers);

        assertEquals(1, riskOffers.size());

        RiskOffer riskOffer = riskOffers.get(0);
        Product result = riskOffer.getProducts().get(0);

        assertEquals(1, riskOffer.getProducts().size());
        assertNotNull(result.getId());
        assertNotNull(result.getName());
        assertNotNull(result.getIsMandatory());
        assertNotNull(result.getFinancialProductType().getId());
        assertNotNull(result.getFinancialProductType().getName());
        assertNotNull(result.getProductAmount().get(0).getAmount());
        assertNotNull(result.getProductAmount().get(0).getCurrency());
        assertNotNull(result.getProductTerm().getFrequency());
        assertNotNull(result.getProductTerm().getValue());
        assertNotNull(result.getStatus());
        assertNotNull(result.getContract().getId());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRateUnit().getPercentage());

        assertEquals(f.getMultbin(), result.getId());
        assertEquals(f.getNomubin(), result.getName());
        assertTrue(result.getIsMandatory());
        assertEquals(f.getTipprof(), result.getFinancialProductType().getId());
        assertEquals(f.getDestpro(), result.getFinancialProductType().getName());
        assertEquals(f.getMontprd(), result.getProductAmount().get(0).getAmount());
        assertEquals(f.getDivisa(), result.getProductAmount().get(0).getCurrency());
        assertEquals("MONTHLY", result.getProductTerm().getFrequency());
        assertEquals(f.getVaplazo(), result.getProductTerm().getValue());
        assertEquals("IN_PROCESS", result.getStatus());
        assertEquals(f.getContrat(), result.getContract().getId());
        assertEquals("TEA", result.getRates().getItemizeRates().get(0).getRateType());
        assertEquals(f.getTea(), result.getRates().getItemizeRates().get(0).getItemizeRateUnit().getPercentage());

        // Index 1
        f = formats.get(1);

        riskOffers = mapper.mapOut2(f, riskOffers);

        assertEquals(1, riskOffers.size());

        riskOffer = riskOffers.get(0);
        result = riskOffer.getProducts().get(1);

        assertEquals(2, riskOffer.getProducts().size());
        assertNull(result.getId());
        assertNull(result.getName());
        assertNotNull(result.getIsMandatory());
        assertNull(result.getFinancialProductType());
        assertNotNull(result.getProductAmount().get(0).getAmount());
        assertNull(result.getProductAmount().get(0).getCurrency());
        assertNull(result.getProductTerm().getFrequency());
        assertNotNull(result.getProductTerm().getValue());
        assertNull(result.getStatus());
        assertNull(result.getContract());
        assertNotNull(result.getRates().getItemizeRates().get(0).getRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRateUnit().getPercentage());

        assertFalse(result.getIsMandatory());
        assertEquals(f.getVaplazo(), result.getProductTerm().getValue());
        assertEquals("TEA", result.getRates().getItemizeRates().get(0).getRateType());
        assertEquals(f.getTea(), result.getRates().getItemizeRates().get(0).getItemizeRateUnit().getPercentage());
    }
}
