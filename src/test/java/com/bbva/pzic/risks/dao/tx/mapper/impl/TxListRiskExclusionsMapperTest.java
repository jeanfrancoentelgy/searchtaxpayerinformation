package com.bbva.pzic.risks.dao.tx.mapper.impl;

import com.bbva.pzic.risks.EntityMock;
import com.bbva.pzic.risks.business.dto.InputListRiskExclusions;
import com.bbva.pzic.risks.dao.model.rif2.FormatoRIMRF21;
import com.bbva.pzic.risks.dao.model.rif2.FormatoRIMRF22;
import com.bbva.pzic.risks.dao.model.rif2.mock.Rif2Stubs;
import com.bbva.pzic.risks.facade.v1.dto.Exclusion;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 17/06/2020.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxListRiskExclusionsMapperTest {

    @InjectMocks
    private TxListRiskExclusionsMapper mapper;
    @Mock
    private Translator translator;

    @Test
    public void mapInFullTest() {
        InputListRiskExclusions input = EntityMock.getInstance().getInputListRiskExclusions();

        FormatoRIMRF21 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCodcent());
        assertEquals(input.getCustomerId(), result.getCodcent());
    }

    @Test
    public void mapInEmptyTest() {
        FormatoRIMRF21 result = mapper.mapIn(new InputListRiskExclusions());
        assertNotNull(result);
        assertNull(result.getCodcent());
    }

    @Test
    public void mapOutTest() throws IOException {
        List<FormatoRIMRF22> formats = Rif2Stubs.getInstance().buildFormatosRIMRF22();
        List<Exclusion> exclusions = new ArrayList<>();

        // Index 0 - full
        FormatoRIMRF22 format = formats.get(0);

        when(translator.translateBackendEnumValueStrictly("risks.exclusions.exclusionType.id", format.getCodexcl()))
                .thenReturn("INELIGIBLE");
        when(translator.translateBackendEnumValueStrictly("risks.exclusions.exclusionLevel", format.getNivexcl()))
                .thenReturn("HIGH");

        exclusions = mapper.mapOut(format, exclusions);

        assertEquals(1, exclusions.size());

        Exclusion result = exclusions.get(0);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getBank().getId());
        assertNotNull(result.getBank().getName());
        assertNotNull(result.getIssuedDate());
        assertNotNull(result.getExclusionType().getId());
        assertNotNull(result.getExclusionType().getName());
        assertNotNull(result.getExclusionType().getInternalCode().getId());
        assertNotNull(result.getExclusionType().getInternalCode().getSubCode());
        assertNotNull(result.getExclusionLevel());

        assertEquals(format.getIdexclu().toString(), result.getId());
        assertEquals(format.getCodbanc(), result.getBank().getId());
        assertEquals(format.getNombanc(), result.getBank().getName());
        assertEquals(format.getFecexcl(), result.getIssuedDate());
        assertEquals("INELIGIBLE", result.getExclusionType().getId());
        assertEquals(format.getNomexcl(), result.getExclusionType().getName());
        assertEquals(format.getCodinel(), result.getExclusionType().getInternalCode().getId());
        assertEquals(format.getSbcoine(), result.getExclusionType().getInternalCode().getSubCode());
        assertEquals("HIGH", result.getExclusionLevel());

        // Index 1 - empty
        format = formats.get(1);

        exclusions = mapper.mapOut(format, exclusions);

        assertEquals(2, exclusions.size());

        result = exclusions.get(1);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNull(result.getBank());
        assertNull(result.getIssuedDate());
        assertNull(result.getExclusionType());
        assertNull(result.getExclusionLevel());

        assertEquals(format.getIdexclu().toString(), result.getId());
    }
}
