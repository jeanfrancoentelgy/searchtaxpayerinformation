package com.bbva.pzic.risks.dao.rest.mapper.impl;

import com.bbva.pzic.risks.dao.model.blacklisted.ModelBlackListedEntity;
import com.bbva.pzic.risks.dao.rest.mock.stub.ResponseRiskBlackListedEntitiesMock;
import com.bbva.pzic.risks.facade.v1.dto.RiskBlackListedEntity;
import com.bbva.pzic.risks.util.mappers.EnumMapper;
import com.bbva.pzic.utilTest.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.List;

import static com.bbva.pzic.risks.EntityMock.DOCUMENT_TYPE_DNI;
import static org.junit.Assert.*;

/**
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class RestListRiskBlackListedEntitiesMapperTest {

    @InjectMocks
    private RestListRiskBlackListedEntitiesMapper mapper;
    @Mock
    private EnumMapper enumMapper;

    @Test
    public void mapOutFullTest() throws IOException {
        ModelBlackListedEntity output = ResponseRiskBlackListedEntitiesMock.getInstance().buildBlackListedEntity();
        Mockito.when(enumMapper.getEnumValue("documentType.id", output.getTipdocumento())).thenReturn(DOCUMENT_TYPE_DNI);
        List<RiskBlackListedEntity> result = mapper.mapOut(output);

        assertEquals(1, result.size());
        assertFalse(result.get(0).getBlocked());
        assertEquals(DOCUMENT_TYPE_DNI, result.get(0).getIdentityDocument().getDocumentType().getId());
        assertEquals(output.getNumdocumento(), result.get(0).getIdentityDocument().getDocumentNumber());
        assertEquals(output.getFecharegistro(), DateUtils.getFormatDefaultHost(result.get(0).getEntryDate()));
    }

    @Test
    public void mapOutNullTest() {
        List<RiskBlackListedEntity> result = mapper.mapOut(null);

        assertNull(result);
    }
}
