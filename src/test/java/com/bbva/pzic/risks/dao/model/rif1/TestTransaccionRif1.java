package com.bbva.pzic.risks.dao.model.rif1;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test de la transacci&oacute;n <code>RIF1</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionRif1 {

    @InjectMocks
    private TransaccionRif1 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() {
        PeticionTransaccionRif1 rq = new PeticionTransaccionRif1();
        RespuestaTransaccionRif1 rs = new RespuestaTransaccionRif1();

        when(servicioTransacciones.invocar(PeticionTransaccionRif1.class, RespuestaTransaccionRif1.class, rq)).thenReturn(rs);

        RespuestaTransaccionRif1 result = transaccion.invocar(rq);
        assertEquals(result, rs);
    }
}