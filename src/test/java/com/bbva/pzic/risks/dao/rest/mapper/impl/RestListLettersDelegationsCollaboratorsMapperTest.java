package com.bbva.pzic.risks.dao.rest.mapper.impl;

import com.bbva.pzic.risks.EntityMock;
import com.bbva.pzic.risks.business.dto.InputListLettersDelegationsCollaborators;
import com.bbva.pzic.risks.dao.model.collaborators.ModelListLettersDelegationsCollaboratorsResponseBody;
import com.bbva.pzic.risks.facade.v0.dto.ListLettersDelegationsCollaborators;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;


public class RestListLettersDelegationsCollaboratorsMapperTest {


    private RestListLettersDelegationsCollaboratorsMapper restListLettersDelegationsCollaboratorsMapper = new RestListLettersDelegationsCollaboratorsMapper();

    private EntityMock mock = EntityMock.getInstance();

    @Test
    public void mapInQueryFullTest() {
        InputListLettersDelegationsCollaborators input = mock.getInputListLettersDelegationsCollaboratorsMock();
        HashMap<String, String> result = restListLettersDelegationsCollaboratorsMapper.mapInQueryParams(input);

        assertNotNull(result);
        assertNotNull(result.get("codigoRegistro"));
        assertNotNull(result.get("codigoOficina"));

        assertEquals(input.getCodigoRegistro(),result.get("codigoRegistro"));
        assertEquals(input.getCodigoOficina(), result.get("codigoOficina"));
    }

    @Test
    public void mapInWithOutEmployeeIdTest() {
        InputListLettersDelegationsCollaborators input = mock.getInputListLettersDelegationsCollaboratorsMock();
        input.setCodigoRegistro(null);
        HashMap<String, String> result = restListLettersDelegationsCollaboratorsMapper.mapInQueryParams(input);

        assertNotNull(result);
        assertNotNull(result.get("codigoOficina"));
        assertNull(result.get("codigoRegistro"));

        assertEquals(input.getCodigoOficina(), result.get("codigoOficina"));
    }

    @Test
    public void mapInWithOutBranchIdTest() {
        InputListLettersDelegationsCollaborators input = mock.getInputListLettersDelegationsCollaboratorsMock();
        input.setCodigoOficina(null);
        HashMap<String, String> result = restListLettersDelegationsCollaboratorsMapper.mapInQueryParams(input);

        assertNotNull(result);
        assertNotNull(result.get("codigoRegistro"));
        assertNull(result.get("codigoOficina"));

        assertEquals(input.getCodigoRegistro(), result.get("codigoRegistro"));
    }

    @Test
    public void mapOutFullTest() throws IOException {
        ModelListLettersDelegationsCollaboratorsResponseBody responses = mock.getModelListLettersDelegationsCollaboratorsResponseBodyMock();
        List<ListLettersDelegationsCollaborators> result = restListLettersDelegationsCollaboratorsMapper.mapOut(responses);

        assertNotNull(result);
        assertFalse(result.isEmpty());

        assertEquals(1,result.size());
        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getFullName());
        assertNotNull(result.get(0).getRole());
        assertNotNull(result.get(0).getRole().getId());
        assertNotNull(result.get(0).getRole().getName());
        assertNotNull(result.get(0).getBank());
        assertNotNull(result.get(0).getBank().getId());
        assertNotNull(result.get(0).getBank().getBranch());
        assertNotNull(result.get(0).getBank().getBranch().getId());
        assertNotNull(result.get(0).getBank().getBranch().getName());
        assertNotNull(result.get(0).getDelegationId());
        assertNotNull(result.get(0).getEconomicGroupLimits().get(0));
        assertNotNull(result.get(0).getEconomicGroupLimits().get(0).getGroup());
        assertNotNull(result.get(0).getEconomicGroupLimits().get(0).getLimit());
        assertNotNull(result.get(0).getEconomicGroupLimits().get(0).getLimit().getAmount());
        assertNotNull(result.get(0).getEconomicGroupLimits().get(0).getLimit().getCurrency());
        assertNotNull(result.get(0).getEconomicGroupLimits().get(0).getDelegationLevel());
        assertNotNull(result.get(0).getLimitsGaranteed().get(0));
        assertNotNull(result.get(0).getLimitsGaranteed().get(0).getGroup());
        assertNotNull(result.get(0).getLimitsGaranteed().get(0).getLimit());
        assertNotNull(result.get(0).getLimitsGaranteed().get(0).getLimit().getAmount());
        assertNotNull(result.get(0).getLimitsGaranteed().get(0).getLimit().getCurrency());
        assertNotNull(result.get(0).getLimitsGaranteed().get(0).getMaximumTerm());
        assertNotNull(result.get(0).getLimitsGaranteed().get(0).getMaximumTerm().getTermType());
        assertNotNull(result.get(0).getLimitsGaranteed().get(0).getMaximumTerm().getTerm());
        assertNotNull(result.get(0).getProductsLimits().get(0));
        assertNotNull(result.get(0).getProductsLimits().get(0).getProduct());
        assertNotNull(result.get(0).getProductsLimits().get(0).getProduct().getId());
        assertNotNull(result.get(0).getProductsLimits().get(0).getProduct().getName());
        assertNotNull(result.get(0).getProductsLimits().get(0).getProduct().getMaximumTerm());
        assertNotNull(result.get(0).getProductsLimits().get(0).getProduct().getMaximumTerm().getTermType());
        assertNotNull(result.get(0).getProductsLimits().get(0).getProduct().getMaximumTerm().getTerm());
        assertNotNull(result.get(0).getProductsLimits().get(0).getLimits().get(0));
        assertNotNull(result.get(0).getProductsLimits().get(0).getLimits().get(0).getGroup());
        assertNotNull(result.get(0).getProductsLimits().get(0).getLimits().get(0).getRange());
        assertNotNull(result.get(0).getProductsLimits().get(0).getLimits().get(0).getLimit());
        assertNotNull(result.get(0).getProductsLimits().get(0).getLimits().get(0).getLimit().getAmount());
        assertNotNull(result.get(0).getProductsLimits().get(0).getLimits().get(0).getLimit().getCurrency());

        assertEquals(responses.getData().get(0).getColaborador().getRegistro(),result.get(0).getId());
        assertEquals(responses.getData().get(0).getColaborador().getNombres(),result.get(0).getFullName());
        assertEquals(responses.getData().get(0).getColaborador().getPuesto().getCodigo(),result.get(0).getRole().getId());
        assertEquals(responses.getData().get(0).getColaborador().getPuesto().getNombre(),result.get(0).getRole().getName());
        assertEquals(responses.getData().get(0).getColaborador().getBanco().getCodigo(),result.get(0).getBank().getId());
        assertEquals(responses.getData().get(0).getColaborador().getBanco().getOficina().getCodigo(),result.get(0).getBank().getBranch().getId());
        assertEquals(responses.getData().get(0).getColaborador().getBanco().getOficina().getNombre(),result.get(0).getBank().getBranch().getName());
        assertEquals(responses.getData().get(0).getId(),result.get(0).getDelegationId());
        assertEquals(responses.getData().get(0).getColaborador().getLimitesPorGrupoEconomico().get(0).getGrupo(),result.get(0).getEconomicGroupLimits().get(0).getGroup());
        assertEquals(responses.getData().get(0).getColaborador().getLimitesPorGrupoEconomico().get(0).getLimite().getMonto(),result.get(0).getEconomicGroupLimits().get(0).getLimit().getAmount());
        assertEquals(responses.getData().get(0).getColaborador().getLimitesPorGrupoEconomico().get(0).getLimite().getMoneda(),result.get(0).getEconomicGroupLimits().get(0).getLimit().getCurrency());
        assertEquals(responses.getData().get(0).getColaborador().getLimitesPorGrupoEconomico().get(0).getNivelDelegacion(),result.get(0).getEconomicGroupLimits().get(0).getDelegationLevel());
        assertEquals(responses.getData().get(0).getColaborador().getLimitesGarantizados().get(0).getGrupo(),result.get(0).getLimitsGaranteed().get(0).getGroup());
        assertEquals(responses.getData().get(0).getColaborador().getLimitesGarantizados().get(0).getLimite().getMonto(),result.get(0).getLimitsGaranteed().get(0).getLimit().getAmount());
        assertEquals(responses.getData().get(0).getColaborador().getLimitesGarantizados().get(0).getLimite().getMoneda(),result.get(0).getLimitsGaranteed().get(0).getLimit().getCurrency());
        assertEquals(responses.getData().get(0).getColaborador().getLimitesGarantizados().get(0).getPlazoMaximo().getTipoPlazo(),result.get(0).getLimitsGaranteed().get(0).getMaximumTerm().getTermType());
        assertEquals(responses.getData().get(0).getColaborador().getLimitesGarantizados().get(0).getPlazoMaximo().getPlazo(),result.get(0).getLimitsGaranteed().get(0).getMaximumTerm().getTerm());
        assertEquals(responses.getData().get(0).getColaborador().getLimitePorProducto().get(0).getProducto().getCodigo(),result.get(0).getProductsLimits().get(0).getProduct().getId());
        assertEquals(responses.getData().get(0).getColaborador().getLimitePorProducto().get(0).getProducto().getDescripcion(),result.get(0).getProductsLimits().get(0).getProduct().getName());
        assertEquals(responses.getData().get(0).getColaborador().getLimitePorProducto().get(0).getProducto().getPlazoMaximo().getTipoPlazo(),result.get(0).getProductsLimits().get(0).getProduct().getMaximumTerm().getTermType());
        assertEquals(responses.getData().get(0).getColaborador().getLimitePorProducto().get(0).getProducto().getPlazoMaximo().getPlazo(),result.get(0).getProductsLimits().get(0).getProduct().getMaximumTerm().getTerm());
        assertEquals(responses.getData().get(0).getColaborador().getLimitePorProducto().get(0).getLimites().get(0).getGrupo(),result.get(0).getProductsLimits().get(0).getLimits().get(0).getGroup());
        assertEquals(responses.getData().get(0).getColaborador().getLimitePorProducto().get(0).getLimites().get(0).getRango(),result.get(0).getProductsLimits().get(0).getLimits().get(0).getRange());
        assertEquals(responses.getData().get(0).getColaborador().getLimitePorProducto().get(0).getLimites().get(0).getLimite().getMonto(),result.get(0).getProductsLimits().get(0).getLimits().get(0).getLimit().getAmount());
        assertEquals(responses.getData().get(0).getColaborador().getLimitePorProducto().get(0).getLimites().get(0).getLimite().getMoneda(),result.get(0).getProductsLimits().get(0).getLimits().get(0).getLimit().getCurrency());

    }

    @Test
    public void mapOutEmptyTest() {
        List<ListLettersDelegationsCollaborators> result = restListLettersDelegationsCollaboratorsMapper.mapOut(new ModelListLettersDelegationsCollaboratorsResponseBody());
        assertNull(result);
    }

}
