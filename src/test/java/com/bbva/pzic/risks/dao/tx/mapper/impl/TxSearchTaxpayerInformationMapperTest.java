package com.bbva.pzic.risks.dao.tx.mapper.impl;

import com.bbva.pzic.risks.EntityMock;
import com.bbva.pzic.risks.business.dto.InputSearchTaxpayerInformation;
import com.bbva.pzic.risks.dao.model.rif3.FormatoRIMRF31;
import com.bbva.pzic.risks.dao.model.rif3.FormatoRIMRF32;
import com.bbva.pzic.risks.dao.model.rif3.mock.Rif3Stubs;
import com.bbva.pzic.risks.facade.v0.dto.SearchTaxpayer;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.text.ParseException;

import static com.bbva.pzic.risks.EntityMock.*;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TxSearchTaxpayerInformationMapperTest {

    @InjectMocks
    private TxSearchTaxpayerInformationMapper mapper;

    @Mock
    private Translator translator;

    @Test
    public void mapInFullTest() throws IOException {
        InputSearchTaxpayerInformation input = EntityMock.getInstance().buildInputSearchTaxpayerInformation();

        FormatoRIMRF31 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getTipdoc());
        assertNotNull(result.getNumdoc());

        assertEquals(DOCUMENT_TYPE_DNI_BACKEND, result.getTipdoc());
        assertEquals(input.getDocumentNumber(), result.getNumdoc());
    }

    @Test
    public void mapInEmptyTest() {
        FormatoRIMRF31 result = mapper.mapIn(new InputSearchTaxpayerInformation());

        assertNotNull(result);
        assertNull(result.getTipdoc());
        assertNull(result.getNumdoc());
    }

    @Test
    public void mapOutFullTest() throws IOException, ParseException {
        FormatoRIMRF32 input = Rif3Stubs.getInstance().getRespuestaTransaccionRif3();

        when(translator.translateBackendEnumValueStrictly("risks.taxpayerInformation.search.taxpayerStatus.id",
               input.getEstcon())).thenReturn(EST_CON_00_BACKEND);
        when(translator.translateBackendEnumValueStrictly("risks.taxpayerInformation.search.taxpayerCondition.id",
                input.getConcon())).thenReturn(CON_CON_0_BACKEND);

        SearchTaxpayer result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getFullName());
        assertNotNull(result.getClossingDate());
        assertNotNull(result.getDebtAmount().getAmount());
        assertNotNull(result.getDebtAmount().getCurrency());
        assertNotNull(result.getTaxPayerStatus());
        assertNotNull(result.getTaxPayerCondition());
        assertNotNull(result.getTaxPayerType());
        assertNotNull(result.getFormationDate());
        assertNotNull(result.getActivityStartDate());
        assertNotNull(result.getEconomicActivity().getId());
        assertNotNull(result.getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0));
        assertNotNull(result.getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getAddress().getLocation().getAddressComponents().get(1).getComponentTypes().get(0));
        assertNotNull(result.getAddress().getLocation().getAddressComponents().get(1).getCode());
        assertNotNull(result.getAddress().getLocation().getAddressComponents().get(1).getName());
        assertNotNull(result.getAddress().getLocation().getAddressComponents().get(2).getComponentTypes().get(0));
        assertNotNull(result.getAddress().getLocation().getAddressComponents().get(2).getCode());
        assertNotNull(result.getAddress().getLocation().getAddressComponents().get(2).getName());

        assertEquals(input.getNomape(), result.getFullName());
        assertEquals(DateUtils.toDate(input.getFcierre(), "YYYY-MM-DD"), result.getClossingDate());
        assertEquals(input.getImpdeu(), result.getDebtAmount().getAmount());
        assertEquals(input.getDivisa(), result.getDebtAmount().getCurrency());
        assertEquals(input.getEstcon(), result.getTaxPayerStatus());
        assertEquals(input.getConcon(), result.getTaxPayerCondition());
        assertEquals(input.getTipcon(), result.getTaxPayerType());
        assertEquals(DateUtils.toDate(input.getFconst(), "YYYY-MM-DD"), result.getFormationDate());
        assertEquals(DateUtils.toDate(input.getFiniact(), "YYYY-MM-DD"), result.getActivityStartDate());
        assertEquals(input.getCiiu(), result.getEconomicActivity().getId());
        assertEquals(input.getDeubig(), result.getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0));
        assertEquals(input.getUbigeo(), result.getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertEquals(input.getTipvia(), result.getAddress().getLocation().getAddressComponents().get(1).getComponentTypes().get(0));
        assertEquals(input.getDtipvia(), result.getAddress().getLocation().getAddressComponents().get(1).getCode());
        assertEquals(input.getNomvia(), result.getAddress().getLocation().getAddressComponents().get(1).getName());
        assertEquals(input.getTipzon(), result.getAddress().getLocation().getAddressComponents().get(2).getComponentTypes().get(0));
        assertEquals(input.getDtipzon(), result.getAddress().getLocation().getAddressComponents().get(2).getCode());
        assertEquals(input.getNomzona(), result.getAddress().getLocation().getAddressComponents().get(2).getName());

    }

    @Test
    public void mapOutEmptyTest() throws ParseException {
        SearchTaxpayer result = mapper.mapOut(new FormatoRIMRF32());

        assertNotNull(result);
        assertNull(result.getFullName());
        assertNull(result.getClossingDate());
        assertNull(result.getDebtAmount());
        assertNull(result.getTaxPayerStatus());
        assertNull(result.getTaxPayerCondition());
        assertNull(result.getTaxPayerType());
        assertNull(result.getFormationDate());
        assertNull(result.getActivityStartDate());
        assertNull(result.getEconomicActivity());
        assertNull(result.getAddress().getLocation().getAddressComponents().get(0));
        assertNull(result.getAddress().getLocation().getAddressComponents().get(1));
        assertNull(result.getAddress().getLocation().getAddressComponents().get(2));
    }



}
