package com.bbva.pzic.risks.dao.model.karct003_1;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test de la transacci&oacute;n <code>KARCT003</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionKarct003_1 {

    @InjectMocks
    private TransaccionKarct003_1 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() {
        PeticionTransaccionKarct003_1 rq = new PeticionTransaccionKarct003_1();
        RespuestaTransaccionKarct003_1 rs = new RespuestaTransaccionKarct003_1();

        when(servicioTransacciones.invocar(PeticionTransaccionKarct003_1.class, RespuestaTransaccionKarct003_1.class, rq)).thenReturn(rs);

        RespuestaTransaccionKarct003_1 result = transaccion.invocar(rq);
        assertEquals(result, rs);
    }
}