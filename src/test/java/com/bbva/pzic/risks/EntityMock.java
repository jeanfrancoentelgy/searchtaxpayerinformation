package com.bbva.pzic.risks;

import com.bbva.pzic.risks.business.dto.*;
import com.bbva.pzic.risks.dao.model.collaborators.ModelListLettersDelegationsCollaboratorsResponseBody;
import com.bbva.pzic.risks.dao.model.financialinformation.ModelSearchRisksFinancialInformationResponseBody;
import com.bbva.pzic.risks.dao.model.hya4.FormatoHYMR403;
import com.bbva.pzic.risks.dao.model.hya4.FormatoHYMR404;
import com.bbva.pzic.risks.dao.model.profille.ModelCustomerRiskProfile;
import com.bbva.pzic.risks.dao.model.rif2.FormatoRIMRF22;
import com.bbva.pzic.risks.facade.v0.dto.*;
import com.bbva.pzic.risks.facade.v1.dto.FinancialDebt;
import com.bbva.pzic.risks.facade.v1.dto.Profile;
import com.bbva.pzic.risks.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.List;

/**
 * @author Entelgy
 */
public final class EntityMock {

    public static final String DOCUMENT_TYPE_DNI = "DNI";
    public static final String DOCUMENT_TYPE_DNI_BACKEND = "L";
    public static final String CON_CON_0_BACKEND = "0";
    public static final String CON_CON_0 = "NO HABIDO";
    public static final String EST_CON_00_BACKEND = "00";
    public static final String EST_CON_00 = "ACTIVO";
    public static final String DOCUMENT_NUMBER = "43455563";
    public static final String PROFILE_ID = "123354654765767";
    public static final String CLIENT_ID = "90016482";
    public static final String CUSTOMER_ID = "123413";
    public static final String ENUM_RISKS_RISK_FINANCIAL_INFORMATION_DOCUMENT_TYPE_ID_DNI = "DNI";
    public static final String ENUM_RISKS_RISK_FINANCIAL_INFORMATION_DOCUMENT_TYPE_ID_L = "L";
    public static final String AAP_MOCK_VALUE = "0000001";
    public static final String CALLING_CHANNEL_MOCK_VALUE = "CALLING_CHANNEL";
    public static final String EMPLOYEE_ID = "P025518";
    public static final String BRANCH_ID = "0660";

    private static final EntityMock INSTANCE = new EntityMock();
    private final ObjectMapperHelper objectMapperHelper;

    private EntityMock() {
        objectMapperHelper = ObjectMapperHelper.getInstance();
    }

    public static EntityMock getInstance() {
        return INSTANCE;
    }

    public ModelCustomerRiskProfile buildCustomerRiskProfile() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/customerRiskProfile.json"), ModelCustomerRiskProfile.class);
    }

    public Profile buildProfile() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/profile_request_patch.json"), Profile.class);
    }

    public List<FinancialDebt> buildListFinancialDebts() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/financialDebtResponse.json"), new TypeReference<List<FinancialDebt>>() {
        });
    }

    public InputListRiskFinancialDebts buildInputListRiskFinancialDebts() {
        InputListRiskFinancialDebts input = new InputListRiskFinancialDebts();
        input.setCustomerId(CUSTOMER_ID);
        return input;
    }

    public InputListRiskOffers buildInputListRiskOffers() {
        InputListRiskOffers input = new InputListRiskOffers();
        input.setCustomerId(CUSTOMER_ID);
        return input;
    }

    public InputListRiskExclusions getInputListRiskExclusions() {
        InputListRiskExclusions input = new InputListRiskExclusions();
        input.setCustomerId(CUSTOMER_ID);
        return input;
    }

    public FormatoHYMR403 buildFormatoHYMR403() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/hya4/HYMR403.json"), FormatoHYMR403.class);
    }

    public List<FormatoHYMR404> buildFormatosHYMR404() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/hya4/HYMR404.json"), new TypeReference<List<FormatoHYMR404>>() {
        });
    }

    public SearchRiskInternalFilters buildSearchRiskInternalFilters() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/searchRiskInternalFilters.json"), SearchRiskInternalFilters.class);
    }

    public InputSearchRiskInternalFilters buildInputSearchRiskInternalFilters() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/inputSearchRiskInternalFilters.json"), InputSearchRiskInternalFilters.class);
    }

    public SearchFinancialInformation getSearchFinancialInformationMock() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/searchFinancialInformation.json"), SearchFinancialInformation.class);
    }


    public InputSearchRisksFinancialInformation getInputSearchRisksFinancialInformationMock() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/inputSearchRisksFinancialInformation.json"), InputSearchRisksFinancialInformation.class);
    }

    public ModelSearchRisksFinancialInformationResponseBody getModelSearchRisksFinancialInformationResponseBodyMock() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/modelSearchRisksFinancialInformationResponseBody.json"), ModelSearchRisksFinancialInformationResponseBody.class);
    }

    public InputListLettersDelegationsCollaborators getInputListLettersDelegationsCollaboratorsMock() {
        InputListLettersDelegationsCollaborators inputListLettersDelegationsCollaborators = new InputListLettersDelegationsCollaborators();
        inputListLettersDelegationsCollaborators.setCodigoRegistro(EMPLOYEE_ID);
        inputListLettersDelegationsCollaborators.setCodigoOficina(BRANCH_ID);
        return inputListLettersDelegationsCollaborators;
    }

    public ModelListLettersDelegationsCollaboratorsResponseBody getModelListLettersDelegationsCollaboratorsResponseBodyMock() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/listLetterDelegationCollaborator.json"), ModelListLettersDelegationsCollaboratorsResponseBody.class);
    }

    public SearchTaxpayer buildSearchTaxpayerInformation() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/searchTaxpayerInformation.json"), SearchTaxpayer.class);
    }

    public InputSearchTaxpayerInformation buildInputSearchTaxpayerInformation() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/inputSearchTaxpayerInformation.json"), InputSearchTaxpayerInformation.class);
    }

}
