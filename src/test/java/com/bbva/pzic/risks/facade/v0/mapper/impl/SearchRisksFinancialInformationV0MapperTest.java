package com.bbva.pzic.risks.facade.v0.mapper.impl;


import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.risks.EntityMock;
import com.bbva.pzic.risks.business.dto.InputSearchRisksFinancialInformation;
import com.bbva.pzic.risks.facade.v0.dto.IdentityDocument;
import com.bbva.pzic.risks.facade.v0.dto.SearchFinancialInformation;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static com.bbva.pzic.risks.EntityMock.*;
import static com.bbva.pzic.risks.util.Constants.AAP_PROPERTY;
import static com.bbva.pzic.risks.util.Constants.CALLING_CHANNEL_PROPERTY;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SearchRisksFinancialInformationV0MapperTest {

    @InjectMocks
    private SearchRisksFinancialInformationV0Mapper mapper;

    @Mock
    private Translator translator;

    @Mock
    private ServiceInvocationContext serviceInvocationContext;

    private EntityMock entityMock = EntityMock.getInstance();

    @Before
    public void init() {
        when(translator.translateFrontendEnumValueStrictly("risks.riskFinancialInformation.documentType.id",
                ENUM_RISKS_RISK_FINANCIAL_INFORMATION_DOCUMENT_TYPE_ID_DNI))
                .thenReturn(ENUM_RISKS_RISK_FINANCIAL_INFORMATION_DOCUMENT_TYPE_ID_L);
        when(serviceInvocationContext.getProperty(AAP_PROPERTY)).thenReturn(AAP_MOCK_VALUE);
        when(serviceInvocationContext.getProperty(CALLING_CHANNEL_PROPERTY)).thenReturn(CALLING_CHANNEL_MOCK_VALUE);
    }

    @Test
    public void mapInFullTest() throws IOException {
        SearchFinancialInformation input = entityMock.getSearchFinancialInformationMock();

        InputSearchRisksFinancialInformation result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getConsumerId());
        assertNotNull(result.getCallingChannel());
        assertNotNull(result.getCustomer());
        assertNotNull(result.getCustomer().getCustomerId());
        assertNotNull(result.getIdentityDocument());
        assertNotNull(result.getIdentityDocument().getDocumentNumber());
        assertNotNull(result.getIdentityDocument().getDocumentType());
        assertNotNull(result.getIdentityDocument().getDocumentType().getId());

        assertEquals(AAP_MOCK_VALUE, result.getConsumerId());
        assertEquals(CALLING_CHANNEL_MOCK_VALUE, result.getCallingChannel());
        assertEquals(input.getCustomerId(), result.getCustomer().getCustomerId());
        assertEquals(input.getIdentityDocument().getDocumentNumber(), result.getIdentityDocument().getDocumentNumber());
        assertEquals(ENUM_RISKS_RISK_FINANCIAL_INFORMATION_DOCUMENT_TYPE_ID_L, result.getIdentityDocument().getDocumentType().getId());
    }

    @Test
    public void mapInWithEmptyIdentityDocumentTest() throws IOException {
        SearchFinancialInformation input = entityMock.getSearchFinancialInformationMock();
        input.setIdentityDocument(new IdentityDocument());

        InputSearchRisksFinancialInformation result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getConsumerId());
        assertNotNull(result.getCallingChannel());
        assertNotNull(result.getCustomer());
        assertNotNull(result.getCustomer().getCustomerId());
        assertNull(result.getIdentityDocument());

        assertEquals(AAP_MOCK_VALUE, result.getConsumerId());
        assertEquals(CALLING_CHANNEL_MOCK_VALUE, result.getCallingChannel());
        assertEquals(input.getCustomerId(), result.getCustomer().getCustomerId());
    }

    @Test
    public void mapInWithNullIdentityDocumentTest() throws IOException {
        SearchFinancialInformation input = entityMock.getSearchFinancialInformationMock();
        input.setIdentityDocument(null);

        InputSearchRisksFinancialInformation result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getConsumerId());
        assertNotNull(result.getCallingChannel());
        assertNotNull(result.getCustomer());
        assertNotNull(result.getCustomer().getCustomerId());
        assertNull(result.getIdentityDocument());

        assertEquals(AAP_MOCK_VALUE, result.getConsumerId());
        assertEquals(CALLING_CHANNEL_MOCK_VALUE, result.getCallingChannel());
        assertEquals(input.getCustomerId(), result.getCustomer().getCustomerId());
    }

    @Test
    public void mapInWithDocumentNumberOnlyTest() throws IOException {
        SearchFinancialInformation input = entityMock.getSearchFinancialInformationMock();
        input.getIdentityDocument().setDocumentType(null);

        InputSearchRisksFinancialInformation result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getConsumerId());
        assertNotNull(result.getCallingChannel());
        assertNotNull(result.getCustomer());
        assertNotNull(result.getCustomer().getCustomerId());
        assertNotNull(result.getIdentityDocument());
        assertNotNull(result.getIdentityDocument().getDocumentNumber());
        assertNull(result.getIdentityDocument().getDocumentType());

        assertEquals(AAP_MOCK_VALUE, result.getConsumerId());
        assertEquals(CALLING_CHANNEL_MOCK_VALUE, result.getCallingChannel());
        assertEquals(input.getCustomerId(), result.getCustomer().getCustomerId());
        assertEquals(input.getIdentityDocument().getDocumentNumber(), result.getIdentityDocument().getDocumentNumber());
    }

    @Test
    public void mapInWithDocumentTypeIdOnlyTest() throws IOException {
        SearchFinancialInformation input = entityMock.getSearchFinancialInformationMock();
        input.getIdentityDocument().setDocumentNumber(null);

        InputSearchRisksFinancialInformation result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getConsumerId());
        assertNotNull(result.getCallingChannel());
        assertNotNull(result.getCustomer());
        assertNotNull(result.getCustomer().getCustomerId());
        assertNotNull(result.getIdentityDocument());
        assertNull(result.getIdentityDocument().getDocumentNumber());
        assertNotNull(result.getIdentityDocument().getDocumentType());
        assertNotNull(result.getIdentityDocument().getDocumentType().getId());

        assertEquals(AAP_MOCK_VALUE, result.getConsumerId());
        assertEquals(CALLING_CHANNEL_MOCK_VALUE, result.getCallingChannel());
        assertEquals(input.getCustomerId(), result.getCustomer().getCustomerId());
        assertEquals(ENUM_RISKS_RISK_FINANCIAL_INFORMATION_DOCUMENT_TYPE_ID_L, result.getIdentityDocument().getDocumentType().getId());
    }

    @Test
    public void mapInEmptyTest() {
        InputSearchRisksFinancialInformation result = mapper.mapIn(new SearchFinancialInformation());

        assertNotNull(result);
        assertNotNull(result.getConsumerId());
        assertNotNull(result.getCallingChannel());
        assertNotNull(result.getCustomer());
        assertNull(result.getCustomer().getCustomerId());
        assertNull(result.getIdentityDocument());

        assertEquals(AAP_MOCK_VALUE, result.getConsumerId());
        assertEquals(CALLING_CHANNEL_MOCK_VALUE, result.getCallingChannel());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<SearchFinancialInformation> result = mapper.mapOut(new SearchFinancialInformation());

        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutNullTest() {
        ServiceResponse<SearchFinancialInformation> result = mapper.mapOut(null);

        assertNull(result);
    }
}
