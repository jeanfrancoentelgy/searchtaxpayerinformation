package com.bbva.pzic.risks.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.BackendContext;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.risks.EntityMock;
import com.bbva.pzic.risks.business.dto.InputListRiskFinancialDebts;
import com.bbva.pzic.risks.facade.v1.dto.FinancialDebt;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created on 19/05/2020.
 *
 * @author Entelgy.
 */
@RunWith(MockitoJUnitRunner.class)
public class ListRiskFinancialDebtsMapperTest {

    @Mock
    private ServiceInvocationContext serviceInvocationContext;

    @InjectMocks
    private ListRiskFinancialDebtsMapper mapper;

    @Test
    public void mapInWithClientContextTest() {
        Mockito.when(serviceInvocationContext.getProperty(BackendContext.ASTA_MX_CLIENT_ID)).thenReturn(EntityMock.CLIENT_ID);
        InputListRiskFinancialDebts result = mapper.mapIn(EntityMock.CUSTOMER_ID);

        assertNotNull(result);
        assertNotNull(result.getCustomerId());

        assertEquals(EntityMock.CLIENT_ID, result.getCustomerId());
    }

    @Test
    public void mapInWithoutClientContextTest() {
        Mockito.when(serviceInvocationContext.getProperty(BackendContext.ASTA_MX_CLIENT_ID)).thenReturn(null);
        InputListRiskFinancialDebts result = mapper.mapIn(EntityMock.CUSTOMER_ID);

        assertNotNull(result);
        assertNotNull(result.getCustomerId());

        assertEquals(EntityMock.CUSTOMER_ID, result.getCustomerId());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        ServiceResponse<List<FinancialDebt>> result = mapper.mapOut(EntityMock.getInstance().buildListFinancialDebts());

        assertNotNull(result);
    }

    @Test
    public void mapOutNullTest() {
        ServiceResponse<List<FinancialDebt>> result = mapper.mapOut(null);

        assertNull(result);
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<List<FinancialDebt>> result = mapper.mapOut(Collections.emptyList());

        assertNull(result);
    }
}
