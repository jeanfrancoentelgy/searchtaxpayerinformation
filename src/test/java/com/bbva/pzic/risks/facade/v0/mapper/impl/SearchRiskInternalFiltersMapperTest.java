package com.bbva.pzic.risks.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.EntityMock;
import com.bbva.pzic.risks.business.dto.InputSearchRiskInternalFilters;
import com.bbva.pzic.risks.facade.v0.dto.SearchRiskInternalFilters;
import com.bbva.pzic.risks.facade.v0.mapper.ISearchRiskInternalFiltersMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created on 15/06/20  20.
 *
 * @author Entelgy.
 */
public class SearchRiskInternalFiltersMapperTest {

    private ISearchRiskInternalFiltersMapper mapper;

    @Before
    public void setUp() {
        mapper = new SearchRiskInternalFiltersMapper();
    }

    @Test
    public void mapInFullTest() throws IOException {
        SearchRiskInternalFilters input = EntityMock.getInstance().buildSearchRiskInternalFilters();
        InputSearchRiskInternalFilters result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCustomerId());
        assertNull(result.getDocumentNumber());
        assertNull(result.getDocumentTypeId());

        assertEquals(input.getCustomerId(), result.getCustomerId());
    }

    @Test
    public void mapInEmptyTest() {
        InputSearchRiskInternalFilters result = mapper.mapIn(new SearchRiskInternalFilters());

        assertNotNull(result);
        assertNull(result.getCustomerId());
        assertNull(result.getDocumentNumber());
        assertNull(result.getDocumentTypeId());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        SearchRiskInternalFilters input = EntityMock.getInstance().buildSearchRiskInternalFilters();
        ServiceResponse result = mapper.mapOut(input);

        assertNotNull(result);
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse result = mapper.mapOut(null);

        assertNull(result);
    }
}