package com.bbva.pzic.risks.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.business.dto.InputListLettersDelegationsCollaborators;
import com.bbva.pzic.risks.facade.v0.dto.ListLettersDelegationsCollaborators;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static com.bbva.pzic.risks.EntityMock.*;
import static org.junit.Assert.*;

public class ListLettersDelegationsCollaboratorsMapperTest {


    private ListLettersDelegationsCollaboratorsMapper listLettersDelegationsCollaboratorsMapper = new ListLettersDelegationsCollaboratorsMapper();

    @Test
    public void mapInFullTest() {
        InputListLettersDelegationsCollaborators result = listLettersDelegationsCollaboratorsMapper.mapIn(EMPLOYEE_ID,BRANCH_ID);

        assertNotNull(result);
        assertNotNull(result.getCodigoRegistro());
        assertNotNull(result.getCodigoOficina());

        assertEquals(EMPLOYEE_ID,result.getCodigoRegistro());
        assertEquals(BRANCH_ID,result.getCodigoOficina());
    }

    @Test
    public void mapInWithOutEmployeeIdTest() {
        InputListLettersDelegationsCollaborators result = listLettersDelegationsCollaboratorsMapper.mapIn(null,BRANCH_ID);

        assertNotNull(result);
        assertNull(result.getCodigoRegistro());
        assertNotNull(result.getCodigoOficina());
        
        assertEquals(BRANCH_ID,result.getCodigoOficina());
    }
    
    @Test
    public void mapInWithOutBranchIdTest() {
        InputListLettersDelegationsCollaborators result = listLettersDelegationsCollaboratorsMapper.mapIn(EMPLOYEE_ID,null);

        assertNotNull(result);
        assertNotNull(result.getCodigoRegistro());
        assertNull(result.getCodigoOficina());

        assertEquals(EMPLOYEE_ID,result.getCodigoRegistro());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<List<ListLettersDelegationsCollaborators>> result = listLettersDelegationsCollaboratorsMapper.mapOut(Collections.singletonList(new ListLettersDelegationsCollaborators()));
        assertNotNull(result);
        assertNotNull(result.getData());
    }

}
