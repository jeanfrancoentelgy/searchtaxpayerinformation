package com.bbva.pzic.risks.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.EntityMock;
import com.bbva.pzic.risks.dao.model.blacklisted.ModelClientRequest;
import com.bbva.pzic.risks.facade.v1.dto.RiskBlackListedEntity;
import com.bbva.pzic.risks.util.mappers.EnumMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

/**
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class ListRiskBlackListedEntitiesMapperTest {

    @InjectMocks
    private ListRiskBlackListedEntitiesMapper mapper;

    @Mock
    private EnumMapper enumMapper;

    @Test
    public void mapInFullTest() {
        Mockito.when(enumMapper.getBackendValue("documentType.id", "DNI"))
                .thenReturn("L");

        ModelClientRequest result = mapper.mapIn(EntityMock.DOCUMENT_TYPE_DNI, EntityMock.DOCUMENT_NUMBER);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getNumdocumento());
        Assert.assertNotNull(result.getTipdocumento());

        Assert.assertEquals(enumMapper.getBackendValue("documentType.id", EntityMock.DOCUMENT_TYPE_DNI), result.getTipdocumento());
        Assert.assertEquals(EntityMock.DOCUMENT_NUMBER, result.getNumdocumento());
    }

    @Test
    public void mapInEmptyTest() {
        ModelClientRequest result = mapper.mapIn(null, null);
        Assert.assertNotNull(result);
        Assert.assertNull(result.getNumdocumento());
        Assert.assertNull(result.getTipdocumento());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<List<RiskBlackListedEntity>> result = mapper.mapOut(Collections.singletonList(new RiskBlackListedEntity()));
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<List<RiskBlackListedEntity>> result = mapper.mapOut(Collections.<RiskBlackListedEntity>emptyList());
        Assert.assertNull(result);

        result = mapper.mapOut(null);
        Assert.assertNull(result);
    }
}
