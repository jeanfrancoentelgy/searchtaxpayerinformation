package com.bbva.pzic.risks.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.EntityMock;
import com.bbva.pzic.risks.business.dto.InputListRiskExclusions;
import com.bbva.pzic.risks.facade.v1.dto.Exclusion;
import com.bbva.pzic.risks.facade.v1.mapper.IListRiskExclusionsMapper;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created on 17/06/2020.
 *
 * @author Entelgy
 */
public class ListRiskExclusionsMapperTest {

    private IListRiskExclusionsMapper mapper;

    @Before
    public void setUp() {
        mapper = new ListRiskExclusionsMapper();
    }

    @Test
    public void mapInFullTest() {
        InputListRiskExclusions result = mapper.mapIn(EntityMock.CUSTOMER_ID);
        assertNotNull(result);
        assertNotNull(result.getCustomerId());
        assertEquals(EntityMock.CUSTOMER_ID, result.getCustomerId());
    }

    @Test
    public void mapInEmptyTest() {
        InputListRiskExclusions result = mapper.mapIn(null);
        assertNotNull(result);
        assertNull(result.getCustomerId());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<List<Exclusion>> result = mapper.mapOut(Collections.singletonList(new Exclusion()));
        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<List<Exclusion>> result = mapper.mapOut(Collections.emptyList());
        assertNull(result);
    }
}
