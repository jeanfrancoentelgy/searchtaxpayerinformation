package com.bbva.pzic.risks.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.EntityMock;
import com.bbva.pzic.risks.business.dto.InputListRiskOffers;
import com.bbva.pzic.risks.facade.v1.dto.RiskOffer;
import com.bbva.pzic.risks.facade.v1.mapper.IListRiskOffersMapper;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created on 12/06/2020.
 *
 * @author Entelgy
 */
public class ListRiskOffersMapperTest {

    private IListRiskOffersMapper mapper;

    @Before
    public void setUp() {
        mapper = new ListRiskOffersMapper();
    }

    @Test
    public void mapInFullTest() {
        InputListRiskOffers result = mapper.mapIn(EntityMock.CUSTOMER_ID);
        assertNotNull(result);
        assertNotNull(result.getCustomerId());
        assertEquals(EntityMock.CUSTOMER_ID, result.getCustomerId());
    }

    @Test
    public void mapInEmptyTest() {
        InputListRiskOffers result = mapper.mapIn(null);
        assertNotNull(result);
        assertNull(result.getCustomerId());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<List<RiskOffer>> result = mapper.mapOut(Collections.singletonList(new RiskOffer()));
        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<List<RiskOffer>> result = mapper.mapOut(Collections.emptyList());
        assertNull(result);
    }
}
