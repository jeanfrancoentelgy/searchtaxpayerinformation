package com.bbva.pzic.risks.facade.v1.mapper.impl;

import com.bbva.pzic.risks.EntityMock;
import com.bbva.pzic.risks.dao.model.blacklisted.ModelProfileRequest;
import com.bbva.pzic.risks.facade.v1.dto.Profile;
import com.bbva.pzic.risks.util.mappers.EnumMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

@RunWith(MockitoJUnitRunner.class)
public class ModifyCustomerRiskProfileMapperTest {

    @Mock
    private EnumMapper enumMapper;

    @InjectMocks
    private ModifyCustomerRiskProfileMapper mapper;

    @Before
    public void init() {
        Mockito.when(enumMapper.getBackendValue("riskProfileType", "MODERATE"))
                .thenReturn("2");
    }

    @Test
    public void mapInTest() throws IOException {
        ModelProfileRequest result = mapper.mapIn(EntityMock.PROFILE_ID, EntityMock.getInstance().buildProfile());
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getProfileId());
        Assert.assertNotNull(result.getRiskProfileType());

        Assert.assertEquals(EntityMock.PROFILE_ID, result.getProfileId());
        Assert.assertEquals("2", result.getRiskProfileType());
    }

    @Test
    public void mapInWithoutPayloadEmptyTest() {
        ModelProfileRequest result = mapper.mapIn(EntityMock.PROFILE_ID, new Profile());
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getProfileId());
        Assert.assertNull(result.getRiskProfileType());

        Assert.assertEquals(EntityMock.PROFILE_ID, result.getProfileId());
    }
}
