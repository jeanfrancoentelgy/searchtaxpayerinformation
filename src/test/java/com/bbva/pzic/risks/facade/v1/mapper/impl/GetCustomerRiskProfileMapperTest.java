package com.bbva.pzic.risks.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.EntityMock;
import com.bbva.pzic.risks.dao.model.profille.ModelCustomerRiskProfile;
import com.bbva.pzic.risks.facade.v1.dto.Profile;
import com.bbva.pzic.risks.util.mappers.EnumMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

/**
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class GetCustomerRiskProfileMapperTest {

    @InjectMocks
    private GetCustomerRiskProfileMapper mapper;

    @Mock
    private EnumMapper enumMapper;

    @Before
    public void setUp() {
        Mockito.when(enumMapper.getEnumValue("riskProfileType", "2"))
                .thenReturn("MODERATE");
        Mockito.when(enumMapper.getEnumValue("status", "A"))
                .thenReturn("ACTIVE");
    }

    @Test
    public void mapOutFullTest() throws IOException {
        ModelCustomerRiskProfile output = EntityMock.getInstance().buildCustomerRiskProfile();
        ServiceResponse<Profile> result = mapper.mapOut(output);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());

        Assert.assertNotNull(result.getData());
        Assert.assertNotNull(result.getData().getId());
        Assert.assertNotNull(result.getData().getStatus());
        Assert.assertNotNull(result.getData().getRiskProfileType());
        Assert.assertNotNull(result.getData().getRecalculationAttemptsLeft());
        Assert.assertNotNull(result.getData().getName());

        Assert.assertEquals(output.getId(), result.getData().getId());
        Assert.assertEquals("ACTIVE", result.getData().getStatus());
        Assert.assertEquals("MODERATE", result.getData().getRiskProfileType());
        Assert.assertEquals(output.getRecalculationAttemptsLeft(), result.getData().getRecalculationAttemptsLeft());
        Assert.assertEquals(output.getName(), result.getData().getName());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<Profile> result = mapper.mapOut(null);
        Assert.assertNull(result);
    }
}
