package com.bbva.pzic.risks.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.risks.EntityMock;
import com.bbva.pzic.risks.business.dto.InputSearchTaxpayerInformation;
import com.bbva.pzic.risks.facade.v0.dto.SearchTaxpayer;
import com.bbva.pzic.routine.translator.facade.Translator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static com.bbva.pzic.risks.EntityMock.DOCUMENT_TYPE_DNI_BACKEND;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 15/06/2020.
 *
 * @author Entelgy.
 */
@RunWith(MockitoJUnitRunner.class)
public class SearchTaxpayerInformationMapperTest {

    @InjectMocks
    private SearchTaxpayerInformationMapper mapper;

    @Mock
    private Translator translator;

    @Test
    public void mapInFullTest() throws IOException {
        SearchTaxpayer input = EntityMock.getInstance().buildSearchTaxpayerInformation();

        when(translator.translateFrontendEnumValueStrictly("risks.taxpayerInformation.search.documentType.id",
                input.getDocumentType())).thenReturn(DOCUMENT_TYPE_DNI_BACKEND);

        InputSearchTaxpayerInformation result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getDocumentNumber());
        assertNotNull(result.getDocumentType());

        assertEquals(input.getDocumentNumber(), result.getDocumentNumber());
        assertEquals(DOCUMENT_TYPE_DNI_BACKEND, result.getDocumentType());

    }

    @Test
    public void mapInEmptyTest(){
        InputSearchTaxpayerInformation result = mapper.mapIn(new SearchTaxpayer());
        assertNotNull(result);
        assertNull(result.getDocumentNumber());
        assertNull(result.getDocumentType());
    }

    @Test
    public void mapOutFullTest(){
        ServiceResponse<SearchTaxpayer> result = mapper.mapOut(new SearchTaxpayer());
        assertNotNull(result);
        assertNotNull(result.getData());

    }

    @Test
    public void mapOutEmptyTest(){

        ServiceResponse<SearchTaxpayer> result = mapper.mapOut(null);
        assertNull(result);

    }


}
